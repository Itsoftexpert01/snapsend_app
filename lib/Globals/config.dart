import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:snap_send_app/model/color.dart';

class Config {
  static String playstore = "com.snap_send.app";
  static String appstore = "id1667165005";
  static String mapKey = "AIzaSyDxMYVrK911xnJt4vEWJKBPy4hih-nuDfM";
  static String version = "App Version v0.0.11";
  static String privacyPolicy = "https://www.snapsendrepot.com/privacy-policy";
  static String termsandConditions = "https://www.snapsendrepot.com/terms-of-service";
  static String baseURL = "https://snapsendreport.com/api/v1/";
  static String liveURL = "https://snapsendreport.com/api/v1/";
  static String imageURL = "https://snapsendreport.com";
}

var spinkit = SpinKitCircle(
  color: buttonColor,
  size: 50.0,
);
