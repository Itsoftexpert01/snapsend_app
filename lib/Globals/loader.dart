import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import 'package:snap_send_app/model/color.dart';

FToast fToast = FToast();

showLoader(BuildContext context, message) {
  AlertDialog alert = AlertDialog(
    content: Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(1),
          child: CircularProgressIndicator(
            backgroundColor: buttonColor,
            color: primaryColor,
          ),
        ),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.all(7),
          child: Text(
            message,
            maxLines: 5,
          ),
        )),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

pop(BuildContext context) {
  Navigator.pop(context);
}

canPop(BuildContext context) {
  if (Navigator.canPop(context)) {
    Navigator.pop(context);
  }
}

String removeHtmlTags(String htmlString) {
  String textWithoutHtml = htmlString.replaceAll(RegExp(r'<[^>]*>'), '');
  return textWithoutHtml;
}

showToast(String msg) {
  return Fluttertoast.showToast(
      timeInSecForIosWeb: 5,
      toastLength: Toast.LENGTH_LONG,
      backgroundColor: buttonColor,
      msg: removeHtmlTags(msg.tr));
}

unFocus(context) {
  FocusScope.of(context).unfocus();
}

underDevelopment(context) {
  showToast("Upcoming Feature");
}
