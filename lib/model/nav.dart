import 'package:flutter/material.dart';
import 'package:page_animation_transition/animations/fade_animation_transition.dart';
import 'package:page_animation_transition/page_animation_transition.dart';

// ignore: prefer_typing_uninitialized_variables
var userData;
double left = 0;
dynamic saveNav;
List<dynamic> screenHistory = [];
double width(context) {
  return MediaQuery.of(context).size.width;
}

double height(context) {
  return MediaQuery.of(context).size.height;
}

void navigate(context, String to, {arguments}) {
  Navigator.of(context).pushNamed(to, arguments: arguments);
}

void nav(context, to, {arguments}) {
  Navigator.of(context, rootNavigator: true).push(PageAnimationTransition(
      page: to, pageAnimationType: FadeAnimationTransition()));
}

navandThen(context, to, back, {arguments}) {
  Navigator.of(context)
      .push(PageAnimationTransition(
          page: to, pageAnimationType: FadeAnimationTransition()))
      .then((value) => back);
}

navandThenNav(context, to, back, {arguments}) {
  Navigator.of(context)
      .push(PageAnimationTransition(
          page: to, pageAnimationType: FadeAnimationTransition()))
      .then((value) => Navigator.of(context).push(PageAnimationTransition(
          page: back, pageAnimationType: FadeAnimationTransition())));
}

void navigateAndRemove(context, to, {arguments}) {
  Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
    MaterialPageRoute(
      builder: (BuildContext context) {
        return to;
      },
    ),
    (_) => false,
  );
  // Navigator.of(context)
  //     .push(PageAnimationTransition(
  //         page: to, pageAnimationType: FadeAnimationTransition()))
  //     .then((value) => exit(0));
}

void navandClear(context, screen) {
  Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
    MaterialPageRoute(
      builder: (BuildContext context) {
        return screen;
      },
    ),
    (_) => false,
  );
}

void navPushReplacement(context, to, {arguments}) {
    Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
    MaterialPageRoute(
      builder: (BuildContext context) {
        return to;
      },
    ),
    (_) => false,
  );

}

void pops(context) {
  Navigator.of(context).pop();
}

void navigateToNextScreen(BuildContext context, Widget screen) {
  Navigator.of(context).push(MaterialPageRoute(builder: (context) => screen));
}
