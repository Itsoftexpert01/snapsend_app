import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/View/Dashboard/home.dart';
import 'package:snap_send_app/View/Notification/notification_screen.dart';
import 'package:snap_send_app/View/Profile/ui/profile_screen.dart';
import 'package:snap_send_app/View/Search/searchDepartments/ui/search_screen.dart';
import 'package:snap_send_app/View/Tip/tip_screen.dart';
import 'package:snap_send_app/model/color.dart';

// ignore: use_key_in_widget_constructors
class MyBottomNavigationBar extends StatefulWidget {
  @override
  // ignore: library_private_types_in_public_api
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  bool iscenter = false;
final GlobalKey<ScaffoldState> key = GlobalKey();

  List<Widget> _buildScreens() {
    return [
      HomeScreen(),
      SearchScreen(),
      TipScreen(
        from: "bottom",
      ),
      const NotificationScreen(),
      MyProfile(),
    ];
  }
  setUp() async {
    await Storage.set("setup", 'false');
  }

  getsetUp() async {
    var isSetup = await Storage.get("setup");
    if (isSetup == null) {
      _controller = PersistentTabController(initialIndex: 4);
    } else {
      _controller = PersistentTabController(initialIndex: 0);
      setUp();
    }
  }

  PersistentTabController? _controller;

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.home),
        title: ("Home").tr,
        activeColorPrimary: buttonColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.search),
        title: ("Search").tr,
        activeColorPrimary: buttonColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Center(
          child: Icon(
            CupertinoIcons.add,
            color: iscenter ? Colors.white : Colors.grey,
            size: 30,
          ),
        ),
        title: ("Add").tr,
        inactiveColorSecondary: Colors.white,
        activeColorPrimary: iscenter ? buttonColor : Colors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.bell),
        title: ("Notification").tr,
        activeColorPrimary: buttonColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.person),
        title: ("Profile").tr,
        activeColorPrimary: buttonColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    getsetUp();
    return Scaffold(
        body: WillPopScope(
      onWillPop: () async {
        if (key.currentState!.isDrawerOpen) {
          key.currentState!.openEndDrawer();
          return false;
        } else {
          final shouldPop = await showDialog<bool>(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text('Do you want to exit the app?'.tr),
                //   actionsAlignment: MainAxisAlignment.spaceBetween,
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context, true);
                    },
                    child: Text('Yes'.tr),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: Text('No'.tr),
                  ),
                ],
              );
            },
          );
          return shouldPop!;
        }
        // ignore: use_build_context_synchronously
      },
      child: PersistentTabView(
        context,
        controller: _controller,
        screens: _buildScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.white, // Default is Colors.white.
        handleAndroidBackButtonPress: true, // Default is true.
        resizeToAvoidBottomInset:
            true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
        stateManagement: true, // Default is true.
        hideNavigationBarWhenKeyboardShows:
            true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
        decoration: NavBarDecoration(
          borderRadius: BorderRadius.circular(10.0),
          colorBehindNavBar: Colors.white,
        ),
        popAllScreensOnTapOfSelectedTab: true,
        popActionScreens: PopActionScreensType.all,
        itemAnimationProperties: const ItemAnimationProperties(
          // Navigation Bar's items animation properties.
          duration: Duration(milliseconds: 200),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: const ScreenTransitionAnimation(
          // Screen transition animation on change of selected tab.
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
        navBarStyle: NavBarStyle.style15,
        onItemSelected: (index) {
          if (index == 2) {
            iscenter = true;

            Storage.set('istip', 'true');
            setState(() {});
          } else {
            iscenter = false;
            setState(() {});
          }
          debugPrint("is centerd $iscenter");
          Storage.set('isfrom', 'nav');
        },
        // Choose the nav bar style with this property.
      ),
    ));
  }
}