import 'package:flutter/material.dart';

Color darkblue = const Color(0xff121D39);
Color blue = Colors.blue;
Color green = Colors.green;

Color black = Colors.black;
Color black54 = Colors.black54;
Color? greylight = Colors.grey[100];
Color grey = Colors.grey;
Color white = Colors.white;
Color red = const Color(0xffDC4E41);
Color primaryColor = const Color(0xff282c54);
Color buttonColor  =const Color(0xff5b97e1);
