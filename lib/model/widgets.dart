import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/model/nav.dart';

Widget text1(String text, double fontsize, Color? color) {
  return Text(
    text.tr,
    textAlign: TextAlign.center,
    style: TextStyle(fontSize: fontsize, color: color, letterSpacing: 0.5),
  );
}

Widget text2(String text, double fontsize, Color? color) {
  return Text(
    text.tr,
    textAlign: TextAlign.center,
    style: TextStyle(
      fontSize: fontsize,
      color: color,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.5,
    ),
  );
}

Widget text7(String text, double fontsize, Color? color) {
  return Text(
    text.tr,
    style: TextStyle(
        fontSize: fontsize,
        color: color,
        fontWeight: FontWeight.bold,
        letterSpacing: 0.1),
  );
}

Widget text3(String text, double fontsize, Color? color) {
  return Text(
    text.tr,
    style: TextStyle(
        fontSize: fontsize,
        color: color,
        overflow: TextOverflow.fade,
        letterSpacing: 0.3),
  );
}

Widget text5(String text, double fontsize, Color? color) {
  return Text(
    text.tr,
    style: TextStyle(fontSize: fontsize, color: color, letterSpacing: 0.5),
  );
}

Widget text4(String text, double fontsize, Color? color) {
  return Text(
    text.tr,
    style: TextStyle(
        fontSize: fontsize,
        color: color,
        fontWeight: FontWeight.bold,
        letterSpacing: 0.15),
  );
}

Widget googlefbIcon({dynamic image}) {
  return Container(
    height: 39,
    width: 39,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(30),
      border: Border.all(
        color: Color(0xff58c4ec),
      ),
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(30),
      child: Image.asset(
        image,
        height: 17,
        fit: BoxFit.fill,
      ),
    ),
  );
}

Widget textformfield1({
  String? hinttext,
  IconData? icon,
  TextEditingController? controller,
  void Function()? onPressed,
  FocusNode? focusNode,
  Function()? onTap,
  bool isreadonly = false,
  bool obscureText = false,
}) {
  return Padding(
    padding: const EdgeInsets.all(0.0),
    child: Container(
      height: 50,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey, width: 0.7),
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextFormField(
        autofocus: true,
        obscureText: obscureText,
        readOnly: isreadonly,
        onTap: onTap,
        focusNode: focusNode,
        controller: controller,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(icon),
            onPressed: onPressed,
          ),
          border: InputBorder.none,
          hintText: hinttext!.tr,
          hintStyle: const TextStyle(fontSize: 16, letterSpacing: 0.5),
          contentPadding: const EdgeInsets.only(left: 10, top: 15),
        ),
      ),
    ),
  );
}

Widget textformfield2(
    {String? hinttext,
    IconData? icon1,
    IconData? icon2,
    TextEditingController? controller}) {
  return Container(
    height: 100,
    width: 400,
    decoration: BoxDecoration(
      border: Border.all(color: Colors.grey, width: 0.7),
      borderRadius: BorderRadius.circular(5),
    ),
    child: TextFormField(
      controller: controller,
      maxLines: 10,
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: const EdgeInsets.fromLTRB(12, 16, 12, 16),
        hintText: hinttext?.tr,
        hintStyle: const TextStyle(fontSize: 13, letterSpacing: 0.5),
      ),
    ),
  );
}

Widget loginbutton(
    {BuildContext? context, dynamic buttontext, dynamic loginIcon}) {
  return Container(
      height: 53,
      width: MediaQuery.of(context!).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.topRight,
          colors: [Color(0XFF9DCEFF), Color(0xff92A3FD)],
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Center(
        child: Container(
          height: 51,
          width: 100,
          child: Row(
            children: [
              Image.asset(loginIcon),
              const SizedBox(
                width: 7,
              ),
              text2(buttontext, 15, Colors.white)
            ],
          ),
        ),
      ));
}

Widget regbutton2({
  BuildContext? context,
  dynamic buttontext,
}) {
  return Container(
      height: 52,
      width: MediaQuery.of(context!).size.width,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: Colors.grey, width: 0.5)),
      child: Center(
          child: text2(
        buttontext,
        15,
        Color(0xff57C7EB),
      )));
}

Widget nextbutton(
    {BuildContext? context, dynamic buttontext, dynamic loginIcon}) {
  return Container(
    height: 53,
    width: MediaQuery.of(context!).size.width,
    decoration: BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.topRight,
        colors: [Color(0XFF9DCEFF), Color(0xff92A3FD)],
      ),
      borderRadius: BorderRadius.circular(30),
    ),
    child: Center(
      child: text2(buttontext, 15, Colors.white),
    ),
  );
}

Widget problemlist(
    {BuildContext? context, dynamic text1, Function()? onTap, Color? color}) {
  return GestureDetector(
    onTap: onTap,
    child: SizedBox(
      width: MediaQuery.of(context!).size.width,
      child: Card(
          color: color,
          shape: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: const BorderSide(color: Colors.white),
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: text7(
                text1,
                16,
                Colors.black,
              ),
            ),
          )),
    ),
  );
}

Widget selectionprob(
    {dynamic image,
    dynamic text,
    Color? colorIcon,
    Color? colortext,
    Color? colorbox}) {
  return Container(
    padding: EdgeInsets.all(10),
    height: 200,
    width: 200,
    decoration: BoxDecoration(
      color: colorbox,
      borderRadius: BorderRadius.circular(20),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(image),
        const SizedBox(
          height: 30,
        ),
        Text(
          text,
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w500, color: colortext),
        ),
      ],
    ),
  );
}

// ignore: must_be_immutable
class InAppWeb extends StatefulWidget {
  dynamic link;
  InAppWeb(this.link, {super.key});

  @override
  State<InAppWeb> createState() => _InAppWebState();
}

class _InAppWebState extends State<InAppWeb> {
  bool isLoading = true;

  bool isError = false;

  InAppWebViewController? webViewController;

  @override
  Widget build(BuildContext context) {
    String url = widget.link.replaceAll(RegExp(r'^https?://'), '');

    //String url = widget.link;
    String httpsUrl = 'https://$url';
    String httpUrl = 'http://$url';
    print("URL IS $url");
    print("httpsUrl IS $httpsUrl");
    print("httpUrl IS $httpUrl");
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              pops(context);
            },
          ),
          title: const Text('Web View'),
        ),
        body: Stack(
          children: [
            InAppWebView(
              initialUrlRequest: URLRequest(url: Uri.parse(httpsUrl)),
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                  javaScriptEnabled: true,
                ),
              ),
              onLoadStart: (controller, url) {
                setState(() {
                  isLoading = true;
                  isError = false;
                });
              },
              onLoadStop: (controller, url) {
                setState(() {
                  isLoading = false;
                  isError = false;
                });
              },
              onLoadError: (controller, url, code, message) {
                // If https fails, try http
                // if (url!.path == httpsUrl) {
                webViewController!.loadUrl(
                  urlRequest: URLRequest(url: Uri.parse(httpUrl)),
                );
                // }
                // else {
                //   setState(() {
                //     isLoading = false;
                //     isError = true;
                //   });
                // }
              },
              onWebViewCreated: (controller) {
                webViewController = controller;
              },
            ),
            if (isLoading)
              const Center(
                child: CircularProgressIndicator(),
              ),
            if (isError)
              const Center(
                child: Text(
                  'Error loading the page',
                  style: TextStyle(color: Colors.red),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
