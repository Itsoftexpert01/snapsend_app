import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget button1(
    {BuildContext? context,
    dynamic text,
    Color? color,
    double? height,
    dynamic ontap}) {
  return Padding(
    padding: const EdgeInsets.only(top: 6, bottom: 5, left: 35, right: 35),
    child: GestureDetector(
      onTap: ontap,
      child: Container(
          width: MediaQuery.of(context!).size.width,
          height: height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: color,
          ),
          child: Center(
              child: Text(
            text,
            style: const TextStyle(
                color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
          ))),
    ),
  );
}

Widget cont55As(
    {BuildContext? context, dynamic text, IconData? icon, Color? color}) {
  return Padding(
    padding: const EdgeInsets.only(top: 10, bottom: 5, left: 35, right: 35),
    child: Container(
      width: MediaQuery.of(context!).size.width,
      height: 60,
      padding: const EdgeInsets.all(10),
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(30), color: color),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(''),
          //  text1(text, 22, Colors.white),
          Icon(
            icon,
            color: Colors.white,
            size: 27,
          ),
        ],
      ),
    ),
  );
}

Widget longbuttons(String title, void Function()? onTap, Color color,
    {double width = 150}) {
  return Padding(
    padding: const EdgeInsets.only(top: 0.0),
    child: GestureDetector(
      onTap: onTap,
      child: Container(
        height: 45,
        width: width,
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(5)),
        child: Center(
          child: Text(
            title.tr,
            style: const TextStyle(color: Colors.white, fontSize: 14),
          ),
        ),
      ),
    ),
  );
}

Widget shortButton(String title, void Function()? onTap, Color color,
    {double fontSize = 16.0, double width = 130.0, double height = 45.0}) {
  return Padding(
    padding: const EdgeInsets.only(top: 0.0),
    child: GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(5)),
        child: Center(
          child: Text(
            title.tr,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: fontSize),
          ),
        ),
      ),
    ),
  );
}
