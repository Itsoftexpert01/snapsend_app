import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
class AboutScreen extends StatefulWidget {
  AboutScreen({Key? key}) : super(key: key);

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  TextEditingController? email = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: buttonColor,
          elevation: 0.0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              pops(context);
            },
          ),
          centerTitle: true,
          title: Text(
            'About'.tr,
            style: const TextStyle(fontSize: 24),
          ),  actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0, left: 20.0),
                  child: Text(
                    'About us'.tr,
                    textAlign: TextAlign.start,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 30),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
                child: Text(
                  'Snap Send Report was created to establish a fast and simple emergency information delivery system for the community to work directly with law enforcement with the goal to help curb the rise in crime and keep our communities safe.'
                      .tr,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(fontSize: 14, letterSpacing: 0.5),
                ),
              ),

              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                  child: Text(
                    'Our approach'.tr,
                    textAlign: TextAlign.start,
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
                child: Text(
                  'People need a fast and easy way to deliver information to the police. The Snap Send system empowers the people in safety with a fast delivery system to aid the police. At the same time to tools it gives Law Enforcement cross borders and connect the world, so the criminals have no place to hide. The power of fast information delivery in your hand. Transfer files, pictures, videos, or voice recording to the needed departments. Search Sex Offenders in your area. Provides an international Missing Persons Database, that includes Native American and Indigenous People. Provides an International Person of Interest Database to track crime around the world. An Emergency Public Notice System, with an Amber Alert forwarding system. Track and add more information to a tip after it has been sent. Additional information can be delivered to the past tip. At Snap Send Report we support the positive interaction between Law Enforcement and their communities.'
                      .tr,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(fontSize: 14, letterSpacing: 0.5),
                ),
              ),

              // Padding(
              //   padding: const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
              //   child: textformfield1(
              //       controller: email,
              //       icon: Icons.email,
              //       hinttext: 'Enter email address'),
              // ),

              // Padding(
              //     padding: const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
              //     child: longbuttons('Send instructions', () {}, buttonColor,
              //         width: 280)),

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Padding(
              //       padding: const EdgeInsets.only(top: 18.0),
              //       child: text3("Remember the password? ", 12.5, Colors.grey),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(top: 18.0),
              //       child: text3("Log in here", 12.5, buttonColor),
              //     ),
              //   ],
              // ),

              // longbuttons('Login', () {}, primaryColor),
              // longbuttons('Register', () {}, Colors.black),
            ],
          ),
        ));
  }
}
