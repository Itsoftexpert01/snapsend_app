import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
class HowItWorks extends StatefulWidget {
  HowItWorks({Key? key}) : super(key: key);

  @override
  State<HowItWorks> createState() => _HowItWorksState();
}

class _HowItWorksState extends State<HowItWorks> {
  TextEditingController? email = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: buttonColor,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            pops(context);
          },
        ),
        centerTitle: true,
        title:  Text(
          'How it works'.tr,
          style: TextStyle(fontSize: 24),
        ),  actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],
      ),
      body: Center(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
           
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 120,
                width: 120,
                child: Image.asset(
                  'assets/snapsend_logo.png',
                  // fit: BoxFit.fill,
                ),
              ),
            ),
             Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: Text(
                'How it works'.tr,
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            SizedBox(height: 16),
            buildStep(
              stepNumber: 'Step 1',
              title: 'Set Up',
              content:
                  'Download the app to your phone or computer and set up your account.',
            ),
            buildStep(
              stepNumber: 'Step 2',
              title: 'Select Departments',
              content:
                  'Choose the Police or Sheriff\'s Office nearest to your location.',
            ),
            buildStep(
              stepNumber: 'Step 3',
              title: 'Witness a Crime',
              content:
                  'Capture videos, pictures, or voice recordings of crimes you witness.',
            ),
            buildStep(
              stepNumber: 'Step 4',
              title: 'Sending a Tip',
              content:
                  'Open the Snap Send Report app or access your account on your computer. Select the department, provide tip information, attach media, and send.',
            ),
          ],
        ),
      ),
    );
  }

  Widget buildStep(
      {required String stepNumber,
      required String title,
      required String content}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          stepNumber.tr,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: buttonColor
          ),
        ),
        SizedBox(height: 8),
        Text(
          title.tr,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 8),
        Padding(
          padding: const EdgeInsets.only(left: 30.0, right: 30.0),
          child: Text(
            content.tr,
            textAlign: TextAlign.justify,
            style: TextStyle(),
          ),
        ),
        // SizedBox(height: 16),
      ],
    );
  }
}
