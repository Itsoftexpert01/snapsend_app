import 'package:flutter/material.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';

// ignore: must_be_immutable
class EndorsementDetails extends StatefulWidget {
  dynamic title, img, details, time;
  EndorsementDetails({Key? key, this.title, this.img, this.details, this.time})
      : super(key: key);

  @override
  State<EndorsementDetails> createState() => _PublicNoticeDetailsState();
}

class _PublicNoticeDetailsState extends State<EndorsementDetails> {
  TextEditingController? email = TextEditingController();

  String removeHtmlAndExtraSpaces(String htmlString) {
    // Remove HTML tags
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);
    String withoutHtmlTags = htmlString.replaceAll(exp, '');

    // Replace &nbsp; with a regular space
    String withoutNbsp = withoutHtmlTags.replaceAll('&nbsp;', ' ');

    // Remove extra spaces
    String withoutExtraSpaces = withoutNbsp.replaceAll(RegExp(r'\s+'), ' ');

    // Trim leading and trailing spaces
    return withoutExtraSpaces.trim();
  }

  @override
  Widget build(BuildContext context) {
    String htmlString = widget.details;
    String plainText = removeHtmlAndExtraSpaces(htmlString);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: buttonColor,
          elevation: 0.0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              pops(context);
            },
          ),
          centerTitle: true,
          title: Text(
            '${widget.title}',
            style: const TextStyle(fontSize: 24),
          ),
          actions: [
            IconButton(
                onPressed: () {
                  navPushReplacement(context, MyBottomNavigationBar());
                },
                icon: const Icon(CupertinoIcons.home))
          ],
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // const Padding(
                //   padding: EdgeInsets.only(top: 40.0),
                //   child: Text(
                //     'Login Account',
                //     textAlign: TextAlign.center,
                //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                //   ),
                // ),

                widget.img == null
                    ? Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: SizedBox(
                          height: 120,
                          width: 120,
                          child: Image.network(
                            'assets/snapsend_logo.png',
                            // fit: BoxFit.fill,
                          ),
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: SizedBox(
                          height: 300,
                          width: 300,
                          child: Image.network(
                            '${widget.img}',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 18.0, left: 24, right: 24),
                  child: Text(
                    '${widget.title}',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, right: 240),
                  child: Text(
                    widget.time.toString(),
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                ),
                Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, left: 30, right: 25),
                    child: Text(
                      plainText,
                      textAlign: TextAlign.justify,
                      style: const TextStyle(fontSize: 16),
                    )

                    //  Html(
                    //   data: '${widget.details}',
                    // ),
                    ),
              ],
            ),
          ),
        ));
  }
}
