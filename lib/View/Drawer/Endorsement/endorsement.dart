import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Drawer/Endorsement/endorsement_details.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class EndorsementScreen extends StatefulWidget {
  @override
  _EndorsementScreenState createState() => _EndorsementScreenState();
}

class _EndorsementScreenState extends State<EndorsementScreen> {
  dynamic endorsements;
  List departmentList = [];

  bool isloading = false;

  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  var formattedDate;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    log(user.toString());
    await getNotices();
    final DateTime currentDate = DateTime.now();
    final DateFormat formatter = DateFormat('EEEE, d MMMM y', 'en_US');

    formattedDate = formatter.format(currentDate);
    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getuserDetails();
  }

  getNotices() async {
    isloading = true;
    setState(() {});

    var response = await Controller().webEntity({"type": "endorsement"});

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      endorsements = response['data']['app_entity'];
    }

    isloading = false;
    setState(() {});
  }

  //Popular Widget
  Widget popularWidget(title, subtitle, time, like, image) => Container(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            //set border radius more than 50% of height and width to make circle
          ),
          elevation: 3.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(top: 4, bottom: 4),
                    height: 100,
                    width: 90,
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.elliptical(12, 12)),
                        image: DecorationImage(
                            image: NetworkImage(image), fit: BoxFit.cover))),
                Padding(
                  padding: const EdgeInsets.only(left: 25.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 180,
                          child: Text(
                            title,
                            style: TextStyle(
                              overflow: TextOverflow.ellipsis,
                              fontSize: 16,
                              color: buttonColor,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 11,
                        ),
                        SizedBox(
                          width: 180,
                          child: Text(
                            subtitle,
                            style: const TextStyle(
                                color: Colors.black,
                                overflow: TextOverflow.ellipsis,
                                fontWeight: FontWeight.w800,
                                fontSize: 14),
                          ),
                        ),
                        const SizedBox(
                          height: 11,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              time.toString(),
                              style: const TextStyle(fontSize: 14),
                            ),
                            const Padding(padding: EdgeInsets.only(left: 12)),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: buttonColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            pops(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios_new,
          ),
          color: Colors.white,
        ),
        title: text2('Endorsement', 18, Colors.white),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : SafeArea(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 0, left: 25, right: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        //  getImage(),
                        const SizedBox(
                          height: 0,
                        ),

                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Endorsement".tr,
                              style: const TextStyle(
                                  fontSize: 27, fontWeight: FontWeight.w700),
                            ),
                            Text(
                              "",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  color: blue),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        endorsements.length == 0
                            ? const Center(
                                child: Text(
                                'No Endorsement Found',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black),
                              ))
                            : ListView.builder(
                                physics: const ScrollPhysics(),
                                itemCount: endorsements.length,
                                shrinkWrap: true,
                                itemBuilder: ((context, i) {
                                  return InkWell(
                                      onTap: () {
                                        nav(
                                            context,
                                            EndorsementDetails(
                                              title:
                                                  "${endorsements[i]['title']}",
                                              img:
                                                  "${endorsements[i]['featured_img_url']}",
                                              details:
                                                  "${endorsements[i]['desc']}",
                                                  time: endorsements[i]['updated_at']
                                            .toString()
                                            .split("T")[0],
                                            ));
                                      },
                                      child: popularWidget(
                                        "${endorsements[i]['title']}",
                                        "${endorsements[i]['short_desc']}",
                                        endorsements[i]['updated_at']
                                            .toString()
                                            .split("T")[0],
                                        "${endorsements[i]['view_by']}",
                                        "${endorsements[i]['featured_img_url']}",
                                      ));
                                })),

                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
