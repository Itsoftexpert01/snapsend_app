import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/View/Auth/login_screen.dart';
import 'package:snap_send_app/View/Drawer/about_snapsend.dart';
import 'package:snap_send_app/View/Drawer/Endorsement/endorsement.dart';
import 'package:snap_send_app/View/Drawer/help.dart';
import 'package:snap_send_app/View/Drawer/how_it_works.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

class DrawerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.7, // Set your desired width
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: buttonColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 70,
                          width: 70,
                          child: Image.asset(
                            'assets/snapsend_logo.png',
                            // fit: BoxFit.fill,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                         Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 0.0),
                              child: Text(
                                'SnapSend Report'.tr,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.white),
                              ),
                            ),
                            SizedBox(width: 150,
                              child: Padding(
                                padding: EdgeInsets.only(top: 5.0),
                                child: Text(
                                  'For Safer Communities'.tr,
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            buildDrawerOption(
              icon: CupertinoIcons.info,
              title: 'About SnapSend',
              onTap: () {
                nav(context, AboutScreen());
                // Add your logic for this option
              },
            ),
            buildDrawerOption(
              icon: CupertinoIcons.settings,
              title: 'How it works',
              onTap: () {
                nav(context, HowItWorks());

                // Add your logic for this option
              },
            ),
            buildDrawerOption(
              icon: CupertinoIcons.hand_thumbsup,
              title: 'Endorsement',
              onTap: () {
                nav(context, EndorsementScreen());

                // Add your logic for this option
              },
            ),
            buildDrawerOption(
              icon: CupertinoIcons.hand_draw,
              title: 'Help',
              onTap: () {
                nav(context, HelpScreen());
                // Add your logic for this option
              },
            ),
            buildDrawerOption(
              icon: CupertinoIcons.lock,
              title: 'Logout',
              onTap: () async {
                await Storage.setToken('null');
                await Storage.setLogin('null');
                await Storage.logout();
                // ignore: use_build_context_synchronously
                navandClear(context, LoginScreen());

                // Add your logic for this option
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDrawerOption(
      {icon, required String title, required VoidCallback onTap}) {
    return ListTile(
      horizontalTitleGap: 0,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20),
      leading: Icon(icon),
      title: Text("$title".tr),
      onTap: onTap,
    );
  }
}
