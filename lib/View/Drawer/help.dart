import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Auth/OTP/otp_screen.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
class HelpScreen extends StatefulWidget {
  HelpScreen({Key? key}) : super(key: key);

  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  bool isloading = false;
  TextEditingController? emailaddress = TextEditingController();
  TextEditingController? firstname = TextEditingController();
  TextEditingController? lastname = TextEditingController();
  TextEditingController? username = TextEditingController();
  TextEditingController? phoneno = TextEditingController();

  TextEditingController? password = TextEditingController();
  TextEditingController? confirmpassword = TextEditingController();
  // ignore: prefer_typing_uninitialized_variables
  var countries;

  var states;
  var cities;
  List<String> countriesList = [];
  List<String> countriesStates = [];

  List<String> countriesCities = [];

  dynamic userdetails;
  List departmentList = [];

  getCustomer(context) async {
    isloading = true;
    setState(() {});
    var data = {};

    var response = await Controller().getCustomer(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      userdetails = response['data']['user_details'];

      emailaddress!.text = userdetails['email'];
      firstname!.text = userdetails['first_name'];
      lastname!.text = userdetails['last_name'];
      username!.text = userdetails['username'];
      phoneno!.text = userdetails['mobile'];
      countryIndex = int.parse(userdetails['country']);
      stateIndex = int.parse(userdetails['state']);
      cityIndex = int.parse(userdetails['city']);
      // selectedCountry = countries[countryIndex]['name'];
      // selectedState = states[countryIndex]['name'];
      // selectedCity = cities[countryIndex]['name'];
    }

    isloading = false;
    setState(() {});
  }

//getCountriesList
  getCountries() async {
    // isloading = true;
    // setState(() {});
    var data = await Controller().getAllCountries();
    countries = data['data']['app_countries'];
    for (int i = 0; i < countries.length; i++) {
      countriesList.add(countries[i]['name']);
    }
    await getStates(231);
    selectedCountry = "United States";
    countryIndex = 230;
    // isloading = false;
    setState(() {});
  }

//getStatesList
  getStates(id) async {
    countriesStates = [];
    isloading = true;
    setState(() {});
    var data = {"country": id};
    var resp = await Controller().getAllStates(data);
    states = resp['data']['app_states'];
    for (int i = 0; i < states.length; i++) {
      countriesStates.add(states[i]['name']);
    }
    isloading = false;
    setState(() {});
  }

//getCitiesList
  getCities(id) async {
    countriesCities = [];
    isloading = true;
    setState(() {});
    var data = {"state": id};
    var resp = await Controller().getAllCities(data);
    cities = resp['data']['app_cities'];
    for (int i = 0; i < cities.length; i++) {
      countriesCities.add(cities[i]['name']);
    }
    isloading = false;
    setState(() {});
  }

  sendMessage(context) async {
    var data = {
      "email": email.text.trim(),
      "message": message.text.trim(),
      "name": name.text.trim(),
      "subject": "Query",
    };
    log(data.toString());
    isloading = true;
    setState(() {});
    var resp = await Controller().contactUs(data);
    if (resp['succ'] == 0) {
      showToast(resp['errors']);
    } else {
      showToast(resp['msg']);

      //pop(context);
    }
    isloading = false;
    setState(() {});
  }

  bool validateStructure(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = RegExp(pattern);
    return regExp.hasMatch(value);
  }

  FocusNode? _focusNode;
  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    // getCountries();
    // getCustomer(context);
  }

  @override
  void dispose() {
    _focusNode?.dispose();
    super.dispose();
  }

  String? selectedCountry;
  String? selectedState;
  String? selectedCity;
  String? initialCode;

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();

  TextEditingController subject = TextEditingController();

  TextEditingController message = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
       // backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'Contact Us'.tr,
          textAlign: TextAlign.center,
          style: const TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20,
              
              // color: Colors.black
               
               ),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
           // color: Colors.black,
          ),
          onPressed: () {
            pops(context);
          },
        ),  actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],
      ),
      // backgroundColor: primaryColor,
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        progressIndicator: spinkit,
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: SizedBox(
                    height: 150,
                    width: 150,
                    child: Image.asset(
                      'assets/snapsend_logo.png',
                      // fit: BoxFit.fill,
                    ),
                  ),
                ),
                Padding(
                    padding:
                        const EdgeInsets.only(left: 60.0, top: 10, right: 60.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  'Send Us a Message'.tr,
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                                child: Text(
                                  'Your Name (required)'.tr,
                                  style: const TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: textformfield1(
                                    focusNode: _focusNode,
                                    controller: name,
                                    onTap: () {
                                      log("im hitted");
                                      FocusScope.of(context)
                                          .requestFocus(_focusNode);
                                      log("im hitted after fouccs");
                                      setState(() {});
                                    },
                                    hinttext: 'Enter your name'),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                                child: Text(
                                  'Your email (required)'.tr,
                                  style: const TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: textformfield1(
                                    controller: email,
                                    hinttext: 'Enter your Email'),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                                child: Text(
                                  'Subject (required)'.tr,
                                  style: const TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: textformfield1(
                                    controller: subject,
                                    hinttext: 'Enter your Subject'),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                                child: Text(
                                  'Your Message (required)'.tr,
                                  style: const TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: textformfield2(
                                    controller: message,
                                    hinttext: 'Enter your Message'),
                              ),

                              const SizedBox(height: 10),

              
                            ]))),  Padding(
            padding: const EdgeInsets.only(
                top: 10.0, left: 60, right: 60.0, bottom: 28),
            child: longbuttons('Submit', () {
              if (name.text.isEmpty ||
                  email.text.isEmpty ||
                  message.text.isEmpty) {
                showToast("Please fill all the fields first");
              } else {
                sendMessage(context);
              }
            }, buttonColor, width: 300)),
              ],
            ),
          ),
        ),
      ),
      
    );
  }

  var countryIndex = 0;
  var stateIndex = 0;
  var cityIndex = 0;
  String? selectedValue;
  dropdownbutton(txt, List<String> list, value) {
    return DropdownButtonFormField2<String>(
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),

        // Add more decoration..
      ),
      hint: Text(
        value == null ? '$txt' : '$value',
        style: const TextStyle(fontSize: 14, color: Colors.black),
      ),
      items: list
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(fontSize: 14, color: Colors.black),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select gender.';
        }
        return null;
      },
      onChanged: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        if (txt == "Country") {
          selectedCountry = selectedValue;
          countryIndex = countriesList.indexOf(value.toString());

          log(countries[countryIndex].toString());
          getStates(countries[countryIndex]['id']);
        } else if (txt == "State") {
          selectedState = selectedValue;
          stateIndex = countriesStates.indexOf(value.toString());

          log(countriesStates[stateIndex].toString());
          getCities(states[stateIndex]['id']);
        } else if (txt == "City") {
          selectedCity = selectedValue;
          cityIndex = countriesCities.indexOf(selectedCity.toString());
        }

        //Do something when selected item is changed.
      },
      onSaved: (value) {
        selectedValue = value.toString();
        print(selectedValue.toString());

        // var index = countriesList.indexOf(value.toString());
        // print(index.toString());
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
    );
  }
}
