
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/PublicNotice/public_notice_details.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class AmberAlert extends StatefulWidget {
  @override
  _AmberAlertState createState() => _AmberAlertState();
}

class _AmberAlertState extends State<AmberAlert> {
  dynamic notices;
  List departmentList = [];

  bool isloading = false;

  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  var formattedDate;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    // log(user.toString());
    await getNotices();
    final DateTime currentDate = DateTime.now();
    final DateFormat formatter = DateFormat('EEEE, d MMMM y', 'en_US');

    formattedDate = formatter.format(currentDate);
    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getuserDetails();
  }

  getNotices() async {
    isloading = true;
    setState(() {});
    var data = {
      "type": "amber_alert",
      "role": "5",
      "country": user['country'],
      "state": user['state'],
      "city": user['city'],
    };

    var response = await Controller().getNotice(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      notices = response['data']['list'];
    }

    isloading = false;
    setState(() {});
  }

  //Popular Widget
  Widget popularWidget(title, subtitle, time, like, image) => Container(
          child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          //set border radius more than 50% of height and width to make circle
        ),
        elevation: 3.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(top: 4, bottom: 4),
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.all(Radius.elliptical(12, 12)),
                      image: DecorationImage(
                          image: NetworkImage(image), fit: BoxFit.fill))),
              Padding(
                padding: const EdgeInsets.only(left: 25.0),
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "$title",
                          style: TextStyle(
                            fontSize: 17,
                            color: buttonColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        const SizedBox(
                          height: 11,
                        ),
                        Text(
                          "${subtitle}",
                          style: const TextStyle(
                              color: Colors.black,
                              overflow: TextOverflow.ellipsis,
                              fontWeight: FontWeight.w800,
                              fontSize: 17),
                        ),
                        Row(
                          children: [
                            IconButton(
                                icon: Icon(
                                  Icons.calendar_month,
                                  size: 22,
                                  color: buttonColor,
                                ),
                                onPressed: null),
                            Text(
                              "${time}",
                              style: const TextStyle(fontSize: 14),
                            ),
                          ],
                        ),
                      ]),
                ),
              )
            ],
          ),
        ),
      ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: buttonColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            pops(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios_new,
          ),
          color: Colors.white,
        ),
        title: text2('Amber Alert', 18, Colors.white),
       actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],
    
      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : SafeArea(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, left: 25, right: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        //  getImage(),
                        // const SizedBox(
                        //   height: 0,
                        // ),

                        // const SizedBox(
                        //   height: 20,
                        // ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //   children: <Widget>[
                        //     Text(
                        //       "Amber Alert".tr,
                        //       style: TextStyle(
                        //           fontSize: 27, fontWeight: FontWeight.w700),
                        //     ),
                        //     Text(
                        //       "View all".tr,
                        //       style: TextStyle(
                        //           fontSize: 16,
                        //           fontWeight: FontWeight.w700,
                        //           color: blue),
                        //     ),
                        //   ],
                        // ),
                        // const SizedBox(
                        //   height: 20,
                        // ),

                        notices.length == 0
                            ? Center(
                                child: Text(
                                'No Amber Alerts Found at this time'.tr,
                                // ignore: prefer_const_constructors
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black),
                              ))
                            : ListView.builder(
                                physics: const ScrollPhysics(),
                                itemCount: notices.length,
                                shrinkWrap: true,
                                itemBuilder: ((context, i) {
                                  return InkWell(
                                      onTap: () {
                                        nav(
                                            context,
                                            PublicNoticeDetails(
                                                img:"${notices[i]['alert_image_link']}",
                                                details: "${notices[i]['message']}"));
                                      },
                                      child: popularWidget(
                                        "${notices[i]['title']}",
                                        "${notices[i]['message']}",
                                        "${notices[i]['time_of_notice']}",
                                        "${notices[i]['view_by']}",
                                        "${notices[i]['alert_image_link']}",
                                      ));
                                })),

                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
