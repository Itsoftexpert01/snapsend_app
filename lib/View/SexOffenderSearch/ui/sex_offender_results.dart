import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:snap_send_app/View/Notification/utils/widgets.dart';
import 'package:snap_send_app/View/SexOffenderSearch/ui/suspect_details_page.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';

class SexOffenderResults extends StatefulWidget {
  final dynamic results;
  final String? selectedCountry;
  final String? selectedState;
  final String? selectedCity;
  // countryId,
  // stateId,
  // cityId;

  SexOffenderResults({
    required this.results,
    required this.selectedCountry,
    required this.selectedState,
    required this.selectedCity,
    // required this.countryId,
    // required this.stateId,
    // required this.cityId
  });

  @override
  _SexOffenderResultsState createState() => _SexOffenderResultsState();
}

class _SexOffenderResultsState extends State<SexOffenderResults> {
  List<dynamic> departmentList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    departmentList = widget.results;
  }

  //Popular Widget
  Widget popularWidget(title, subtitle, time, like, image) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 3.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 4, bottom: 4),
                height: 100,
                width: 90,
                decoration: BoxDecoration(
                  borderRadius:
                      const BorderRadius.all(Radius.elliptical(12, 12)),
                  image: DecorationImage(
                    image: NetworkImage(image),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: 180,
                        child: Text(
                          title,
                          style: TextStyle(
                            overflow: TextOverflow.ellipsis,
                            fontSize: 16,
                            color: buttonColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 11,
                      ),
                      SizedBox(
                        width: 180,
                        child: Text(
                          subtitle,
                          style: const TextStyle(
                            color: Colors.black,
                            overflow: TextOverflow.ellipsis,
                            fontWeight: FontWeight.w800,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 11,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            time,
                            style: const TextStyle(fontSize: 14),
                          ),
                          const Padding(padding: EdgeInsets.only(left: 12)),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    log(departmentList.toString());
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: buttonColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            pops(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios_new,
          ),
          color: Colors.white,
        ),
        title: text2('Search', 18, Colors.white),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 0, left: 25, right: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Search Result For ${widget.selectedCountry} - \n${widget.selectedState}",
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    "${departmentList.length} Result Found",
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  departmentList.isEmpty
                      ? const Center(
                          child: Text(
                            'No Result Found',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Colors.black,
                            ),
                          ),
                        )
                      : ListView.builder(
                          physics: const ScrollPhysics(),
                          itemCount: departmentList.length,
                          shrinkWrap: true,
                          itemBuilder: ((context, i) {
                            return InkWell(
                              onTap: () {
                                nav(
                                  context,
                                  SuspectDetailsPage(
                                      suspectData: departmentList[i]),
                                );
                              },
                              child: popularWidget(
                                "${departmentList[i]['first_name']??""}",
                                "Date : ${departmentList[i]['date']??""}",
                                "Time : ${departmentList[i]['time']??""}",
                                "${departmentList[i]['more_data']}",
                                "${departmentList[i]['profile_pic_url'] == "" ? "https://snapsendreport.com//web/images/logo.png" : departmentList[i]['profile_pic_url']}",
                              ),
                            );
                          }),
                        ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
