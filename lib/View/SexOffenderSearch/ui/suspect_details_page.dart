import 'package:flutter/material.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';

class SuspectDetailsPage extends StatelessWidget {
  // Define a variable to store the suspect data fetched from the API
  final Map<String, dynamic> suspectData; // Replace with actual data from API

  SuspectDetailsPage({required this.suspectData}); // Pass the fetched data here

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: buttonColor,
        leading: IconButton(
            onPressed: () {
              pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios)),
        title: const Text('Suspect details'),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: const Text('Name'),
                subtitle: Text(
                  '${suspectData['first_name'] ?? ""} ${suspectData['last_name'] ?? ""}',
                ),
              ),
              ListTile(
                title: const Text('Date of Birth'),
                subtitle: Text("${suspectData['dob'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Resident Address'),
                subtitle: Text("${suspectData['resident_address'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Gender'),
                subtitle: Text("${suspectData['gender'] ?? ""}"),
              ),
              ListTile(
                title: const Text('City'),
                subtitle: Text("${suspectData['city_id'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Email'),
                subtitle: Text("${suspectData['email'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Phone'),
                subtitle: Text("${suspectData['phone'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Blood Type'),
                subtitle: Text("${suspectData['bloodtype'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Height'),
                subtitle: Text("${suspectData['height'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Weight'),
                subtitle: Text("${suspectData['weight'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Eye Color'),
                subtitle: Text("${suspectData['eye_color'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Hair Color'),
                subtitle: Text("${suspectData['hair_color'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Piercing'),
                subtitle: Text("${suspectData['plercing'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Glasses'),
                subtitle: Text(
                    suspectData['glasses'].toString() == '1' ? 'Yes' : 'No'),
              ),
              ListTile(
                title: const Text('Race'),
                subtitle: Text("${suspectData['race'] ?? ""}"),
              ),
              ListTile(
                title: const Text('Is Sex Offender'),
                subtitle: Text(suspectData['is_sex_offender'].toString() == '1'
                    ? 'Yes'
                    : 'No'),
              ),
              ListTile(
                title: const Text('Social Link'),
                subtitle:
                    Text("${suspectData['meta_data']['social_link'] ?? ""}"),
              ),
              // Add more ListTile widgets for other details
              // You can customize the layout and styling as needed
            ],
          ),
        ),
      ),
    );
  }
}
