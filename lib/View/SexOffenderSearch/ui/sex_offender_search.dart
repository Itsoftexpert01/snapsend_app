import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:map_location_picker/map_location_picker.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/functions.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/SexOffenderSearch/ui/sex_offender_results.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class SexOffenderScreen extends StatefulWidget {
  const SexOffenderScreen({super.key});

  @override
  State<SexOffenderScreen> createState() => _SexOffenderScreenState();
}

class _SexOffenderScreenState extends State<SexOffenderScreen> {
  dynamic scars;
  dynamic tattoos;
  dynamic language;
  dynamic eyecolor;
  dynamic haircolor;
  dynamic weight;
  dynamic height;
  dynamic race;
  dynamic age;
  String? scarsid;
  String? tattoosid;
  String? languageid;
  String? eyecolorid;
  String? haircolorid;
  String? weightid;
  String? offenderLevelId;

  String? heightid;
  String? raceid;
  String? ageid;
  String? genderid;
  List<String> scarsList = [];
  List<String> tattoosList = [];
  List<String> languageList = [];
  List<String> eyecolorList = [];
  List<String> haircolorList = [];
  List<String> weightList = [];
  List<String> heightList = [];
  List<String> raceList = [];
  List<String> ageList = [];

  List<String> scarsListids = [];
  List<String> tattoosListids = [];
  List<String> languageListids = [];
  List<String> eyecolorListids = [];
  List<String> haircolorListids = [];
  List<String> weightListids = [];
  List<String> heightListids = [];
  List<String> raceListids = [];

  String selectedscars = "Select";
  String selectedtattoos = "Select";
  String selectedlanguage = "Select";
  String selectedeyecolor = "Select";
  String selectedhaircolor = "Select";
  String selectedweight = "Select";
  String selectedheight = "Select";
  String selectedrace = "Select";
  String selectedage = "Select";
  bool isloading = false;

  dynamic departments;
  List<String> departmentList = [];
  String selectedDepartment = "Select";
  getDepartments(context) async {
    isloading = true;
    setState(() {});
    var data = {"department_type": 1};

    var response = await Controller().getDepartment(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      departments = response['data'];
      for (int i = 0; i < departments.length; i++) {
        departmentList.add(departments['user_department_in_array'][i]);
      }
    }

    isloading = false;
    setState(() {});
  }

  getEntities(context) async {
    isloading = true;
    setState(() {});
    var scarresp = await Controller().webEntity({"type": "scars"});

    scars = scarresp['data']['app_entity'];
    for (int i = 0; i < scars.length; i++) {
      scarsList.add(scars[i]['title']);
    }
    var tattoosresp = await Controller().webEntity({"type": "tattoos"});

    tattoos = tattoosresp['data']['app_entity'];
    for (int i = 0; i < tattoos.length; i++) {
      tattoosList.add(tattoos[i]['title']);
    }
    var languagesresp = await Controller().webEntity({"type": "language"});

    language = languagesresp['data']['app_entity'];
    for (int i = 0; i < language.length; i++) {
      languageList.add(language[i]['title']);
      languageListids.add(language[i]['id'].toString());
    }
    var eyecoloresp = await Controller().webEntity({"type": "eye-color"});

    eyecolor = eyecoloresp['data']['app_entity'];
    for (int i = 0; i < eyecolor.length; i++) {
      eyecolorList.add(eyecolor[i]['title']);
      eyecolorListids.add(eyecolor[i]['id'].toString());
    }
    var haircoloresp = await Controller().webEntity({"type": "hair-color"});
    haircolor = haircoloresp['data']['app_entity'];
    for (int i = 0; i < haircolor.length; i++) {
      haircolorList.add(haircolor[i]['title']);
      haircolorListids.add(haircolor[i]['id'].toString());
    }
    var weightresp = await Controller().webEntity({"type": "weight"});

    weight = weightresp['data']['app_entity'];
    for (int i = 0; i < weight.length; i++) {
      weightList.add(weight[i]['title']);
      weightListids.add(weight[i]['id'].toString());
    }
    var heightresp = await Controller().webEntity({"type": "height"});

    height = heightresp['data']['app_entity'];
    for (int i = 0; i < height.length; i++) {
      heightList.add(height[i]['title']);
      heightListids.add(height[i]['id'].toString());
    }
    var raceresp = await Controller().webEntity({"type": "race"});

    race = raceresp['data']['app_entity'];
    for (int i = 0; i < race.length; i++) {
      raceList.add(race[i]['title']);
      raceListids.add(race[i]['id'].toString());
    }
    var ageresp = await Controller().webEntity({"type": "member_age"});
    print("ageresp is $ageresp");
    age = ageresp['data']['app_entity'];
    for (int i = 10; i < 100; i++) {
      ageList.add("$i Years");
      // ageList.add(age[i]['title']);
    }
    //  await getDepartments(context);
    await getCountries();
    isloading = false;
    setState(() {});
  }

  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();

    log(user.toString());
    // isloading = false;
    //  setState(() {});
  }

  var countries;
  List<String> countriesList = [];
  String selectedCountry = "Country";
  getCountries() async {
    selectedCountry = "Country";
    countries = null;
    countriesList = [];
    statesList = [];
    citiesList = [];

    var response = await Functions().getCountries();
    countries = response['countries'];
    countriesList = response['countriesList'];

    await getStates(231);
    selectedCountry = "United States";
    countryIndex = 230;
    countryid = "231";
    log("selectedCountry is $selectedCountry");
  }

  var states;
  List<String> statesList = [];
  String selectedstate = "State";
  getStates(id) async {
    selectedstate = "State";
    statesList = [];
    citiesList = [];
    isloading = true;
    setState(() {});
    var response = await Functions().getStates(id);
    states = response['states'];
    statesList = response['statesList'];
    isloading = false;
    setState(() {});
  }

  var cities;
  List<String> citiesList = [];
  String selectedCity = "Selected";
  getCities(id) async {
    citiesList = [];
    selectedCity = "Selected";
    isloading = true;
    setState(() {});
    var response = await Functions().getCities(id);
    cities = response['cities'];
    citiesList = response['citiesList'];
    isloading = false;
    setState(() {});
  }

  var levelOffender;
  List<String> levelList = [
    'Low Risk to Reoffend',
    'Medium Risk to Reoffend',
    'High Risk to Reoffend'
  ];
  String selectedlevel = "Selected";
  @override
  void initState() {
    super.initState();
    getEntities(context);
    getuserDetails();
  }

  final GlobalKey<FormFieldState<String>> _dropdownKey =
      GlobalKey<FormFieldState<String>>();
  final GlobalKey<FormFieldState<String>> _dropdownKey2 =
      GlobalKey<FormFieldState<String>>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: buttonColor,
        leading: IconButton(
            onPressed: () {
              pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios)),
        title: Text('Sex Offender Search'.tr),
        actions: [
          // GestureDetector(
          //   onTap: () {
          //     selectedCity = "Selected";
          //   //  selectedstate = "State";
          //     // cityIndex = 0;
          //     // stateIndex = 0;
          //     //   countries ="";
          //     //_dropdownKey.currentState?.reset(); // Reset the selected value
          //     _dropdownKey2.currentState?.reset();
          //     selectedscars = "Select";
          //     selectedtattoos = "Select";
          //     selectedlanguage = "Select";
          //     selectedeyecolor = "Select";
          //     selectedhaircolor = "Select";
          //     selectedweight = "Select";
          //     selectedheight = "Select";
          //     selectedrace = "Select";
          //     selectedage = "Select";
          //     scarsid = "";
          //     tattoosid = "";
          //     languageid = "";
          //     eyecolorid = "";
          //     haircolorid = "";
          //     weightid = "";
          //     offenderLevelId = "";

          //     heightid = "";
          //     raceid = "";
          //     ageid = "";
          //     genderid = "";
              
          //     setState(() {});
          //   },
          //   child: Chip(
          //     backgroundColor: primaryColor,
          //     label: Row(
          //       children: [
          //         Icon(CupertinoIcons.refresh),
          //         SizedBox(
          //           width: 5,
          //         ),
          //         Text(
          //           'Reset'.tr,
          //           style: TextStyle(color: Colors.white),
          //         ),
          //       ],
          //     ),
          //   ),
          // ),
          // const SizedBox(
          //   width: 10,
          // ),
          // IconButton(
          //     onPressed: () {
          //       navPushReplacement(context, MyBottomNavigationBar());
          //     },
          //     icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        progressIndicator: spinkit,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: SizedBox(
            // height: height(context),
            // width: width(context),
            child: ListView(
              children: [
                dropdown(context),
              ],
            ),
          ),
        ),
      ),
      persistentFooterButtons: [],
    );
  }

  final _formKey = GlobalKey<FormState>();

  TextEditingController sexOffenderdetailsController = TextEditingController();
  TextEditingController searchNameController = TextEditingController();

  TextEditingController teethController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController scarsController = TextEditingController();
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1970, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        dobController.text = selectedDate.toString().split(" ")[0];
      });
    }
  }

  Widget dropdown(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      elevation: 3.0,
      child: Form(
        key: _formKey,
        child: ListView(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 100,
                width: 100,
                child: Image.asset(
                  'assets/snapsend_logo.png',
                  // fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // const Padding(
                  //   padding: EdgeInsets.only(top: 8.0),
                  //   child: Text(
                  //     'Personal information',
                  //     style:
                  //         TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  const SizedBox(height: 10),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Country'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Country', countriesList, selectedCountry),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'State, Province, Territory'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('State', statesList, selectedstate,
                      key: _dropdownKey),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'City, Town, Village'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('City', citiesList, selectedCity,
                      key: _dropdownKey2),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Search By Name'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: searchNameController,
                        icon: CupertinoIcons.person,
                        hinttext: 'Search Name'),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Gender'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Gender', genderItems, genderValue),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Age'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Age', ageList, selectedage),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'DOB'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        isreadonly: true,
                        onTap: () {
                          _selectDate(context);
                        },
                        controller: dobController,
                        icon: CupertinoIcons.calendar,
                        hinttext: 'DOB'),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Race'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Race', raceList, selectedrace),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Height'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Height', heightList, selectedheight),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Weight'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Weight', weightList, selectedweight),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Hair Color'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Hair', haircolorList, selectedhaircolor),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Eye Color'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Eye', eyecolorList, selectedeyecolor),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Teeth'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: teethController,
                        icon: CupertinoIcons.person_3,
                        hinttext: 'Enter..'),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Languages'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Languages', languageList, selectedlanguage),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Tattoos'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Tattoos', tattoosList, selectedtattoos),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Scars/Birthmarks'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: scarsController,
                        icon: CupertinoIcons.person_3,
                        hinttext: 'Enter..'),
                  ),
                  //  dropdownbutton('Scars', scarsList, selectedscars),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Resident Address'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        isreadonly: true,
                        onTap: () {
                          showPlacePicker();
                        },
                        controller: location,
                        icon: CupertinoIcons.location,
                        hinttext: 'Enter..'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Levels of Offender'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Level', levelList, selectedlevel),

                  Padding(
                    padding: const EdgeInsets.only(top: 28.0),
                    child: Center(
                        child: longbuttons('Submit', () {
                      getSexOffender(context);
                      // showToast("No Results Found");
                    }, buttonColor, width: 250)),
                  ),

                  const SizedBox(height: 30),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getSexOffender(context) async {
    isloading = true;
    setState(() {});

    var data =
        "?scountry=${countryid ?? ""}&sstate=${stateid ?? ""}&isSexOffenderVal=1&dob=${dobController.text}&name=${searchNameController.text}&gender=${genderid ?? ""}&member_age=${ageid == null ? "" : ageid.toString().split(" ")[0]}&race=${raceid ?? ""}&height=${heightid ?? ""}&weight=${weightid ?? ""}&hair_color=${haircolorid ?? ""}&eye_color=${eyecolorid ?? ""}&teeth=${teethController.text}&language=${languageid ?? ""}&tattoos=${tattoosid ?? ""}&scars=${scarsController.text}&res_address=${location.text}&risk_level=${offenderLevelId ?? ""}";
    print("SENDING THIS $data");
    var response = await Controller().getSexOffender(data);
    //  showToast(response.toString());
    log(response.toString());
    isloading = false;
    setState(() {});

    if (response['succ'] == false) {
      showToast("error");
    } else {
      showToast(response['msg'].toString());
      nav(
          context,
          SexOffenderResults(
            selectedCity: selectedCity,
            selectedCountry: selectedCountry,
            selectedState: selectedstate,
            results: response['data']['list']['data'],
          ));

      //  navPushReplacement(context, SplashScreen());
      // navigateAndRemove(context, MyBottomNavigationBar());
    }
  }

  TextEditingController location = TextEditingController();
  String address = "null";
  String autocompletePlace = "null";
  Prediction? initialValue;
  void showPlacePicker() async {
    TextEditingController searchLoc = TextEditingController();
    try {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Location'.tr),
            content: SizedBox(
              height: 300,
              width: 300,
              child: PlacesAutocomplete(
                controller: searchLoc,
                searchController: searchLoc,
                apiKey: Config.mapKey,
                searchHintText: "Search for a place".tr,
                mounted: mounted,
                hideBackButton: true,
                initialValue: initialValue,
                onSuggestionSelected: (value) {
                  setState(() {
                    autocompletePlace =
                        value.structuredFormatting?.mainText ?? "";
                    initialValue = value;
                  });
                },
                onGetDetailsByPlaceId: (value) {
                  setState(() {
                    address = value?.result.formattedAddress ?? "";
                  });
                },
                onChanged: (value) {},
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Done'.tr),
                onPressed: () {
                  location.text = searchLoc.text;
                  print("the location is ${location.text}");
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      // var kGoogleApiKey = Config.mapKey;

      // void onError(PlacesAutocompleteResponse response) {
      //   ScaffoldMessenger.of(context).showSnackBar(
      //     SnackBar(
      //       content: Text(response.errorMessage ?? 'Unknown error'),
      //     ),
      //   );
      // }

      // final Prediction? p = await PlacesAutocomplete.show(
      //   context: context,
      //   apiKey: kGoogleApiKey,
      //   onError: onError,
      //   mode: Mode.overlay, // or Mode.fullscreen
      //   language: 'fr',
      //   components: [const Component(Component.country, 'fr')],
      // );
      // log(p!.description.toString());
      // location.text = p.description.toString();
      // setState(() {});
      // LocationResult result =
      //     await Navigator.of(context).push(MaterialPageRoute(
      //         builder: (context) => PlacePicker(
      //               Config.mapKey,
      //               //  displayLocation: customLocation,
      //             )));
      //log(result.toString());
    } catch (e) {
      log(e.toString());
    }

    // Handle the result in your way
  }

  picVidVoi(icon) {
    return Container(
      decoration: BoxDecoration(
          color: buttonColor, borderRadius: BorderRadius.circular(10)),
      height: 60,
      width: 60,
      child: Center(
          child: Icon(
        icon,
        color: Colors.white,
      )),
    );
  }

  bool _yesSelected = false;
  bool _noSelected = false;
  radioButton() {
    return SizedBox(
      height: 50,
      width: width(context),
      child: Row(
        children: [
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: const Text('Yes'),
              value: true,
              groupValue: _yesSelected,
              onChanged: (value) {
                setState(() {
                  _yesSelected = value!;
                  _noSelected = !value; // Toggle the opposite option
                });
              },
            ),
          ),
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: const Text('No'),
              value: true,
              groupValue: _noSelected,
              onChanged: (value) {
                setState(() {
                  _noSelected = value!;
                  _yesSelected = !value; // Toggle the opposite option
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  bool _suspectNameChecked = false;

  bool _gangNameChecked = false;

  checkbox() {
    return Column(
      children: [
        CheckboxListTile(
          title: const Text('Suspect Name'),
          value: _suspectNameChecked,
          onChanged: (value) {
            setState(() {
              _suspectNameChecked = value!;
            });
          },
        ),
        CheckboxListTile(
          title: const Text('Gang Name'),
          value: _gangNameChecked,
          onChanged: (value) {
            setState(() {
              _gangNameChecked = value!;
            });
          },
        ),
      ],
    );
  }

  String? selectedReportid;

  dynamic countryIndex;
  dynamic stateIndex;
  dynamic cityIndex;

  final List<String> genderItems = [
    'Male',
    'Female',
  ];

  String? selectedValue;
  String? genderValue;
  String? countryid, cityid, stateid, departmentid;
  dropdownbutton(txt, List<String> items, type, {key}) {
    return DropdownButtonFormField2<String>(
      key: key,
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        // Add more decoration..
      ),
      hint: Text(
        type == null ? '$txt'.tr : '$type',
        style: const TextStyle(fontSize: 14),
      ),
      items: items
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                      fontSize: 14, overflow: TextOverflow.ellipsis),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select gender.';
        }
        return null;
      },
      onChanged: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        if (txt == "Country") {
          selectedCountry = selectedValue!;
          countryIndex = countriesList.indexOf(value.toString());

          log(countries[countryIndex].toString());
          countryid = countries[countryIndex]['id'].toString();
          getStates(countries[countryIndex]['id']);
        } else if (txt == "State") {
          selectedstate = selectedValue!;
          stateIndex = statesList.indexOf(value.toString());
          stateid = states[stateIndex]['id'].toString();
          log(statesList[stateIndex].toString());
          getCities(states[stateIndex]['id']);
        } else if (txt == "City") {
          selectedCity = selectedValue!;
          cityIndex = citiesList.indexOf(selectedCity.toString());
          cityid = citiesList[cityIndex];
        } else if (type == "departments") {
          selectedDepartment = value.toString();
          var id = items.indexOf(selectedDepartment);
          departmentid = departments['user_department_id'][id].toString();
        } else if (txt == "Scars") {
          scarsid = scarsList.indexOf(selectedValue!.toString()).toString();
          scarsid = scarsListids[int.parse(scarsid!)];
        } else if (txt == "Tattoos") {
          tattoosid = tattoosList.indexOf(selectedValue!.toString()).toString();
        } else if (txt == "Languages") {
          languageid =
              languageList.indexOf(selectedValue!.toString()).toString();
          languageid = languageListids[int.parse(languageid!)];
        } else if (txt == "Eye") {
          eyecolorid =
              eyecolorList.indexOf(selectedValue!.toString()).toString();
          eyecolorid = eyecolorListids[int.parse(eyecolorid!)];
        } else if (txt == "Hair") {
          haircolorid =
              haircolorList.indexOf(selectedValue!.toString()).toString();
          haircolorid = haircolorListids[int.parse(haircolorid!)];
        } else if (txt == "Weight") {
          weightid = weightList.indexOf(selectedValue!.toString()).toString();
          weightid = weightListids[int.parse(weightid!)];
        } else if (txt == "Height") {
          heightid = heightList.indexOf(selectedValue!.toString()).toString();
          heightid = heightListids[int.parse(heightid!)];
        } else if (txt == "Race") {
          raceid = raceList.indexOf(selectedValue!.toString()).toString();
          raceid = raceListids[int.parse(raceid!)];
        } else if (txt == "Age") {
          ageid = selectedValue!.toString();

          ///(ageList.indexOf(selectedValue!.toString()) + 1).toString();
          //ageid = [int.parse(ageid!)];
        } else if (txt == "Gender") {
          genderValue = selectedValue!.toString();
          if (genderValue == "Male") {
            genderid = "99";
          } else {
            genderid = "98";
          }
        } else if (txt == "Level") {
          offenderLevelId =
              (levelList.indexOf(selectedValue!.toString()) + 1).toString();
        }

        //Do something when selected item is changed.
      },
      onSaved: (value) {
        //   selectedValue = value.toString();
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
      dropdownSearchData: DropdownSearchData(
        searchController: textEditingController,
        searchInnerWidgetHeight: 50,
        searchInnerWidget: Container(
          height: 50,
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            expands: true,
            maxLines: null,
            controller: textEditingController,
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 8,
              ),
              hintText: 'Search'.tr,
              hintStyle: const TextStyle(fontSize: 12),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ),
        searchMatchFn: (item, searchValue) {
          return item.value
              .toString()
              .toLowerCase()
              .contains(searchValue.toLowerCase());
        },
      ),
      //This to clear the search value when you close the menu
      onMenuStateChange: (isOpen) {
        if (!isOpen) {
          textEditingController.clear();
        }
      },
    );
  }

  final TextEditingController textEditingController = TextEditingController();

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }
}
