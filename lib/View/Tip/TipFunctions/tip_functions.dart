// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'dart:typed_data';
import 'package:flutter_audio_trimmer/flutter_audio_trimmer.dart';
import 'package:path_provider/path_provider.dart';

import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class TipFunctions {
  String _base64Image = ''; // To store the base64 encoded image

  pickAndConvertImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      //log("them extensionIndex is ${pickedFile.path}");

      final extension =
          pickedFile.path.split('.').last; // Get the file extension
      //log("Image extension: $extension");

      //  final Uint8List imageBytes = await pickedFile.readAsBytes();
      var imageBytes = await File(pickedFile.path).readAsBytes();
      // log("them pickedFile is $imageBytes");

      // Convert bytes to base64
      String base64Image = base64Encode(imageBytes);

      // String base64Images = "data:image/png;base64," + base64Encode(imageBytes);

      _base64Image = base64Image;
      // log("them image is ${base64Image.toString()}");

      return {
        'name': pickedFile.name,
        'base64': "data:image/$extension;base64,$_base64Image"
      };
    }
  }

  pickAndConvertMedia() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickVideo(
        source: ImageSource.gallery); // Use pickVideo for videos

    if (pickedFile != null) {
      // Read the video as bytes
      List<int> videoBytes = await pickedFile.readAsBytes();
      final extension =
          pickedFile.path.split('.').last; // Get the file extension
      // log("Video extension: $extension");

      // Convert bytes to base64
      String base64Video = base64Encode(videoBytes);
      // log(base64Video.toString());

      return {
        'name': pickedFile.name,
        'base64': "data:video/$extension;base64,$base64Video",
        "asset": File(pickedFile.path)
      };
    }
  }

  recordAndConvertAudio() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.audio,
    );
    // FilePickerResult? result = await FilePicker.platform.pickFiles(
    //   type: FileType.custom,
    //   allowedExtensions: ['M4A', 'WAV', 'AAC', 'MP3'],
    // );

    if (result != null) {
      //   setState(() {
      var _pickedFilePath = result.files.single.path ?? '';
      // });
    }
    // if (result != null) {
    //   // Read the audio as bytes
    //   Uint8List fileBytes = result.files.first.bytes!;

    //   // Convert bytes to base64
    //   String base64Audio = base64Encode(fileBytes);
    //   log(base64Audio.toString());
    //   return {'name': result.files.first.name, 'base64': base64Audio};
    // }
  }

  String _formatFileSize(int bytes) {
    if (bytes < 1024) {
      return '$bytes B';
    } else if (bytes < 1024 * 1024) {
      double kbSize = bytes / 1024;
      return '${kbSize.toStringAsFixed(2)} KB';
    } else {
      double mbSize = bytes / (1024 * 1024);
      return '${mbSize.toStringAsFixed(2)} MB';
    }
  }

  nothingss() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowCompression: true,
      type: FileType.audio,
    );

    if (result != null) {
      var pickedFilePath = result.files.single.path ?? '';
      var fileSize = File(pickedFilePath).lengthSync();
      var size = _formatFileSize(fileSize);

      final extension =
          pickedFilePath.split('.').last; // Get the file extension
      // log("audio extension: $extension");
      // log("the size is ${size.split(".")[1].toString().split(" ")[1]}");
      if (int.parse(size.split(".")[0]) > 8 &&
          size.split(".")[1].toString().split(" ")[1] != "KB") {
        showToast("Audio file size should be less than 8 mb");
      } else if (extension != 'mp3' && extension != 'm4a') {
        showToast("Audio file should be in mp3 or m4a format");
      } else {
        try {
          Directory directory = await getApplicationDocumentsDirectory();

          File? trimmedAudioFile = await FlutterAudioTrimmer.trim(
            inputFile: File(pickedFilePath),
            outputDirectory: directory,
            fileName: DateTime.now().millisecondsSinceEpoch.toString(),
            fileType:
                Platform.isAndroid ? AudioFileType.mp3 : AudioFileType.m4a,
            time: AudioTrimTime(
              start: const Duration(seconds: 20),
              end: const Duration(seconds: 100),
            ),
          );

          List<int> audioBytes = await trimmedAudioFile!.readAsBytes();
          String base64String = base64Encode(audioBytes);
          List<int> decodedBytes = base64.decode(base64String);

          // Compress the data using zlib
          List<int> compressedData = zlib.encode(decodedBytes);

          // Convert compressed data to Base64
          String convertbase64String = base64Encode(compressedData);
          // print("Original Size: ${decodedBytes.length}");
          // print("Compressed Size: ${compressedData.length}");

          return {
            'name': pickedFilePath,
            'base64': "audio/$extension;base64,$convertbase64String",
            "asset": trimmedAudioFile.path
          };
        } catch (e) {
          List<int> audioBytes = await File(pickedFilePath).readAsBytes();
          String base64String = base64Encode(audioBytes);
          List<int> decodedBytes = base64.decode(base64String);

          // Compress the data using zlib
          List<int> compressedData = zlib.encode(decodedBytes);

          // Convert compressed data to Base64
          String convertbase64String = base64Encode(compressedData);
          // print("Original Size: ${decodedBytes.length}");
          // print("Compressed Size: ${compressedData.length}");

          return {
            'name': pickedFilePath,
            'base64': "audio/$extension;base64,$convertbase64String",
            "asset": pickedFilePath
          };
        }
      }
    }
  }

  nothings() async {
    
   await [
   Permission.accessMediaLocation,
   Permission.audio,
Permission.mediaLibrary,
Permission.microphone,
  Permission.storage
].request();
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      //allowCompression: true,
      type: FileType.custom,
      allowedExtensions: ['mp3','ogg','aac'],
    );

    if (result != null) {
      var pickedFilePath = result.files.single.path ?? '';
      //var fileSize = File(pickedFilePath).lengthSync();
      // var size = _formatFileSize(fileSize);

      final extension =
          pickedFilePath.split('.').last; // Get the file extension
      // log("audio extension: $extension");
      // log("the size is ${size.split(".")[1].toString().split(" ")[1]}");
      // if (int.parse(size.split(".")[0]) > 8 &&
      //     size.split(".")[1].toString().split(" ")[1] != "KB") {
      //   showToast("Audio file size should be less than 8 mb");
      // } else if (extension != 'mp3' && extension != 'm4a') {
      //   showToast("Audio file should be in mp3 or m4a format");
      // }

      // else {
      try {
        List<int> audioBytes = await File(pickedFilePath).readAsBytes();
        String base64String = base64Encode(audioBytes);
        // List<int> decodedBytes = base64.decode(base64String);

        // // Compress the data using zlib
        // List<int> compressedData = zlib.encode(decodedBytes);

        // // Convert compressed data to Base64
        // String convertbase64String = base64Encode(compressedData);
        // print("Original Size: ${decodedBytes.length}");
        // print("Compressed Size: ${compressedData.length}");

        return {
          'name': pickedFilePath,
          'base64': "audio/$extension;base64,$base64String",
          "asset": pickedFilePath
        };
      } catch (e) {
        print(e);
      }
      //}
    }
  }

  Future<void> pickAudio(context) async {
  try {
    List<AssetEntity>? result = await AssetPicker.pickAssets(
      context,pickerConfig:const AssetPickerConfig(requestType: RequestType.audio,)
    );

    if (result != null && result.isNotEmpty) {
      // Handle the selected audio asset(s)
      AssetEntity asset = result.first;

      print('Asset path: ${asset.file}');
      print('Asset type: ${asset.type}');
    } else {
      // User canceled the asset picking
    }
  } catch (e) {
    // Handle any errors that might occur during asset picking
    print('Error picking audio asset: $e');
  }
}

// nothings() async {
//   FilePickerResult? result = await FilePicker.platform.pickFiles(
//     allowCompression: true,
//     type: FileType.audio,
//   );

//   if (result != null) {
//     var pickedFilePath = result.files.single.path ?? '';
//     var fileSize = File(pickedFilePath).lengthSync();
//     var size = _formatFileSize(fileSize);
//      final extension =
//           pickedFilePath.split('.').last;   log("audio extension: $extension");
//     log("the size is ${int.parse(size.split(".")[0])}");
//     if (int.parse(size.split(".")[0]) > 8) {
//       showToast("Audio file size should be less than 8 mb");
//     } else {
//      // Get the file extension

//       try {
//         Directory directory = await getApplicationDocumentsDirectory();

//         File? trimmedAudioFile = await FlutterAudioTrimmer.trim(
//           inputFile: File(pickedFilePath),
//           outputDirectory: directory,
//           fileName: DateTime.now().millisecondsSinceEpoch.toString(),
//           fileType:
//               Platform.isAndroid ? AudioFileType.mp3 : AudioFileType.m4a,
//           time: AudioTrimTime(
//             start: const Duration(seconds: 20),
//             end: const Duration(seconds: 100),
//           ),
//         );

//         List<int> audioBytes = await trimmedAudioFile!.readAsBytes();
//         String base64String = base64Encode(audioBytes);
//         List<int> decodedBytes = base64.decode(base64String);

//         // Compress the data using zlib
//         List<int> compressedData = zlib.encode(decodedBytes);

//         // Convert compressed data to Base64
//         String convertbase64String = base64Encode(compressedData);
//         print("Original Size: ${decodedBytes.length}");
//         print("Compressed Size: ${compressedData.length}");

//         return {
//           'name': pickedFilePath,
//           'base64': "audio/$extension;base64,$convertbase64String",
//           "asset": trimmedAudioFile.path
//         };
//       } catch (e) {
//         List<int> audioBytes = await File(pickedFilePath).readAsBytes();
//         String base64String = base64Encode(audioBytes);
//         List<int> decodedBytes = base64.decode(base64String);

//         // Compress the data using zlib
//         List<int> compressedData = zlib.encode(decodedBytes);

//         // Convert compressed data to Base64
//         String convertbase64String = base64Encode(compressedData);
//         print("Original Size: ${decodedBytes.length}");
//         print("Compressed Size: ${compressedData.length}");

//         return {
//           'name': pickedFilePath,
//           'base64': "audio/$extension;base64,$convertbase64String",
//           "asset": pickedFilePath
//         };
//       }
//     }
//   }
// }

  getCustomerReport(userid, context) async {
    var data = {"user_id": userid, "isPast": true};

    var response = await Controller().getCustomerReport(data);
    // log("the previous reports are ${response.toString()}");

    if (response['succ'] == false) {
      showToast(response['errors']);

      return null;
    } else {
      return response['data']['customer_report'];
    }
  }

  List<String> divideBase64IntoChunks(String base64String, int chunkSize) {
    List<int> decodedBytes = base64.decode(base64String);
    List<String> chunks = [];

    for (int i = 0; i < decodedBytes.length; i += chunkSize) {
      int end = i + chunkSize;
      end = end > decodedBytes.length ? decodedBytes.length : end;
      List<int> chunkBytes = decodedBytes.sublist(i, end);
      String chunkBase64 = base64.encode(chunkBytes);
      chunks.add(chunkBase64);
    }

    return chunks;
  }
}
