import 'dart:convert';
import 'dart:developer';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:map_location_picker/map_location_picker.dart';

import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Profile/ui/set_up.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/search_tip.dart';
import 'package:snap_send_app/View/Settings/utils/dialogs.dart';
import 'package:snap_send_app/View/Tip/TipFunctions/tip_functions.dart';
import 'package:snap_send_app/View/Tip/TipFunctions/audio_player.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';
import 'package:video_player/video_player.dart';

// ignore: must_be_immutable
class TipScreen extends StatefulWidget {
  dynamic from;
  TipScreen({super.key, this.from});
  @override
  State<TipScreen> createState() => _TipScreenState();
}

class _TipScreenState extends State<TipScreen> {
  dynamic tiptypes;
  dynamic carmake;
  dynamic carmodel;
  dynamic yearofcar;
  dynamic carcolor;

  TextEditingController dateController = TextEditingController();
  TextEditingController dateController2 =
      TextEditingController(text: DateTime.now().toString().split(" ")[0]);
  TextEditingController timeController = TextEditingController();
  TextEditingController timeController2 =
      TextEditingController(text: "12:00 AM");
  bool _yesSelected = true;
  bool _noSelected = false;
  bool isnewtip = true;
  bool ispasttip = false;
  String? departmentid;
  String? tiptypeid;
  String? carmakeid;
  String? carmodelid;
  String? yearofcarid;
  String? carcolorid;
  List<String> tipTypeList = [];
  List<String> carmakeList = [];
  List<String> carmodelList = [];
  List<String> yearofcarList = [];
  List<String> carcolorList = [];
  List<int> carmakeIdList = [];
  List<int> carmodelIdList = [];
  List<int> yearofcarIdList = [];
  List<int> carcolorIdList = [];

  String selectedTipType = "Select";
  String selectedcarmake = "Select";
  String selectedcarmodel = "Select";
  String selectedyearofcar = "Select";
  String selectedcarcolor = "Select";
  bool isloading = false;

  dynamic departments;
  List<String> departmentList = [];
  List<String> departmentids = [];
  String selectedDepartment = "Select";

  getDepartments(context) async {
    isloading = true;
    setState(() {});
    var data = {"department_type": 1};

    var response = await Controller().getDepartment(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      departments = response['data'];
      for (int i = 0; i < departments['user_department_in_array'].length; i++) {
        departmentList.add(departments['user_department_in_array'][i]);
        departmentids.add(departments['user_department_id'][i].toString());
      }
    }
    var data2 = {"department_type": 2};
    var response2 = await Controller().getDepartment(data2);

    if (response2['succ'] == false) {
      //  showToast(response2['errors']);
    } else {
      departments['travel_departments'] = response2['data'];
      for (int i = 0;
          i <
              departments['travel_departments']['user_department_in_array']
                  .length;
          i++) {
        departmentList.add(
            departments['travel_departments']['user_department_in_array'][i]);
        departmentids.add(departments['travel_departments']
                ['user_department_id'][i]
            .toString());
      }
    }
    if (departmentList.isEmpty) {
      showToast("Please set department first");
      nav(context, SetUpScreen(isfrom: 'TIP',));
    }
    isloading = false;
    setState(() {});
  }

  getEntities(context) async {
    selectedTipType = "Select";
    selectedcarmake = "Select";
    selectedcarmodel = "Select";
    selectedyearofcar = "Select";
    selectedcarcolor = "Select";
    selectedDepartment = "Select";
    tipTypeList = [];
    carmakeList = [];
    carmodelList = [];
    yearofcarList = [];
    carcolorList = [];
    isloading = true;
    setState(() {});

    tiptypes = await Controller().webEntity({"type": "report-type"});
    var carmake = await Controller().webEntity({"type": "car-make"});
    var carmodel = await Controller().webEntity({"type": "car-model"});
    var yearofcar = await Controller().webEntity({"type": "car-year"});
    var carcolor = await Controller().webEntity({"type": "color"});
    await getDepartments(context);
    tiptypes = tiptypes['data']['app_entity'];
    for (int i = 0; i < tiptypes.length; i++) {
      tipTypeList.add(tiptypes[i]['title']);
    }
    // selectedTipType = tipTypeList[0];

    carmake = carmake['data']['app_entity'];
    for (int i = 0; i < carmake.length; i++) {
      carmakeList.add(carmake[i]['title']);
      carmakeIdList.add(carmake[i]['id']);
    }
    carmodel = carmodel['data']['app_entity'];
    for (int i = 0; i < carmodel.length; i++) {
      carmodelList.add(carmodel[i]['title']);
      carmodelIdList.add(carmodel[i]['id']);
    }
    yearofcar = yearofcar['data']['app_entity'];
    for (int i = 0; i < yearofcar.length; i++) {
      yearofcarList.add(yearofcar[i]['title']);
      yearofcarIdList.add(yearofcar[i]['id']);
    }
    carcolor = carcolor['data']['app_entity'];
    for (int i = 0; i < carcolor.length; i++) {
      carcolorList.add(carcolor[i]['title']);
      carcolorIdList.add(carcolor[i]['id']);
    }

    isloading = false;
    setState(() {});
  }

  // ignore: prefer_typing_uninitialized_variables
  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    log(user.toString());
  }

  submitTip(context) async {
    isloading = true;
    setState(() {});
    var data = {
      "filePics": ["${selectedImage == null ? "" : selectedImage['base64']}"],
      "fileVideo": ["${selectedVideo == null ? "" : selectedVideo['base64']}"],
      "fileVideoTrack":selectedAudio == null ? "" : "${selectedAudio['base64']}",
      "new_report": 1,
      "report_type": tiptypeid,
      "department_id": departmentid,
      "user_id": user['id'],
      "description": tipdetails.text.trim(),
      "location": location.text.trim(),
      "file_reported_date":dateController2.text.trim(),
      "file_reported_time": timeController.text.trim(),
      "no_of_people_inv": numberofpeople.text.trim(),
      "suspect_name": suspectname.text.trim(),
      "gang_name": gangname.text.trim(),
      "license_plate_number": license.text.trim(),
      "car_model": carmodelid,
      "make_model_of_car": yearofcarid,
      "color_of_car": carcolorid,
      "car_make": carmakeid,
      "car_year": yearofcarid,
      "add_contact": _yesSelected ? 1 : 0,
    };

    var response = await Controller().createfileReport(data);
    log(response.toString());
    isloading = false;
    setState(() {});

    if (response['succ'] == 0) {
      showToast(response['errors'].toString());
    } else {
      if (widget.from == "home" || widget.from == "bottom") {
        return Get.dialog(TipDialog(response['msg'].toString()));
      }
      clear();
    }
  }

  clear() {
    selectedImage = null;
    selectedVideo = null;
    selectedAudio = null;
    tiptypeid = null;
    departmentid = null;
    tipdetails.text = "";
    location.text = "";
    dateController2.text = "";
    timeController.text = "";
    numberofpeople.text = "";
    suspectname.text = "";
    gangname.text = "";
    license.text = "";
    carmodelid = null;
    carmakeid = null;
    carcolorid = null;
    carmakeid = null;
    _yesSelected = false;
    selectedTipType = "Select";
    selectedcarmake = "Select";
    selectedcarmodel = "Select";
    selectedyearofcar = "Select";
    selectedcarcolor = "Select";
  }

  // ignore: prefer_typing_uninitialized_variables
  var getPreviousReports;
  List<String> customerReportList = [];
  String selectedReport = "Select";
  getCustomerReport() async {
    getPreviousReports =
        await TipFunctions().getCustomerReport(user['id'], context);

    for (int i = 0; i < getPreviousReports.length; i++) {
      customerReportList.add(
          "${getPreviousReports[i]['id']} ${getPreviousReports[i]['report_title']} ${getPreviousReports[i]['name']}");
    }
  }

  @override
  void initState() {
    super.initState();
    // submitTip(context);
    getEntities(context);
    getuserDetails();
  }

  final TextEditingController textEditingController = TextEditingController();
  getCall() async {
    var istip = await Storage.get('istip');
    if (istip == 'true') {
      clear();
      // ignore: use_build_context_synchronously
      getEntities(context);
    }

    await Storage.set('istip', 'false');
  }

  @override
  Widget build(BuildContext context) {
    getCall();
    Set<String> uniqueSet = Set<String>.from(departmentList);
    Set<String> uniqueSet2 = Set<String>.from(departmentids);
    departmentList = List<String>.from(uniqueSet);
    departmentids = List<String>.from(uniqueSet2);
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: buttonColor,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                if (widget.from == "home") {
                  pop(context);
                }
              },
              icon: Icon(
                Icons.arrow_back_ios,
                color: widget.from == "home" ? Colors.white : buttonColor,
              )),
          actions: [
            IconButton(
                onPressed: () {
                  navPushReplacement(context, MyBottomNavigationBar());
                },
                icon: const Icon(CupertinoIcons.home))
          ],
          title: Text('File A Tip'.tr)),
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        progressIndicator: spinkit,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: SizedBox(
            height: height(context),
            width: width(context),
            child: ListView(
              children: [
                childWidget(context),
              ],
            ),
          ),
        ),
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.only(bottom: 28.0),
          child: Center(
              child: longbuttons('Submit', () {
            submitTip(context);
          }, buttonColor, width: 190)),
        ),
      ],
    );
  }

  final _formKey = GlobalKey<FormState>();

  TextEditingController tipdetails = TextEditingController();
  TextEditingController location = TextEditingController();

  TextEditingController license = TextEditingController();

  TextEditingController numberofpeople = TextEditingController();

  dynamic selectedImage;
  dynamic selectedVideo;
  dynamic selectedAudio;
  late VideoPlayerController _controller;

  initializeVideo() async {
    _controller = VideoPlayerController.file(selectedVideo['asset'])
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    _controller.play();
  }

 

  @override
  void dispose() {
    try {
      _controller.dispose();
      textEditingController.dispose();
    } catch (e) {}

    super.dispose();
  }

  Widget childWidget(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      elevation: 3.0,
      child: Form(
        key: _formKey,
        child: ListView(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 100,
                width: 100,
                child: Image.asset(
                  'assets/snapsend_logo.png',
                  // fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Tip information'.tr,
                      style:
                          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      'follow the steps below to submit a tip'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const SizedBox(height: 14),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
                    child: Text(
                      'add my contact information (if this is an anonymous tip select NO):'
                          .tr,
                      style:
                          const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  radioButton(),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'New Tip'.tr,
                      style:
                          const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  isNewradioButton(),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Destination Department'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(
                      selectedDepartment, departmentList, 'departments'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Type of Tip'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedTipType, tipTypeList, 'tiptype'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Tip Details'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield2(
                        controller: tipdetails,
                        hinttext: 'Enter details here...'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Location'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: location,
                        icon: CupertinoIcons.map_pin_ellipse,
                        hinttext: 'Address or Closest street',
                        isreadonly: true,
                        onTap: () {
                          showPlacePicker();
                        }),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Date'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: dateController,
                        icon: CupertinoIcons.calendar,
                        hinttext: 'Select Date',
                        isreadonly: true,
                        onTap: () {
                          _pickDate();
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Time'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: timeController,
                        icon: CupertinoIcons.time,
                        hinttext: 'Select Time',
                        isreadonly: true,
                        onTap: () {
                          _pickTime();
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Car Make'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedcarmake, carmakeList, 'carmake'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Car Model'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedcarmodel, carmodelList, 'carmodel'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Year of Car'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedyearofcar, yearofcarList, 'yearofcar'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Car Color'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedcarcolor, carcolorList, 'carcolor'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'License Plate Number'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: license,
                        icon: CupertinoIcons.creditcard_fill,
                        hinttext: 'License Plate Number'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Number of people involved'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: numberofpeople,
                        icon: CupertinoIcons.person,
                        hinttext: '0 Person'),
                  ),
                  checkbox(),

                  Padding(
                    padding: const EdgeInsets.only(top: 12.0, bottom: 8.0),
                    child: Text(
                      'Upload Files'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      picVidVoi(CupertinoIcons.camera, () async {
                        selectedImage =
                            await TipFunctions().pickAndConvertImage();
                        setState(() {});
                        if (selectedImage != null) {
                          showToast("Image has been Selected".tr);
                        }
                      }),
                      picVidVoi(CupertinoIcons.video_camera, () async {
                        selectedVideo =
                            await TipFunctions().pickAndConvertMedia();
                        if (selectedImage != null) {
                          showToast("Video has been Selected".tr);
                          await initializeVideo();
                          setState(() {});
                        }
                      }),
                      picVidVoi(CupertinoIcons.mic, () async {
                        selectedAudio = null;
                        setState(() {});
                        //  TipFunctions().pickAudio(context);
                        selectedAudio = await TipFunctions().nothings();
                        setState(() {});
                        if (selectedAudio != null) {
                          showToast("Audio has been Selected".tr);
                        }
                        //  await TipFunctions().recordAndConvertAudio();
                        // showToast("Audio has been Selected");
                      })
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      selectedImage != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.memory(
                                base64Decode(selectedImage['base64']
                                        .toString()
                                        .split(",")[
                                    1]), // Decoding and displaying the image
                                width: 100,
                                height: 100,
                                fit: BoxFit.fill,
                              ),
                            )
                          : Container(),
                      selectedVideo != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: SizedBox(
                                width: 100,
                                height: 100,
                                child: AspectRatio(
                                  aspectRatio: _controller.value.aspectRatio,
                                  child: VideoPlayer(_controller),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                  const SizedBox(height: 20),
                  selectedAudio != null
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: SizedBox(
                              width: 250,
                              height: 80,
                              child: AudioPlay(asset: selectedAudio['asset'])),
                        )
                      : const SizedBox(),
          
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _pickDate() async {
    DateTime? selectedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );
    if (selectedDate != null) {
      setState(() {
        dateController2.text = selectedDate.toString().split(" ")[0];
        dateController.text =
            "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}";
      });
    }
  }

  Future<void> _pickTime() async {
    TimeOfDay? selectedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (selectedTime != null) {
      setState(() {
        timeController.text = selectedTime.format(context);
      });
    }
  }

  String address = "null";
  String autocompletePlace = "null";
  Prediction? initialValue;
  void showPlacePicker() async {
    TextEditingController searchLoc = TextEditingController();
    try {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Location'.tr),
            content: SizedBox(
              height: 300,
              width: 300,
              child: PlacesAutocomplete(
                controller: searchLoc,
                searchController: searchLoc,
                apiKey: Config.mapKey,
                searchHintText: "Search for a place".tr,
                mounted: mounted,
                hideBackButton: true,
                initialValue: initialValue,
                onSuggestionSelected: (value) {
                  setState(() {
                    autocompletePlace =
                        value.structuredFormatting?.mainText ?? "";
                    initialValue = value;
                  });
                },
                onGetDetailsByPlaceId: (value) {
                  setState(() {
                    address = value?.result.formattedAddress ?? "";
                  });
                },
                onChanged: (value) {},
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Done'.tr),
                onPressed: () {
                  location.text = searchLoc.text;
                  print("the location is ${location.text}");
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      log(e.toString());
    }

    // Handle the result in your way
  }

  picVidVoi(icon, void Function()? onPressed) {
    return Container(
      decoration: BoxDecoration(
          color: buttonColor, borderRadius: BorderRadius.circular(10)),
      height: 60,
      width: 60,
      child: Center(
          child: IconButton(
              onPressed: onPressed,
              icon: Icon(
                icon,
                color: Colors.white,
              ))),
    );
  }



  isNewradioButton() {
    return SizedBox(
      height: 50,
      width: width(context),
      child: Row(
        children: [
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: Text(
                'Yes'.tr,
              ),
              value: true,
              groupValue: isnewtip,
              onChanged: (value) {
                setState(() {
                  isnewtip = value!;
                  ispasttip = !value; // Toggle the opposite option
                });
              },
            ),
          ),
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: Text(
                'No'.tr,
              ),
              value: true,
              groupValue: ispasttip,
              onChanged: (value) {
                nav(context, SearchTip());
                setState(() {
                  ispasttip = value!;
                  isnewtip = !value; // Toggle the opposite option
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  radioButton() {
    return SizedBox(
      height: 50,
      width: width(context),
      child: Row(
        children: [
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: Text(
                'Yes'.tr,
              ),
              value: true,
              groupValue: _yesSelected,
              onChanged: (value) {
                setState(() {
                  _yesSelected = value!;
                  _noSelected = !value; // Toggle the opposite option
                });
              },
            ),
          ),
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: Text(
                'No'.tr,
              ),
              value: true,
              groupValue: _noSelected,
              onChanged: (value) {
                setState(() {
                  _noSelected = value!;
                  _yesSelected = !value; // Toggle the opposite option
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  bool _suspectNameChecked = false;

  bool _gangNameChecked = false;

  TextEditingController suspectname = TextEditingController();
  TextEditingController gangname = TextEditingController();
  checkbox() {
    return Column(
      children: [
        CheckboxListTile(
          title: Text('Suspect Name'.tr),
          value: _suspectNameChecked,
          onChanged: (value) {
            setState(() {
              _suspectNameChecked = value!;
            });
            //   FocusScope.of(context).requestFocus(_focusNode);
          },
        ),
        _suspectNameChecked
            ? Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: textformfield1(
                    controller: suspectname,
                    icon: CupertinoIcons.person,
                    hinttext: 'Suspect Name'),
              )
            : const SizedBox(),
        CheckboxListTile(
          title: Text('Gang Name'.tr),
          value: _gangNameChecked,
          onChanged: (value) {
            setState(() {
              _gangNameChecked = value!;
            });
          },
        ),

        _gangNameChecked
            ? Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: textformfield1(
                    // focusNode: _focusNode,
                    controller: gangname,
                    icon: CupertinoIcons.person,
                    hinttext: 'Gang Name'),
              )
            : const SizedBox(),
      ],
    );
  }

  String? selectedReportid;
  dropdownbutton(txt, List<String> items, type) {
    return DropdownButtonFormField2<String>(
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        // Add more decoration..
      ),
      hint: Text(
        '$txt'.tr,
        style: const TextStyle(fontSize: 14),
      ),
      items: items
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item.tr,
                  style: const TextStyle(
                      fontSize: 14, overflow: TextOverflow.ellipsis),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select gender.';
        }
        return null;
      },
      onChanged: (value) {
        if (type == "departments") {
          selectedDepartment = value.toString();
          var id = items.indexOf(selectedDepartment);
          departmentid = departmentids[id].toString();
          log("departmentid is $departmentid");
        } else if (type == "tiptype") {
          selectedTipType = value.toString();
          tiptypeid = items.indexOf(value.toString()).toString();
          log("tiptypeid is $tiptypeid and type is ${tiptypes[int.parse(tiptypeid!)]['id']}");
          tiptypeid = "${tiptypes[int.parse(tiptypeid!)]['id']}";
        } else if (type == "carmake") {
          selectedcarmake = value.toString();
          carmakeid = items.indexOf(value.toString()).toString();
          carmakeid = carmakeIdList[int.parse(carmakeid!)].toString();
        } else if (type == "carmodel") {
          selectedcarmodel = value.toString();
          carmodelid = items.indexOf(value.toString()).toString();
          carmodelid = carmodelIdList[int.parse(carmodelid!)].toString();
        } else if (type == "yearofcar") {
          selectedyearofcar = value.toString();
          yearofcarid = items.indexOf(value.toString()).toString();
          yearofcarid = yearofcarIdList[int.parse(yearofcarid!)].toString();
        } else if (type == "carcolor") {
          selectedcarcolor = value.toString();
          carcolorid = items.indexOf(value.toString()).toString();
          carcolorid = carcolorIdList[int.parse(carcolorid!)].toString();
        } else if (type == "Choose past tip") {
          selectedReport = value.toString();
          var id = items.indexOf(value.toString()).toString();
          log("${getPreviousReports[int.parse(id)]}");

          selectedReportid = getPreviousReports[int.parse(id)]['id'].toString();
          selectedDepartment =
              getPreviousReports[int.parse(id)]['name'].toString();
          departmentid = getPreviousReports[int.parse(id)]
                  ['file_tip_department']
              .toString();
          tiptypeid =
              getPreviousReports[int.parse(id)]['report_type'].toString();

          selectedTipType =
              getPreviousReports[int.parse(id)]['report_title'].toString();

          log("selectedReportid is $selectedReportid selectedDepartment is $selectedDepartment departmentid is $departmentid tiptypeid is $tiptypeid selectedTipType is $selectedTipType");
          setState(() {});
        }
        //Do something when selected item is changed.
      },
      onSaved: (value) {
        //   selectedValue = value.toString();
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),

      dropdownSearchData: DropdownSearchData(
        searchController: textEditingController,
        searchInnerWidgetHeight: 50,
        searchInnerWidget: Container(
          height: 50,
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            expands: true,
            maxLines: null,
            controller: textEditingController,
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 8,
              ),
              hintText: 'Search...'.tr,
              hintStyle: const TextStyle(fontSize: 12),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ),
        searchMatchFn: (item, searchValue) {
          return item.value
              .toString()
              .toLowerCase()
              .contains(searchValue.toLowerCase());
        },
      ),
      //This to clear the search value when you close the menu
      onMenuStateChange: (isOpen) {
        if (!isOpen) {
          textEditingController.clear();
        }
      },
    );
  }
}
