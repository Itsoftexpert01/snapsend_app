import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:pinput/pinput.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Auth/login_screen.dart';
import 'package:snap_send_app/View/Notification/utils/color.dart';
import 'package:snap_send_app/View/Notification/utils/widgets.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

class OTPScreen extends StatefulWidget {
  final String phone, ext;
  const OTPScreen(this.ext, this.phone);
  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  var otp;

  final defaultPinTheme = PinTheme(
    width: 56,
    height: 56,
    textStyle: const TextStyle(
        fontSize: 20,
        color: Color.fromRGBO(30, 60, 87, 1),
        fontWeight: FontWeight.w600),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.black),
      borderRadius: BorderRadius.circular(20),
    ),
  );
  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 60,
      height: 60,
      textStyle: TextStyle(
          fontSize: 20, color: buttonColor, fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: buttonColor),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
        border: Border.all(color: buttonColor));

    final submittedPinTheme = defaultPinTheme.copyWith(
        decoration: defaultPinTheme.decoration!
            .copyWith(color: ColorSelect().greylight));

    return Scaffold(
      key: _scaffoldkey,
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        progressIndicator: spinkit,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 42, left: 30, right: 30),
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Image.asset(
                      'assets/snapsend_logo.png',
                      height: 150,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Center(child: text2('SnapSend Report', 22, Colors.black)),
                  const SizedBox(
                    height: 50,
                  ),
                  Center(child: text2('OTP VERIFICATION', 22, Colors.black)),
                  const SizedBox(
                    height: 30,
                  ),
                  Center(
                      child: text1(
                          'Enter The 4 digit code that has \n            sent to your phone',
                          18,
                          ColorSelect().black54)),
                  const SizedBox(
                    height: 21,
                  ),
                  Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Pinput(
                        defaultPinTheme: defaultPinTheme,
                        focusedPinTheme: focusedPinTheme,
                        submittedPinTheme: submittedPinTheme,
                        validator: (s) {
                          return null;
                        },
                        pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                        showCursor: true,
                        onCompleted: (pin) => {otp = pin, print(otp)},
                      )),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        text1("Don't receive code? ", 15, ColorSelect().black),
                        GestureDetector(
                            onTap: () {
                              resendOTP(otpToken, context);
                            },
                            child: text2("Resend OTP Code?", 15, buttonColor)),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  longbuttons('VERIFY', () {
                    verifyOTP(otpToken, otp, context);
                  }, buttonColor, width: 200),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String otpToken = "";
  reqOTP(context) async {
    isloading = true;
    setState(() {});
    var data = {
      "mobile": widget.phone,
      "country_ext": widget.ext,
      "ver_method": "sms",
      "checkRegUser": true
    };
    log(data.toString());
    var response = await Controller().reqOTP(data);
    log(response.toString());

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      otpToken = response['data']['otp_token'];
      showToast(response['msg']);
    }

    isloading = false;
    setState(() {});
  }

  resendOTP(token, context) async {
    isloading = true;
    setState(() {});
    var data = {"otp_token": "$token"};

    var response = await Controller().resendOTP(data);
    log(response.toString());

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      showToast(response['msg']);
    }

    isloading = false;
    setState(() {});
  }

//
  verifyOTP(token, otp, context) async {
    isloading = true;
    setState(() {});
    var data = {
      "otp": "$otp",
      "otp_token": "$token",
      "get_user_details": true,
      "auto_login": true,
      "upsert": true
    };
    log(data.toString());
    if (otp == null) {
    } else {
      var response = await Controller().verifyOTP(data);

      if (response['succ'] == false) {
        showToast(response['errors']);
      } else {
        showToast(response['msg']);
        nav(context, LoginScreen());
      }
    }

    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    reqOTP(context);
  }
}
