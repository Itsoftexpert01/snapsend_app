import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Auth/OTP/otp_screen.dart';
import 'package:snap_send_app/View/Auth/forgot_password.dart';
import 'package:snap_send_app/View/Auth/register_screen.dart';
import 'package:snap_send_app/View/Settings/utils/language_dialog.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController? email = TextEditingController();
  TextEditingController? password = TextEditingController();
  bool isloading = false;
  getcredentials() async {
    var credentials = await Storage.get('credentials');
    if (credentials != false) {
      credentials = json.decode(await Storage.get('credentials'));
      email?.text = credentials['username'];
      password?.text = credentials['password'];
      ischecked = true;
      setState(() {});
    }
  }

  login(data, ischecked, context) async {
    isloading = true;
    setState(() {});
    var resp = await Controller().login(data);
    if (resp['succ'] == 0) {
      showToast(resp['errors']);
    } else {
      var userdet = resp['data']['user_details'];
      if (userdet['mobile_verified_at'] == null ||
          userdet['mobile_verified_at'] == 'null') {
        nav(
            context,
            OTPScreen(userdet['country_ext'].toString(),
                userdet['mobile'].toString()));
      } else {
        if (ischecked) {
          await Storage.set('isremember', 'true');
          await Storage.set(
              'credentials',
              json.encode({
                'username': data['username'],
                'password': data['password']
              }));
        } else {
          await Storage.set('isremember', 'false');
          await Storage.remove('credentials');
        }

        await Storage.setLogin(resp['data']['user_details']);
        await Storage.setToken(resp['data']['token']['access_token']);
        nav(context, MyBottomNavigationBar());
      }
    }
    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getcredentials();
  }

  bool obsecureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ModalProgressHUD(
      inAsyncCall: isloading,
      progressIndicator: spinkit,
      child: ListView(
        children: [
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 40.0),
                  child: Text(
                    'Login Account'.tr,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: SizedBox(
                    height: 150,
                    width: 150,
                    child: Image.asset(
                      'assets/snapsend_logo.png',
                      // fit: BoxFit.fill,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text(
                    'SnapSend Report'.tr,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0),
                  child: Text(
                    'For Safer Communities'.tr,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: shortButton('Login as User', () {}, buttonColor,
                      fontSize: 14),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
                  child: textformfield1(
                      controller: email,
                      icon: Icons.email,
                      hinttext: 'Username or Email address'),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
                  child: textformfield1(
                      obscureText: obsecureText,
                      onPressed: () {
                        obsecureText = !obsecureText;
                        setState(() {});
                      },
                      controller: password,
                      icon: !obsecureText
                          ? Icons.visibility_off
                          : Icons.visibility,
                      hinttext: 'Password'),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 60, right: 60.0, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () {
                          ischecked = !ischecked;
                          setState(() {});
                        },
                        child: Icon(
                          ischecked
                              ? Icons.check_box
                              : Icons.check_box_outline_blank,
                          size: 20,
                          color: Colors.grey,
                        ),
                      ),
                      const SizedBox(
                        width: 0,
                      ),
                      text3("Remember Me", 12.5, buttonColor),
                      const SizedBox(
                        width: 50,
                      ),
                      InkWell(
                          onTap: () async {
                            nav(context, ForgotScreen());
                          },
                          child: text3("Forgot Password?", 12, buttonColor)),
                    ],
                  ),
                ),
                Padding(
                    padding:
                        const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
                    child: longbuttons('Login To Snapsend', () {
                      var data = {
                        "login_method": "user_pass",
                        "username": email?.text.trim(),
                        "password": password?.text.trim(),
                        "upsert": true,
                        "get_user_details": true,
                      };
                      if (email!.text.isNotEmpty || password!.text.isNotEmpty) {
                        login(data, ischecked, context);
                      } else {
                        showToast('Please fill all the fields first');
                      }
                    }, buttonColor, width: 300)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 18.0),
                      child: text3("No Account? ", 12.5, Colors.grey),
                    ),
                    GestureDetector(
                      onTap: () {
                        nav(context, RegisterScreen());
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: text3("Create new account", 12.5, buttonColor),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    openDialog();
                  },
                  child: Chip(
                    backgroundColor: buttonColor,
                    label: Text(
                      "Choose Language".tr,
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }

  openDialog() async {
    // var islanguageSelected = await Storage.get('language');
    // print("islanguageSelected is $islanguageSelected");
    // if (islanguageSelected.runtimeType == bool && !islanguageSelected) {
    return Get.dialog(LanguageDialog());
    // }
  }

  bool ischecked = false;
}
