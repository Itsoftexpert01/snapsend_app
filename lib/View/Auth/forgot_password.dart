import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class ForgotScreen extends StatefulWidget {
  ForgotScreen({Key? key}) : super(key: key);

  @override
  State<ForgotScreen> createState() => _ForgotScreenState();
}

class _ForgotScreenState extends State<ForgotScreen> {
  TextEditingController? email = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            'Reset Password'.tr,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              pops(context);
            },
          ),
        ),
        // backgroundColor: primaryColor,
        body: ModalProgressHUD(
          inAsyncCall: isloading,
          progressIndicator: spinkit,
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // const Padding(
                  //   padding: EdgeInsets.only(top: 40.0),
                  //   child: Text(
                  //     'Login Account',
                  //     textAlign: TextAlign.center,
                  //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: SizedBox(
                      height: 200,
                      width: 200,
                      child: Image.asset(
                        'assets/snapsend_logo.png',
                        // fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Reset Password'.tr,
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Text(
                      'Enter your email to get instructions'.tr,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),

                  Padding(
                    padding:
                        const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
                    child: textformfield1(
                        controller: email,
                        icon: Icons.email,
                        hinttext: 'Enter email address'),
                  ),

                  Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 60, right: 60.0),
                      child: longbuttons('Send instructions', () async {
                        if (email!.text.isEmpty) {
                          showToast("Please write your email first");
                        } else {
                          var data = {
                            "user_type": "user",
                            "email": email!.text.trim(),
                            "request_key": "app"
                          };
                          forgorPassword(data, context);
                        }
                      }, buttonColor, width: 280)),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child:
                            text3("Remember the password? ", 12.5, Colors.grey),
                      ),
                      GestureDetector(
                        onTap: () {
                          pop(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 18.0),
                          child: text3("Log in here", 12.5, buttonColor),
                        ),
                      ),
                    ],
                  ),

                  // longbuttons('Login', () {}, primaryColor),
                  // longbuttons('Register', () {}, Colors.black),
                ],
              ),
            ),
          ),
        ));
  }

  bool isloading = false;

  forgorPassword(data, context) async {
    isloading = true;
    setState(() {});
    var resp = await Controller().resetPassword(data);
    //log(resp.toString());
    if (resp['succ'] == 0) {
      showToast(resp['errors']);
    } else {}
    isloading = false;
    setState(() {});
  }
}
