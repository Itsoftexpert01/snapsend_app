import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Auth/OTP/otp_screen.dart';
import 'package:snap_send_app/View/Auth/login_screen.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool isloading = false;
  TextEditingController? emailaddress = TextEditingController();
  TextEditingController? firstname = TextEditingController();
  TextEditingController? lastname = TextEditingController();
  TextEditingController? username = TextEditingController();
  TextEditingController? phoneno = TextEditingController();

  TextEditingController? password = TextEditingController();
  TextEditingController? confirmpassword = TextEditingController();
  // ignore: prefer_typing_uninitialized_variables
  var countries;

  var states;
  var cities;
  List<String> countriesList = [];
  List<String> countriesStates = [];

  List<String> countriesCities = [];
//getCountriesList
  getCountries() async {
    // isloading = true;
    // setState(() {});
    var data = await Controller().getAllCountries();
    countries = data['data']['app_countries'];
    for (int i = 0; i < countries.length; i++) {
      countriesList.add(countries[i]['name']);
    }
    await getStates(231);
    selectedCountry = "United States";
    countryIndex = 230;
    // isloading = false;
    setState(() {});
  }

//getStatesList
  getStates(id) async {
    countriesStates = [];
    isloading = true;
    setState(() {});
    var data = {"country": id};
    var resp = await Controller().getAllStates(data);
    states = resp['data']['app_states'];
    for (int i = 0; i < states.length; i++) {
      countriesStates.add(states[i]['name']);
    }
    isloading = false;
    setState(() {});
  }

//getCitiesList
  getCities(id) async {
    countriesCities = [];
    isloading = true;
    setState(() {});
    var data = {"state": id};
    var resp = await Controller().getAllCities(data);
    cities = resp['data']['app_cities'];
    for (int i = 0; i < cities.length; i++) {
      countriesCities.add(cities[i]['name']);
    }
    isloading = false;
    setState(() {});
  }

  register(context) async {
    if (username!.text.isNotEmpty ||
        password!.text.isNotEmpty ||
        firstname!.text.isNotEmpty ||
        lastname!.text.isNotEmpty ||
        emailaddress!.text.isNotEmpty) {
      if (password!.text == confirmpassword!.text) {
        if (!validateStructure(password!.text)) {
          showToast('''Password should contains
              Minimum 1 Upper case,
              Minimum 1 lowercase,
              Minimum 1 Numeric Number,
              Minimum 1 Special Character,
              Common Allow Character ( ! @ #  & * ~ )''');
        } else {
          var data = {
            "login_method": "user_pass",
            "country": countries[countryIndex]['id'],
            "state": states[stateIndex]['id'],
            "city": cities[cityIndex]['id'],
            "username": username?.text.trim(),
            "password": password?.text.trim(),
            "first_name": firstname?.text.trim(),
            "last_name": lastname?.text.trim(),
            "country_ext": initialCode.toString().split("+")[1],
            "mobile": phoneno?.text.trim(),
            "email": emailaddress?.text.trim(),
            "confirm_password": confirmpassword?.text.trim(),
            "role_id": 5,
            "user_details": true,
            "status":1
          };
         // log(data.toString());
          isloading = true;
          setState(() {});
          var resp = await Controller().signUp(data);
          if (resp['succ'] == 0) {
            showToast(resp['errors']);
          } else {
            // /navigateAndRemove
            showToast(resp['msg'].toString());
            nav(
                context,
                OTPScreen(initialCode.toString().split("+")[1],
                    phoneno!.text.trim()));
          // navigateAndRemove(context, LoginScreen());
            //pop(context);
          }
          isloading = false;
          setState(() {});
        }
      } else {
        showToast('Password & confirm password doesn\'t match');
      }
    } else {
      showToast('Please fill all the fields first');
    }
  }

  bool validateStructure(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  FocusNode? _focusNode;
  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    getCountries();
  }

  @override
  void dispose() {
    _focusNode?.dispose();
    textEditingController.dispose();

    super.dispose();
  }

  String? selectedCountry;
  String? selectedState;
  String? selectedCity;
  String? initialCode;

  bool obsecureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title:  Text(
            'Register Account'.tr,
            textAlign: TextAlign.center,
            style: const TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              pops(context);
            },
          ),
        ),
        // backgroundColor: primaryColor,
        body: ModalProgressHUD(
          inAsyncCall: isloading,
          progressIndicator: spinkit,
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0),
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Image.asset(
                        'assets/snapsend_logo.png',
                        // fit: BoxFit.fill,
                      ),
                    ),
                  ),
                   Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Join Snapsend'.tr,
                      textAlign: TextAlign.center,
                      style:
                          const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                    ),
                  ),
                   Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Text(
                      'Join these and 10,465,789 other private citizens'.tr,
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: shortButton('Private Citizen', () {}, buttonColor,
                        fontSize: 14),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 60.0, top: 10, right: 60.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'ُPersonal information'.tr,
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'First Name'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: textformfield1(
                                focusNode: _focusNode,
                                controller: firstname,
                                onTap: () {
                                //  log("im hitted");
                                  FocusScope.of(context)
                                      .requestFocus(_focusNode);
                                 // log("im hitted after fouccs");
                                  setState(() {});
                                },
                                hinttext: 'First Name'),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'Last Name'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: textformfield1(
                                controller: lastname, hinttext: 'Last Name'),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'User Name'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: textformfield1(
                                controller: username, hinttext: 'User Name'),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'Phone Number'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: IntlPhoneField(
                              dropdownDecoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5)),
                              decoration: InputDecoration(
                                //   labelText: 'Phone Number',
                                border: OutlineInputBorder(
                                    borderSide: const BorderSide(),
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                              initialCountryCode: 'US',
                              onChanged: (phone) {
                                phoneno!.text = phone.number;
                                initialCode = phone.countryCode;
                              //  log(phoneno!.text);
                              },
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 8.0),
                            child: Text(
                              'Email',
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: textformfield1(
                                controller: emailaddress,
                                hinttext: 'Email address'),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'Country'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          dropdownbutton(
                              'Country', countriesList, selectedCountry),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'State, Province, Territory'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          dropdownbutton(
                              'State', countriesStates, selectedState),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'City, Town, Village'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          dropdownbutton('City', countriesCities, selectedCity),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Set Password'.tr,
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'Password'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: textformfield1(
                                obscureText: obsecureText,
                                onPressed: () {
                                  obsecureText = !obsecureText;
                                  setState(() {});
                                },
                                controller: password,
                                icon: !obsecureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                hinttext: 'Password'),
                          ),
                           Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Text(
                              'Confirm Password'.tr,
                              style: const TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: textformfield1(
                              obscureText: obsecureText,
                              onPressed: () {
                                obsecureText = !obsecureText;
                                setState(() {});
                              },
                              controller: confirmpassword,
                              hinttext: 'Confirm Password',
                              icon: !obsecureText
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 60, right: 60.0),
                      child: longbuttons('Register To Snapsend', () {
                        register(context);
                      }, buttonColor, width: 300)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: text3("Already Account? ", 12.5, Colors.grey),
                      ),
                      GestureDetector(
                        onTap: () {
                          nav(context, LoginScreen());
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 18.0),
                          child: text3("Login", 12.5, buttonColor),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  )
                ],
              ),
            ),
          ),
        ));
  }

  var countryIndex = 0;
  var stateIndex = 0;
  var cityIndex = 0;
  String? selectedValue;
  dropdownbutton(txt, List<String> list, value) {
    return DropdownButtonFormField2<String>(
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),

        // Add more decoration..
      ),
      hint: Text(
        value == null ? '$txt'.tr : '$value',
        style: const TextStyle(fontSize: 14, color: Colors.black),
      ),
      items: list
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(fontSize: 14, color: Colors.black),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select gender.';
        }
        return null;
      },
      onChanged: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        if (txt == "Country") {
          selectedCountry = selectedValue;
          countryIndex = countriesList.indexOf(value.toString());

          log(countries[countryIndex].toString());
          getStates(countries[countryIndex]['id']);
        } else if (txt == "State") {
          selectedState = selectedValue;
          stateIndex = countriesStates.indexOf(value.toString());

          log(countriesStates[stateIndex].toString());
          getCities(states[stateIndex]['id']);
        } else if (txt == "City") {
          selectedCity = selectedValue;
          cityIndex = countriesCities.indexOf(selectedCity.toString());
        }

        //Do something when selected item is changed.
      },
      onSaved: (value) {
        selectedValue = value.toString();
        print(selectedValue.toString());

        // var index = countriesList.indexOf(value.toString());
        // print(index.toString());
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
      dropdownSearchData: DropdownSearchData(
        searchController: textEditingController,
        searchInnerWidgetHeight: 50,
        searchInnerWidget: Container(
          height: 50,
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            expands: true,
            maxLines: null,
            controller: textEditingController,
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 8,
              ),
              hintText: 'Search...'.tr,
              hintStyle: const TextStyle(fontSize: 12),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ),
        searchMatchFn: (item, searchValue) {
          return item.value.toString().toLowerCase().contains(searchValue.toLowerCase());
        },
      ),
      //This to clear the search value when you close the menu
      onMenuStateChange: (isOpen) {
        if (!isOpen) {
          textEditingController.clear();
        }
      },
    );
  }

  final TextEditingController textEditingController = TextEditingController();
}
