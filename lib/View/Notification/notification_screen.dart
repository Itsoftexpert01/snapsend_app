import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Notification/utils/widgets.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  dynamic notifications;

  bool isloading = false;

  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  var formattedDate;

  @override
  void initState() {
    super.initState();
    getNotices();
  }

  getNotices() async {
    isloading = true;
    setState(() {});

    var response = await Controller().getNotifications({"request_key": "app"});

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      notifications = response['data']['notification'];
    }

    isloading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            backgroundColor: buttonColor,
            title:  Text('Notifications'.tr,),  actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],),
        body: isloading
            ? Center(
                child: spinkit,
              )
            : Padding(
                padding: const EdgeInsets.only(top: 10, left: 14, right: 14),
                child: notifications.length == 0
                    ?  Center(
                        child: Text("No Notification Found".tr),
                      )
                    : SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: notifications.length,
                              itemBuilder: ((context, index) => InkWell(
                                    onTap: () {
                                      // Navigator.push(context,
                                      //     MaterialPageRoute(builder: (context) =>const Crisesteam()));
                                    },
                                    child: chatcard(
                                        context: context,
                                        text1: 'Mia Gonzalez',
                                        text2: 'How are you doing?',
                                        text3: '03:00 pm',
                                        img: 'assets/snapsend_logo.png',
                                        text4: '4'),
                                  )),
                            ),

                            const SizedBox(
                              height: 5,
                            ),
                            
                          ],
                        ),
                      ),
              ));
  }
}
