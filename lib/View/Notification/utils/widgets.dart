

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/model/color.dart';

import 'color.dart';

Widget text1(dynamic text, double fontsize, Color? color ) {
  return Text(
    text, style: TextStyle(fontSize: fontsize, color: color,letterSpacing: 0.5),
  );
}
Widget text2(String text, double fontsize, Color? color ) {
  return Text(
    text.tr, style: TextStyle(fontSize: fontsize, color: color,fontWeight: FontWeight.bold,
    letterSpacing: 0.5,),
  );
}
Widget text7(dynamic text, double fontsize, Color? color ) {
  return Text(
    text, style: TextStyle(fontSize: fontsize, color: color,fontWeight: FontWeight.bold,
      letterSpacing: 0.1
  ),
  );
}

Widget text3(dynamic text, double fontsize, Color? color ) {
  return Text(
    text, style: TextStyle(fontSize: fontsize, color: color,overflow: TextOverflow.fade,letterSpacing: 0.3),
  );
}
Widget text5(dynamic text, double fontsize, Color? color ) {
  return Text(
    text, style: TextStyle(fontSize: fontsize, color: color,letterSpacing: 0.5),
  );
}
Widget text4(dynamic text, double fontsize, Color? color ) {
  return Text(
    text, style: TextStyle(fontSize: fontsize, color: color,fontWeight: FontWeight.bold,
      letterSpacing: 0.15
  ),
  );
}

Widget chatcard({BuildContext? context,dynamic text1, dynamic text4,dynamic text2,dynamic text3,dynamic img}){
  return  Padding(
      padding: const EdgeInsets.symmetric(vertical: 7),
      child: Card(elevation: 3.0,
            color: ColorSelect().white,
            shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: Colors.white),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ClipRRect(borderRadius: BorderRadius.circular(100), child: Image.asset(img,fit: BoxFit.fill,height: 40,),),
                  Container(
                    height: 74,width: 180,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 10,),
                        Text(text1, style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 15,overflow: TextOverflow.ellipsis),),
                        const SizedBox(height: 10,),
                        Text(text2, style: TextStyle(color: Colors.grey,fontSize: 13,overflow: TextOverflow.ellipsis),),
                      ],),
                  ),

               Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                 Text(text3, style: TextStyle(color: Colors.black54,fontSize: 11.5,overflow: TextOverflow.ellipsis),),
                 SizedBox(height: 17,),
               Container(
                 height: 22,width: 22,
                 decoration: BoxDecoration(
                   color: buttonColor,
                   shape: BoxShape.circle
                 ),
                 child:   Center(child: Text(text4, style: TextStyle(color: ColorSelect().white,fontSize: 10,overflow: TextOverflow.ellipsis),)),
               )
               ],)
                ],),
            )),

    );

}