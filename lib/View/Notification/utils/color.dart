import 'package:flutter/material.dart';

class ColorSelect {

  Color blue1 = const Color(0xff57C7EB);
  Color blue2 =  const Color(0xff58c4ec);
  Color bg1 =  const Color(0xff1E1E1E);
  Color green1 =  const Color(0xffC1D985);
  Color lightgreen =  const Color(0xffF3FCE5);
  Color black = Colors.black;
  Color blue3 = Color(0xff10EEB5);
  Color grey5 = Color(0XFFF4F4F2);



  Color blue = Color(0xff39B7FF);
  Color green = Color(0xffC1D985);

  Color black54 = Colors.black54;
  Color? greylight = Color(0xffF1F1F1);
  Color grey = Color(0xffD9D9D9);
  Color white = Colors.white;
  Color bluegreyy = Colors.blueGrey;
  Color red = const Color(0xffDC4E41);
  Color bluegrey = const Color(0xff155F76);
}

