import 'dart:convert';
import 'dart:developer';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:map_location_picker/map_location_picker.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/search_tip_details.dart';
import 'package:snap_send_app/View/Settings/utils/dialogs.dart';
import 'package:snap_send_app/View/Tip/TipFunctions/tip_functions.dart';
import 'package:snap_send_app/View/Tip/TipFunctions/audio_player.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';
import 'package:video_player/video_player.dart';

class NewInformation extends StatefulWidget {
  dynamic report;
  NewInformation({this.report});
  @override
  State<NewInformation> createState() => _NewInformationState();
}

class _NewInformationState extends State<NewInformation> {
  var tiptypes;
  var carmake;
  var carmodel;
  var yearofcar;
  var carcolor;
  String? departmentid;
  String? tiptypeid;
  String? carmakeid;
  String? carmodelid;
  String? yearofcarid;
  String? carcolorid;
  List<String> tipTypeList = [];
  List<String> carmakeList = [];
  List<String> carmodelList = [];
  List<String> yearofcarList = [];
  List<String> carcolorList = [];
  List<int> carmakeIdList = [];
  List<int> carmodelIdList = [];
  List<int> yearofcarIdList = [];
  List<int> carcolorIdList = [];
  String selectedTipType = "Select";
  String selectedcarmake = "Select";
  String selectedcarmodel = "Select";
  String selectedyearofcar = "Select";
  String selectedcarcolor = "Select";
  bool isloading = false;

  dynamic departments;
  List<String> departmentList = [];
  String selectedDepartment = "Select";

  final _formKey = GlobalKey<FormState>();
  TextEditingController dateController = TextEditingController();
  TextEditingController dateController2 =
      TextEditingController(text: DateTime.now().toString().split(" ")[0]);
  TextEditingController timeController = TextEditingController();
  TextEditingController timeController2 =
      TextEditingController(text: "12:00 AM");
  TextEditingController tipdetails = TextEditingController();
  TextEditingController location = TextEditingController();

  TextEditingController license = TextEditingController();

  TextEditingController numberofpeople = TextEditingController();

  dynamic selectedImage;
  dynamic selectedVideo;
  dynamic selectedAudio;
  late VideoPlayerController _controller;

  initializeVideo() async {
    _controller = VideoPlayerController.file(selectedVideo['asset'])
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    _controller.play();
  }

  @override
  void dispose() {
    try {
      _controller.dispose();
      textEditingController.dispose();
    } catch (e) {}
    //audioPlayer.dispose();

    super.dispose();
  }

  getDepartments(context) async {
    isloading = true;
    setState(() {});
    var data = {"department_type": 1};

    var response = await Controller().getDepartment(data);
    log('selectedDepartment is $selectedDepartment and name is ${widget.report['name']}');
    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      departments = response['data'];
      for (int i = 0; i < departments['user_department_in_array'].length; i++) {
        departmentList.add(departments['user_department_in_array'][i]);
        if (departments['user_department_in_array'][i] ==
            widget.report['name']) {
          selectedDepartment = departments['user_department_in_array'][i];
          departmentid = departments['user_department_id'][i].toString();
          log('selectedDepartment is $selectedDepartment and name is $departmentid');
        }
      }
//log(selectedDepartment)
      // selectedReportid = getPreviousReports[int.parse(id)]['id'].toString();
      // selectedDepartment = value.toString();
      // departmentid = items.indexOf(selectedDepartment).toString();
    }

    isloading = false;
    setState(() {});
  }

  getEntities(context) async {
    isloading = true;
    setState(() {});

    var tiptypes = await Controller().webEntity({"type": "report-type"});
    var carmake = await Controller().webEntity({"type": "car-make"});
    var carmodel = await Controller().webEntity({"type": "car-model"});
    var yearofcar = await Controller().webEntity({"type": "car-year"});
    var carcolor = await Controller().webEntity({"type": "color"});

    selectedDepartment = widget.report['name'];
    departmentid = widget.report['file_tip_department'].toString();
    // await getDepartments(context);
    tiptypes = tiptypes['data']['app_entity'];
    for (int i = 0; i < tiptypes.length; i++) {
      tipTypeList.add(tiptypes[i]['title']);
      if (tiptypes[i]['id'] == widget.report['report_type']) {
        selectedTipType = tiptypes[i]['title'];
        tiptypeid = tiptypes[i]['id'].toString();
      }
    }
    carmake = carmake['data']['app_entity'];
    for (int i = 0; i < carmake.length; i++) {
      carmakeList.add(carmake[i]['title']);
      carmakeIdList.add(carmake[i]['id']);
    }
    carmodel = carmodel['data']['app_entity'];
    for (int i = 0; i < carmodel.length; i++) {
      carmodelList.add(carmodel[i]['title']);
      carmodelIdList.add(carmodel[i]['id']);
    }
    yearofcar = yearofcar['data']['app_entity'];
    for (int i = 0; i < yearofcar.length; i++) {
      yearofcarList.add(yearofcar[i]['title']);
      yearofcarIdList.add(yearofcar[i]['id']);
    }
    carcolor = carcolor['data']['app_entity'];
    for (int i = 0; i < carcolor.length; i++) {
      carcolorList.add(carcolor[i]['title']);
      carcolorIdList.add(carcolor[i]['id']);
    }

    isloading = false;
    setState(() {});
  }

  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    // getCustomerReport();

    log(user.toString());
    // isloading = false;
    //  setState(() {});
  }

  submitTip(context) async {
    isloading = true;
    setState(() {});

    var data2 = {
      "filePics": selectedImage == null ? [] : ["${selectedImage['base64']}"],
      "fileVideo": selectedVideo == null ? [] : ["${selectedVideo['base64']}"],
      "fileVideoTrack":
          selectedAudio == null ? "" : "${selectedAudio['base64']}",
      "new_report": 0,
      "report_type": tiptypeid,
      "department_id": departmentid,
      "user_id": user['id'],
      "description": tipdetails.text.trim(),
      "location": location.text.trim(),
      "file_reported_date":
          //DateTime.now().toString().split(" ")[0],
          dateController2.text.trim(),
      //DateTime.now().toString().split(" ")[0],
      "file_reported_time": timeController.text.trim(),
      // DateTime.now().toString().split(" ")[1],
      "no_of_people_inv": numberofpeople.text.trim(),
      "suspect_name": suspectname.text.trim(),
      "gang_name": gangname.text.trim(),
      "license_plate_number": license.text.trim(),
      "car_model": carmodelid,
      "make_model_of_car": yearofcarid,
      "color_of_car": carcolorid,
      "car_make": carmakeid,
      "add_contact": _yesSelected ? 1 : 0,
      "report_id": selectedReportid
    };
    var response = await Controller().createfileReport(data2);
    log(response.toString());
    isloading = false;
    setState(() {});

    if (response['succ'] == 0) {
      showToast("Error");
    } else {
      Get.dialog(
        TipDialog(response['msg'].toString(), isfrom: "old tip"),
      );
    }
  }

  var getPreviousReports;
  List<String> customerReportList = [];
  String selectedReport = "Select";
  getCustomerReport() async {
    getPreviousReports =
        await TipFunctions().getCustomerReport(user['id'], context);

    for (int i = 0; i < getPreviousReports.length; i++) {
      customerReportList.add(
          "${getPreviousReports[i]['id']} ${getPreviousReports[i]['report_title']} ${getPreviousReports[i]['name']}");
    }
    log("the previous reports are ${customerReportList.toString()}");
  }

  @override
  void initState() {
    super.initState();
    selectedReportid = widget.report['id'].toString();
    getEntities(context);
    getuserDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: buttonColor,
        leading: IconButton(
            onPressed: () {
              pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios)),
        title: Text('File A Tip'.tr),
        actions: [
          GestureDetector(
            onTap: () {
              nav(context, ReportDetailPage(widget.report));
            },
            child: Chip(
              backgroundColor: primaryColor,
              label: Text(
                "View Tip".tr,
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 5,
          ),
        ],
      ),
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        progressIndicator: spinkit,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: SizedBox(
            height: height(context),
            width: width(context),
            child: ListView(
              children: [
                childWidget(context),
              ],
            ),
          ),
        ),
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.only(bottom: 28.0),
          child: Center(
              child: longbuttons('Submit', () {
            submitTip(context);
          }, buttonColor, width: 190)),
        ),
      ],
    );
  }

  Widget childWidget(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      elevation: 3.0,
      child: Form(
        key: _formKey,
        child: ListView(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 100,
                width: 100,
                child: Image.asset(
                  'assets/snapsend_logo.png',
                  // fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Tip information'.tr,
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      'follow the steps below to submit a tip'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  // const Padding(
                  //   padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                  //   child: Text(
                  //     'Is New Tip',
                  //     style:
                  //         TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  // isNewradioButton(),

                  !isnewtip
                      ? Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: Text(
                                'Add to Past Tip'.tr,
                                style: const TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            dropdownbutton(selectedReport, customerReportList,
                                'Choose past tip'),
                          ],
                        )
                      : const SizedBox(),

                  Padding(
                    padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
                    child: Text(
                      'Add My Contact Information (if this is an anonymous tip hit the NO):'
                          .tr,
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  radioButton(),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Destination Department'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(
                      selectedDepartment, departmentList, 'departments'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Type of Tip'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedTipType, tipTypeList, 'tiptype'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Tip Details'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield2(
                        controller: tipdetails,
                        hinttext: 'Enter details here...'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Location'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: location,
                        icon: CupertinoIcons.location,
                        hinttext: 'Address or Closest street',
                        isreadonly: true,
                        onTap: () {
                          showPlacePicker();
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Date'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: dateController,
                        icon: CupertinoIcons.calendar,
                        hinttext: 'Select Date',
                        isreadonly: true,
                        onTap: () {
                          _pickDate();
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Time'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: timeController,
                        icon: CupertinoIcons.time,
                        hinttext: 'Select Time',
                        isreadonly: true,
                        onTap: () {
                          _pickTime();
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Car Make'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedcarmake, carmakeList, 'carmake'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Car Model'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedcarmodel, carmodelList, 'carmodel'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Year of Car'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedyearofcar, yearofcarList, 'yearofcar'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Car Color'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton(selectedcarcolor, carcolorList, 'carcolor'),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'License Plate Number'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: license,
                        icon: CupertinoIcons.creditcard_fill,
                        hinttext: 'License Plate Number'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Number of people involved'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: textformfield1(
                        controller: numberofpeople,
                        icon: CupertinoIcons.person,
                        hinttext: '0 Person'),
                  ),
                  checkbox(),

                  Padding(
                    padding: const EdgeInsets.only(top: 12.0, bottom: 8.0),
                    child: Text(
                      'Upload Files'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      picVidVoi(CupertinoIcons.camera, () async {
                        selectedImage =
                            await TipFunctions().pickAndConvertImage();
                        setState(() {});
                        if (selectedImage != null) {
                          showToast("Image has been Selected");
                        }
                      }),
                      picVidVoi(CupertinoIcons.video_camera, () async {
                        selectedVideo =
                            await TipFunctions().pickAndConvertMedia();
                        if (selectedImage != null) {
                          showToast("Video has been Selected");
                          await initializeVideo();
                          setState(() {});
                        }
                      }),
                      picVidVoi(CupertinoIcons.mic, () async {
                        selectedAudio = await TipFunctions().nothings();
                        setState(() {});
                        if (selectedAudio != null) {
                          showToast("Audio has been Selected");
                        }
                      })
                    ],
                  ),

                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      selectedImage != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.memory(
                                base64Decode(selectedImage['base64']
                                        .toString()
                                        .split(",")[
                                    1]), // Decoding and displaying the image
                                width: 100,
                                height: 100,
                                fit: BoxFit.fill,
                              ),
                            )
                          : Container(),
                      selectedVideo != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: SizedBox(
                                width: 100,
                                height: 100,
                                child: AspectRatio(
                                  aspectRatio: _controller.value.aspectRatio,
                                  child: VideoPlayer(_controller),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                  const SizedBox(height: 20),
                  selectedAudio != null
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: SizedBox(
                              width: 250,
                              height: 80,
                              child: AudioPlay(asset: selectedAudio['asset'])),
                        )
                      : const SizedBox(),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _pickDate() async {
    DateTime? selectedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );
    if (selectedDate != null) {
      setState(() {
        dateController2.text = selectedDate.toString().split(" ")[0];
        dateController.text =
            "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}";
      });
    }
  }

  Future<void> _pickTime() async {
    TimeOfDay? selectedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (selectedTime != null) {
      setState(() {
        timeController.text = selectedTime.format(context);
      });
    }
  }

  String address = "null";
  String autocompletePlace = "null";
  Prediction? initialValue;
  void showPlacePicker() async {
    TextEditingController searchLoc = TextEditingController();
    try {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Location'.tr),
            content: SizedBox(
              height: 300,
              width: 300,
              child: PlacesAutocomplete(
                controller: searchLoc,
                searchController: searchLoc,
                apiKey: Config.mapKey,
                searchHintText: "Search for a place".tr,
                mounted: mounted,
                hideBackButton: true,
                initialValue: initialValue,
                onSuggestionSelected: (value) {
                  setState(() {
                    autocompletePlace =
                        value.structuredFormatting?.mainText ?? "";
                    initialValue = value;
                  });
                },
                onGetDetailsByPlaceId: (value) {
                  setState(() {
                    address = value?.result.formattedAddress ?? "";
                  });
                },
                onChanged: (value) {},
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Done'.tr),
                onPressed: () {
                  location.text = searchLoc.text;
                  print("the location is ${location.text}");
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      // var kGoogleApiKey = Config.mapKey;

      // void onError(PlacesAutocompleteResponse response) {
      //   ScaffoldMessenger.of(context).showSnackBar(
      //     SnackBar(
      //       content: Text(response.errorMessage ?? 'Unknown error'),
      //     ),
      //   );
      // }

      // final Prediction? p = await PlacesAutocomplete.show(
      //   context: context,
      //   apiKey: kGoogleApiKey,
      //   onError: onError,
      //   mode: Mode.overlay, // or Mode.fullscreen
      //   language: 'fr',
      //   components: [const Component(Component.country, 'fr')],
      // );
      // log(p!.description.toString());
      // location.text = p.description.toString();
      // setState(() {});
      // LocationResult result =
      //     await Navigator.of(context).push(MaterialPageRoute(
      //         builder: (context) => PlacePicker(
      //               Config.mapKey,
      //               //  displayLocation: customLocation,
      //             )));
      //log(result.toString());
    } catch (e) {
      log(e.toString());
    }

    // Handle the result in your way
  }

  // void showPlacePicker() async {
  //   try {
  //     var kGoogleApiKey = Config.mapKey;

  //     void onError(PlacesAutocompleteResponse response) {
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(
  //           content: Text(response.errorMessage ?? 'Unknown error'),
  //         ),
  //       );
  //     }

  //     final Prediction? p = await PlacesAutocomplete.show(
  //       context: context,
  //       apiKey: kGoogleApiKey,
  //       onError: onError,
  //       mode: Mode.overlay, // or Mode.fullscreen
  //       language: 'fr',
  //       components: [const Component(Component.country, 'fr')],
  //     );
  //     log(p!.description.toString());
  //     location.text = p.description.toString();
  //     setState(() {});
  //     // LocationResult result =
  //     //     await Navigator.of(context).push(MaterialPageRoute(
  //     //         builder: (context) => PlacePicker(
  //     //               Config.mapKey,
  //     //               //  displayLocation: customLocation,
  //     //             )));
  //     //log(result.toString());
  //   } catch (e) {
  //     log(e.toString());
  //   }

  //   // Handle the result in your way
  // }

  picVidVoi(icon, void Function()? onPressed) {
    return Container(
      decoration: BoxDecoration(
          color: buttonColor, borderRadius: BorderRadius.circular(10)),
      height: 60,
      width: 60,
      child: Center(
          child: IconButton(
              onPressed: onPressed,
              icon: Icon(
                icon,
                color: Colors.white,
              ))),
    );
  }

  bool _yesSelected = false;
  bool _noSelected = false;
  bool isnewtip = true;
  bool ispasttip = false;

  isNewradioButton() {
    //print("is new tip $isnewtip and ispasttip is $ispasttip");
    return SizedBox(
      height: 50,
      width: width(context),
      child: Row(
        children: [
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: const Text('Yes'),
              value: true,
              groupValue: isnewtip,
              onChanged: (value) {
                setState(() {
                  isnewtip = value!;
                  ispasttip = !value; // Toggle the opposite option
                });
              },
            ),
          ),
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: const Text('No'),
              value: true,
              groupValue: ispasttip,
              onChanged: (value) {
                setState(() {
                  ispasttip = value!;
                  isnewtip = !value; // Toggle the opposite option
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  radioButton() {
    return SizedBox(
      height: 50,
      width: width(context),
      child: Row(
        children: [
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: const Text('Yes'),
              value: true,
              groupValue: _yesSelected,
              onChanged: (value) {
                setState(() {
                  _yesSelected = value!;
                  _noSelected = !value; // Toggle the opposite option
                });
              },
            ),
          ),
          Expanded(
            child: RadioListTile<bool>(
              activeColor: buttonColor,
              title: const Text('No'),
              value: true,
              groupValue: _noSelected,
              onChanged: (value) {
                setState(() {
                  _noSelected = value!;
                  _yesSelected = !value; // Toggle the opposite option
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  bool _suspectNameChecked = false;

  bool _gangNameChecked = false;

  TextEditingController suspectname = TextEditingController();
  TextEditingController gangname = TextEditingController();
  checkbox() {
    return Column(
      children: [
        CheckboxListTile(
          title: Text('Suspect Name'.tr),
          value: _suspectNameChecked,
          onChanged: (value) {
            setState(() {
              _suspectNameChecked = value!;
            });
          },
        ),
        _suspectNameChecked
            ? Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: textformfield1(
                    controller: suspectname,
                    icon: CupertinoIcons.person,
                    hinttext: 'Suspect Name'.tr),
              )
            : const SizedBox(),
        CheckboxListTile(
          title: Text('Gang Name'.tr),
          value: _gangNameChecked,
          onChanged: (value) {
            setState(() {
              _gangNameChecked = value!;
            });
          },
        ),
        _gangNameChecked
            ? Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: textformfield1(
                    controller: gangname,
                    icon: CupertinoIcons.person,
                    hinttext: 'Gang Name'.tr),
              )
            : const SizedBox(),
      ],
    );
  }

  String? selectedReportid;
  dropdownbutton(txt, List<String> items, type) {
    return DropdownButtonFormField2<String>(
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        // Add more decoration..
      ),
      hint: Text(
        '$txt'.tr,
        style: const TextStyle(fontSize: 14),
      ),
      items: items
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                      fontSize: 14, overflow: TextOverflow.ellipsis),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select gender.';
        }
        return null;
      },
      onChanged: (value) {
        if (type == "departments") {
          selectedDepartment = value.toString();
          departmentid = items.indexOf(selectedDepartment).toString();
        } else if (type == "carmake") {
          selectedcarmake = value.toString();
          carmakeid = items.indexOf(value.toString()).toString();
          carmakeid = carmakeIdList[int.parse(carmakeid!) + 1].toString();
        } else if (type == "carmodel") {
          selectedcarmodel = value.toString();
          carmodelid = items.indexOf(value.toString()).toString();
          carmodelid = carmodelIdList[int.parse(carmodelid!)].toString();
        } else if (type == "yearofcar") {
          selectedyearofcar = value.toString();
          yearofcarid = items.indexOf(value.toString()).toString();
          yearofcarid = yearofcarIdList[int.parse(yearofcarid!)].toString();
        } else if (type == "carcolor") {
          selectedcarcolor = value.toString();
          carcolorid = items.indexOf(value.toString()).toString();
          carcolorid = carcolorIdList[int.parse(carcolorid!)].toString();
        }
        else if (type == "Choose past tip") {
          selectedReport = value.toString();
          var id = items.indexOf(value.toString()).toString();
         
        }
        //Do something when selected item is changed.
      },
      onSaved: (value) {
        //   selectedValue = value.toString();
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
      dropdownSearchData: DropdownSearchData(
        searchController: textEditingController,
        searchInnerWidgetHeight: 50,
        searchInnerWidget: Container(
          height: 50,
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            expands: true,
            maxLines: null,
            controller: textEditingController,
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 8,
              ),
              hintText: 'Search...',
              hintStyle: const TextStyle(fontSize: 12),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ),
        searchMatchFn: (item, searchValue) {
          return item.value
              .toString()
              .toLowerCase()
              .contains(searchValue.toLowerCase());
        },
      ),
      //This to clear the search value when you close the menu
      onMenuStateChange: (isOpen) {
        if (!isOpen) {
          textEditingController.clear();
        }
      },
    );
  }

  final TextEditingController textEditingController = TextEditingController();
}
