import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/View/Search/controller/search_controller.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/search_tip_details.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/new_file_tip.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

// ignore: must_be_immutable
class SearchTip extends StatefulWidget {
  dynamic isfrom;
  SearchTip({super.key, this.isfrom});
  @override
  // ignore: library_private_types_in_public_api
  _SearchTipState createState() => _SearchTipState();
}

class _SearchTipState extends State<SearchTip> {
  dynamic reports;
  List departmentList = [];

  bool isloading = false;

  dynamic user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  dynamic formattedDate;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    log(user.toString());
    await getCustomerReport();
    final DateTime currentDate = DateTime.now();
    final DateFormat formatter = DateFormat('EEEE, d MMMM y', 'en_US');

    formattedDate = formatter.format(currentDate);
  
  }

  @override
  void initState() {
    super.initState();
    getuserDetails();
  }

  String filter = "";
  String searchKey = "";
  getCustomerReport() async {
    var data = {
      "filter": filter,
      "search_key": searchKey,
      "user_id": "${user['id']}",
    };
    isloading = true;
    setState(() {});

    var response = await SearchControllers().getCustomerReport(data);
    reports = response;

    isloading = false;
    setState(() {});
  }

  //Popular Widget
  Widget popularWidget(title, subtitle, time, like, report) => Container(
          child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          //set border radius more than 50% of height and width to make circle
        ),
        elevation: 3.0,
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            SizedBox(
                              width: 120,
                              child: Text(
                                "Snap Send No :-".tr,
                                style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontSize: 15,
                                  color: buttonColor,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 180,
                              child: Text(
                                title,
                                style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontSize: 15,
                                  color: buttonColor,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 120,
                              child: Text(
                                "Report Type :-".tr,
                                style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontSize: 16,
                                  color: buttonColor,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                            Text(
                              subtitle,
                              style: const TextStyle(
                                  color: Colors.black,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.w800,
                                  fontSize: 17),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                IconButton(
                                    icon: Icon(
                                      Icons.calendar_month,
                                      size: 22,
                                      color: buttonColor,
                                    ),
                                    onPressed: null),
                                Text(
                                  time,
                                  style: const TextStyle(fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                GestureDetector(
                                  onTap: () async {
                                    // nav(
                                    //     context,
                                    //     NewInformation(
                                    //       report: report,
                                    //     ));

                                    final result = await Navigator.of(context,
                                            rootNavigator: true)
                                        .push(
                                      MaterialPageRoute(
                                        builder: (context) => NewInformation(
                                          report: report,
                                        ),
                                      ),
                                    );
                                    print(
                                        'Received result from Second Screen: $result');
                                    // Check if a result was returned from the second screen.
                                    if (result == null) {
                                      // Do something with the result.
                                      getCustomerReport();
                                      // getUserDetails();
                                    }
                                  },
                                  child: Chip(
                                    backgroundColor: buttonColor,
                                    label: Text(
                                      'New Information'.tr,
                                      style: const TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            ),
                       
                          ],
                        )
                      ]),
                ),
              )
            ],
          ),
        ),
      ));
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: buttonColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            if (widget.isfrom == null) {
              navPushReplacement(context, MyBottomNavigationBar());
            } else {
              pops(context);
            }
          },
          icon: const Icon(
            Icons.arrow_back_ios_new,
          ),
          color: Colors.white,
        ),
        title: text2('Search Report', 18, Colors.white),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : SafeArea(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 0, left: 25, right: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        //  getImage(),

                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Reports".tr,
                              style: const TextStyle(
                                  fontSize: 27, fontWeight: FontWeight.w700),
                            ),
                            Text(
                              "",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  color: blue),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                10.0), // Adjust the radius as needed
                            border: Border.all(
                              color: Colors.grey,
                              width: 1.0,
                            ),
                          ),
                          child: TextFormField(
                            controller: searchController,
                            // onChanged: _performSearch,
                            decoration: InputDecoration(
                              hintText: 'Search...'.tr,
                              contentPadding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              border: const OutlineInputBorder(
                                borderSide:
                                    BorderSide.none, // Remove the border
                              ),
                              suffixIcon: IconButton(
                                icon: const Icon(
                                  Icons.search,
                                  weight: 2.0,
                                  size: 30,
                                ),
                                onPressed: () async {
                                  searchKey = searchController.text.trim();
                                  await getCustomerReport();
                                },
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        reports == null || reports.length == 0
                            ? Center(
                                child: Text(
                                'No Reports Found'.tr,
                                style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black),
                              ))
                            : ListView.builder(
                                physics: const ScrollPhysics(),
                                itemCount: reports.length,
                                shrinkWrap: true,
                                itemBuilder: ((context, i) {
                                  // log("the parent ids are $i ${reports[i]['parent_id']}\n");
                                  return InkWell(
                                      onTap: () {
                                        nav(
                                          context,
                                          ReportDetailPage(reports[i]),
                                        );
                                      },
                                      child: popularWidget(
                                        "${reports[i]['snap_send_no']}",
                                        "${reports[i]['report_title']}",
                                        "${reports[i]['file_reported_date']}",
                                        "${reports[i]['file_reported_time']}",
                                        reports[i],
                                      ));
                                })),

                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
