import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Search/controller/search_controller.dart';
import 'package:snap_send_app/View/Tip/TipFunctions/audio_player.dart';

import 'package:video_player/video_player.dart';

// ignore: must_be_immutable
class ReportDetailPage extends StatefulWidget {
  dynamic reportData;

  ReportDetailPage(this.reportData,  {super.key});

  @override
  State<ReportDetailPage> createState() => _ReportDetailPageState();
}

class _ReportDetailPageState extends State<ReportDetailPage> {
  String filter = "";
  String searchKey = "";
  bool isloading = false;
  dynamic reports;
  getCustomerReport() async {
    var data = {
      "filter": filter,
      "search_key": searchKey,
      "id": widget.reportData['id'],
    };
    isloading = true;
    setState(() {});

    var response = await SearchControllers().getCustomerReport2(data);
       // dynamic response2 = await SearchControllers().getReportDetails(data['id']);

    reports = response;

    isloading = false;
    setState(() {});
  }

  var data;

  @override
  void initState() {
    super.initState();
    getCustomerReport();
    data = json.decode(widget.reportData['meta_data']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Report Details'.tr),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              pop(context);
            },
          ),  actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],
        ),
        body: isloading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                children: [
                  ListView.builder(
                    physics: const ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: reports['customer_report'].length,
                    itemBuilder: ((context, i) {
                      data = json
                          .decode(reports['customer_report'][i]['meta_data']);
                      return reportWidget(reports['customer_report'][i], data);
                    }),
                  ),
                  reports['report_history'] == null ||
                          reports['report_history'].isEmpty
                      ? Container()
                      : const Padding(
                          padding: EdgeInsets.only(left: 18.0),
                          child: Text(
                            "New Information :",
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.bold),
                          ),
                        ),
                  ListView.builder(
                    physics: const ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: reports['report_history'].length,
                    itemBuilder: ((context, i) {
                     var datas = json
                          .decode(reports['report_history'][i]['meta_data']);
                      return reportWidget(reports['report_history'][i], datas);
                    }),
                  ),
                ],
              ));
  }

  Widget imgDialog(imageUrl) {
    return Dialog(
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop(); // Close the dialog on tap
        },
        child: CachedNetworkImage(
          width: 350,
          height: 350,
          imageUrl: '${Config.imageURL}/web/report/picture/$imageUrl',
          placeholder: (context, url) => Center(child: spinkit),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }

  Widget reportWidget(reportdata, data) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${'Report ID'.tr}: ${reportdata['id']}',
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
          ),
          const SizedBox(height: 8.0),
          Text(
            '${'Snap Send No'.tr}: ${reportdata['snap_send_no'] ?? ""}',
            style: const TextStyle(fontSize: 16.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Description:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            reportdata['description'].toString(),
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Location:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            '${reportdata['location']}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Reported Date and Time:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            '${reportdata['file_reported_date']} ${reportdata['file_reported_time']}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'No. of People Involved: '.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            '${reportdata['no_of_people_inv']}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Suspect Name: '.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            '${reportdata['suspect_name']??''}',
            style: const TextStyle(fontSize: 14.0),
          ),
           const SizedBox(height: 16.0),
          Text(
            'Gang Name: '.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            '${reportdata['gang_name']??''}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'License Plate Number: '.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            '${reportdata['license_plate_number']??''}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Car Make and Model:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),

          Text(
            ' ${reportdata['carmake']??''} ${reportdata['carmodel']??''}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Car Year:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          Text(
            ' ${reportdata['caryear']??''} ',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Color of Car: '.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),

          Text(
            '${reportdata['carcolor']??''}',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 8.0),
          Text(
            'Images:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          const SizedBox(height: 8.0),
          if (data['pic'] != null)
            //  Display images using CachedNetworkImage
            for (String imageUrl in data['pic'])
              GestureDetector(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return imgDialog(imageUrl);
                    },
                  );
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: CachedNetworkImage(
                    width: 250,
                    height: 150,
                    imageUrl: '${Config.imageURL}/web/report/picture/$imageUrl',
                    placeholder: (context, url) => Center(child: spinkit),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
          const SizedBox(height: 8.0),
          Text(
            'Audio:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          const SizedBox(height: 8.0),
          if (data['voice_track'] != null)
            ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: SizedBox(
                    width: 250,
                    height: 80,
                    child: AudioPlay2(
                        asset: 
                            '${Config.imageURL}/web/report/voicetrack/${data['voice_track']}'))),

          // Play audio using Audioplayers
          // AudioPlayer().playerButton(
          //   context,
          //   audioUrl,
          //   backgroundColor: Colors.blue,
          //   iconColor: Colors.white,
          // ),
          const SizedBox(height: 8.0),
          Text(
            'Video:'.tr,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          const SizedBox(height: 8.0),
          // // Play video using VideoPlayer
          if (data['video'] != null)
            GestureDetector(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return vidDialog(data['video'][0]);
                  },
                );
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: VideoPlayerWidget(
                  '${Config.imageURL}/web/report/video/${data['video'][0]}',
                  200.0,
                  250.0,
                ),
              ),
            ),
          const SizedBox(height: 28.0),
        ],
      ),
    );
  }

  Widget vidDialog(videoUrl) {
    return Dialog(
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop(); // Close the dialog on tap
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: VideoPlayerWidget(
            '${Config.imageURL}/web/report/video/${data['video'][0]}',
            350.0,
            350.0,
          ),
        ),
      ),
    );
  }
}

class VideoPlayerWidget extends StatefulWidget {
  double height;
  double width;
  final String videoUrl;

  VideoPlayerWidget(this.videoUrl, this.height, this.width);

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  VideoPlayerController? _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(widget.videoUrl))
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    _controller!.addListener(() {
      setState(() {});
    });
  //  _controller!.play();
  }

  @override
  void dispose() {
    _controller!.dispose();
    super.dispose();
  }

  bool _isPlaying = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      height: widget.height,
      child: AspectRatio(
        aspectRatio: _controller!.value.aspectRatio,
        child: Stack(
          // alignment: Alignment.bottomCenter,
          children: [
            VideoPlayer(_controller!),
            Positioned(
              bottom: 0.0,
              child: SizedBox(
                height: 50,
                width: 250,
                child: VideoPlayerControls(
                  controller: _controller!,
                  isPlaying: _isPlaying,
                  onPlayPause: () {
                    if (_isPlaying) {
                      _controller!.pause();
                    } else {
                      _controller!.play();
                    }
                    setState(() {
                      _isPlaying = !_isPlaying;
                    });
                  },
                  onStop: () {
                    _controller!.seekTo(Duration.zero);
                    _controller!.pause();
                    setState(() {
                      _isPlaying = false;
                    });
                  },
                  onVolumeChanged: (double volume) {
                    _controller!.setVolume(volume);
                    setState(() {});
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class VideoPlayerControls extends StatelessWidget {
  final VideoPlayerController controller;
  final bool isPlaying;
  final VoidCallback onPlayPause;
  final VoidCallback onStop;
  final ValueChanged<double> onVolumeChanged;

  VideoPlayerControls({
    required this.controller,
    required this.isPlaying,
    required this.onPlayPause,
    required this.onStop,
    required this.onVolumeChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: Icon(
                isPlaying ? CupertinoIcons.pause : CupertinoIcons.play_arrow),
            onPressed: onPlayPause,
            color: Colors.white,
          ),
          IconButton(
            icon: const Icon(CupertinoIcons.stop),
            onPressed: onStop,
            color: Colors.white,
          ),
          Expanded(
            child: Slider(
              value: controller.value.volume,
              onChanged: onVolumeChanged,
            ),
          ),
        ],
      ),
    );
  }
}
