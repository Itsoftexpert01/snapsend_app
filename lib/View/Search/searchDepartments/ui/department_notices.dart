import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/PublicNotice/public_notice_details.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
class DepartmentNotices extends StatefulWidget {
  dynamic id;
  DepartmentNotices({super.key, this.id});

  @override
  // ignore: library_private_types_in_public_api
  _PublicNoticeState createState() => _PublicNoticeState();
}

class _PublicNoticeState extends State<DepartmentNotices> {
  dynamic notices;
  List departmentList = [];

  bool isloading = false;

  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  var formattedDate;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    log(user.toString());
    await getNotices();
    final DateTime currentDate = DateTime.now();
    final DateFormat formatter = DateFormat('EEEE, d MMMM y', 'en_US');

    formattedDate = formatter.format(currentDate);
    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getuserDetails();
  }

  getNotices() async {
    isloading = true;
    setState(() {});
    var data = {
      "type": "notice",
      "role": "5",
      "department": "${widget.id}",
      "country": user['country'],
      "state": user['state'],
      "city": user['city'],
    };

    var response = await Controller().getNotice(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      notices = response['data']['list'];
    }

    isloading = false;
    setState(() {});
  }

  //Profile Picture Widget
  Widget getImage() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Hello! "${user['first_name']}"',
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: 20),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "$formattedDate",
                style: const TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          InkWell(
            onTap: () {},
            hoverColor: Colors.deepOrange,
            child: Container(
                height: 55,
                width: 55,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    image: DecorationImage(
                        image:
                            NetworkImage("${user['profile_picture_url']}")))),
          ),

          /*Container(
        child: CircleAvatar(
          child: InkWell(
            onTap: (){},
          ),
          maxRadius: 25,
          backgroundColor: Colors.deepOrange,
          backgroundImage: NetworkImage("https://avatars2.githubusercontent.com/u/29674485?s=460&v=4"),
        ),
      )*/
        ],
      );

  //List Item
  Widget getListItem(coverImage, title, author, time, authorImage) => Container(
        //margin: const EdgeInsets.only(right: 30),
        height: 350,
        width: 290,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.elliptical(20, 20)),
          image: DecorationImage(
              image: NetworkImage(coverImage), fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20, top: 20, bottom: 10),
                      child: Container(
                        width: 45,
                        height: 45,
                        decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                                Radius.elliptical(12, 12)),
                            image: DecorationImage(
                              image: NetworkImage(authorImage),
                              fit: BoxFit.cover,
                            )),
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          author,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 17),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            const Icon(
                              Icons.access_time,
                              size: 16,
                              color: Colors.grey,
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            Text(
                              time,
                              style: const TextStyle(
                                  color: Colors.grey, fontSize: 14),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20, top: 150, bottom: 20),
                  child: Text(
                    title,
                    style: const TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )),
            ],
          ),
        ),
      );

  //Popular Widget
  Widget popularWidget(title, subtitle, time, like, image) => Container(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            //set border radius more than 50% of height and width to make circle
          ),
          elevation: 3.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(top: 4, bottom: 4),
                    height: 100,
                    width: 90,
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.elliptical(12, 12)),
                        image: DecorationImage(
                            image: NetworkImage(image), fit: BoxFit.fill))),
                Padding(
                  padding: const EdgeInsets.only(left: 25.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 180,
                          child: Text(
                            title,
                            style: TextStyle(
                              overflow: TextOverflow.ellipsis,
                              fontSize: 16,
                              color: buttonColor,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 11,
                        ),
                        SizedBox(
                          width: 180,
                          child: Text(
                            subtitle,
                            style: const TextStyle(
                                color: Colors.black,
                                overflow: TextOverflow.ellipsis,
                                fontWeight: FontWeight.w800,
                                fontSize: 14),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            const IconButton(
                                icon: Icon(
                                  Icons.access_time,
                                  size: 22,
                                ),
                                onPressed: null),
                            Text(
                              time,
                              style: const TextStyle(fontSize: 14),
                            ),
                            const Padding(padding: EdgeInsets.only(left: 12)),
                            Icon(
                              Icons.thumb_up,
                              size: 18,
                              color: Colors.grey[400],
                            ),
                            const Padding(padding: EdgeInsets.only(left: 12)),
                            Text(
                              like,
                              style: const TextStyle(fontSize: 14),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: buttonColor,
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios_new,
              color: Colors.white,
            )),
        title: text2('New Department Notice', 18, Colors.white),
  actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : notices.isEmpty
              ? SizedBox(
                  child: Center(
                    child: Text(
                      "No Public Notice Found at this time".tr,
                    ),
                  ),
                )
              : SafeArea(
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding:
                            const EdgeInsets.only(top: 10, left: 25, right: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            // getImage(),
                            // const SizedBox(
                            //   height: 20,
                            // ),
                            // Card(
                            //   shape: RoundedRectangleBorder(
                            //     borderRadius: BorderRadius.circular(10),
                            //     //set border radius more than 50% of height and width to make circle
                            //   ),
                            //   elevation: 3.0,
                            //   child: Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: Container(
                            //       width: 350,
                            //       height: 370,
                            //       child: Padding(
                            //         padding: const EdgeInsets.all(8.0),
                            //         child: ListView(
                            //           children: <Widget>[
                            //             ListView.builder(
                            //                 physics: const ScrollPhysics(),
                            //                 itemCount: 1,
                            //                 shrinkWrap: true,
                            //                 itemBuilder: ((context, i) {
                            //                   return InkWell(
                            //                       onTap: () {
                            //                         nav(
                            //                             context,
                            //                             PublicNoticeDetails(
                            //                                 img:
                            //                                     "${notices[0]['alert_image_link']}",
                            //                                 details:
                            //                                     "${notices[0]['message']}"));
                            //                       },
                            //                       child: getListItem(
                            //                         "${notices[0]['alert_image_link']}",
                            //                         "",
                            //                         "${notices[0]['type']}",
                            //                         "${notices[0]['time_of_notice']}",
                            //                         "https://scontent.fisb6-1.fna.fbcdn.net/v/t39.30808-6/346451408_783340383088316_2314026725903987275_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=7f8c78&_nc_ohc=dauQoFl18QEAX9IoXXx&_nc_ht=scontent.fisb6-1.fna&oh=00_AfCu7epI4_lpK-GX4W_RZI2GREFIgESHSNjM8PoPve7VGw&oe=64DBB92E",
                            //                       ));
                            //                 })),
                            //           ],
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            // const SizedBox(
                            //   height: 20,
                            // ),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //   children: <Widget>[
                            //     const Text(
                            //       "Latest",
                            //       style: TextStyle(
                            //           fontSize: 27,
                            //           fontWeight: FontWeight.w700),
                            //     ),
                            //     Text(
                            //       "",
                            //       style: TextStyle(
                            //           fontSize: 16,
                            //           fontWeight: FontWeight.w700,
                            //           color: blue),
                            //     ),
                            //   ],
                            // ),
                            // const SizedBox(
                            //   height: 20,
                            // ),
                            ListView.builder(
                                physics: const ScrollPhysics(),
                                itemCount: notices.length,
                                shrinkWrap: true,
                                itemBuilder: ((context, i) {
                                  return InkWell(
                                      onTap: () {
                                        nav(
                                            context,
                                            PublicNoticeDetails(
                                                img:
                                                    "${notices[i]['alert_image_link']}",
                                                details:
                                                    "${notices[i]['message']}"));
                                      },
                                      child: popularWidget(
                                        "${notices[i]['title']}",
                                        "${notices[i]['message']}",
                                        "${notices[i]['time_of_notice']}",
                                        "${notices[i]['view_by']}",
                                        "${notices[i]['alert_image_link']}",
                                      ));
                                })),
                            const SizedBox(
                              height: 30,
                            ),
                            // InkWell(
                            //     onTap: () {},
                            //     child: popularWidget(
                            //         "Protect yourself",
                            //         "Please follow the CDD",
                            //         "14 min ago",
                            //         "120",
                            //         "https://images.pexels.com/photos/4744771/pexels-photo-4744771.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1")),
                            const SizedBox(
                              height: 30,
                            ),
                            // InkWell(
                            //     onTap: () {},
                            //     child: popularWidget(
                            //         "PSYCHOLOGISTS",
                            //         "Most Awaited - \nMEDICAL Launches",
                            //         "14 sec ago",
                            //         "120",
                            //         "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVteQvAa_awP21V_ClaLK89W1kdtSltiedJmRsjTcE-e9Pn9moDQ")),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
    );
  }
}
