import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Search/controller/search_controller.dart';
import 'package:snap_send_app/View/Search/searchDepartments/ui/search_results.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/model/widgets.dart';

class SearchScreen extends StatefulWidget {
  dynamic from;
  SearchScreen({this.from});
  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  bool isloading = false;
  var countries;

  var states;
  var cities;
  List<String> countriesList = [];
  List<String> countriesStates = [];

  List<String> countriesCities = [];
  var id = 231;
//getCountriesList
  getCountries() async {
    countriesList = [];
    isloading = true;
    setState(() {});
    var data = await Controller().getAllCountries();
    countries = data['data']['app_countries'];
    for (int i = 0; i < countries.length; i++) {
      if (countriesList.contains(countries[i]['name'])) {
      } else {
        countriesList.add(countries[i]['name']);
      }
      //  countriesList.add(countries[i]['name']);
    }

    await getStates(231);
    selectedCountry = "United States";
    countryIndex = 230;
    isloading = false;
    setState(() {});
  }

//getStatesList
  getStates(id) async {
    countriesStates = [];
    isloading = true;
    setState(() {});
    var data = {"country": id};
    var resp = await Controller().getAllStates(data);
    states = resp['data']['app_states'];
    for (int i = 0; i < states.length; i++) {
      if (countriesStates.contains(states[i]['name'])) {
      } else {
        countriesStates.add(states[i]['name']);
      }
    }
    isloading = false;
    setState(() {});
  }

//getCitiesList
  getCities(id) async {
    countriesCities = [];
    isloading = true;
    setState(() {});
    var data = {"state": id};
    var resp = await Controller().getAllCities(data);
    cities = resp['data']['app_cities'];
    // countriesCities.add("None");
    for (int i = 0; i < cities.length; i++) {
      if (countriesCities.contains(cities[i]['name'])) {
      } else {
        countriesCities.add(cities[i]['name']);
      }
    }
    isloading = false;
    setState(() {});
  }

  FocusNode? _focusNode;
  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    getCountries();
  }

  @override
  void dispose() {
    _focusNode?.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  String? selectedCountry;
  String? selectedState;
  String? selectedCity;
  String? initialCode;

  var getPreviousReports;
  List<String> customerReportList = [];
  String selectedReport = "Select";
  getDepartment() async {
    getPreviousReports = await SearchControllers().getDepartments(
      countries[countryIndex]['id'],
      states[stateIndex]['id'],
      cities[cityIndex]['id'],
    );

    for (int i = 0; i < getPreviousReports.length; i++) {
      customerReportList.add(
          "${getPreviousReports[i]['id']} ${getPreviousReports[i]['report_title']} ${getPreviousReports[i]['name']}");
    }
    log("the previous reports are ${customerReportList.toString()}");
  }

  List fetchedDepartments = [];
  fetchdepartments(country, state, city) async {
    // fetchedDepartments = [];
    // var data = {
    //   "country": country,
    //   "state": state,
    //   "City": city,
    //   "per_page": 10,
    //   "page": 1
    //   // not mandatory
    // };
    // isloading = true;
    // setState(() {});

    // var resp = await Controller().getDepartmentsByCountryState(data);
    // log("getDepartmentsByCountryState is ${resp.toString()}");
    // fetchedDepartments = resp['data']['list'];
    // isloading = false;
    // setState(() {});

    // nav(
    //     context,
    //     SearchResult(
    //         results: fetchedDepartments,
    //         selectedCountry: selectedCountry,
    //         selectedState: selectedState));
    // ignore: use_build_context_synchronously
    nav(
        context,
        SearchResult(
          results: fetchedDepartments,
          selectedCountry: selectedCountry,
          selectedState: selectedState,
          selectedCity: selectedCity,
          countryId: country.toString()=="1"?"":country.toString(),
          stateId: state.toString(),
          cityId: city.toString(),
          keyword: search.text.trim(),
        ));
  }

  // getPreviousReports = await SearchControllers().getDepartments(
  //   countries[countryIndex]['id'],
  //   states[stateIndex]['id'],
  //   cities[cityIndex]['id'],
  // );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              if (widget.from == "home") {
                pop(context);
              }
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: widget.from == "home" ? Colors.white : buttonColor,
            )),
        automaticallyImplyLeading: false,
        backgroundColor: buttonColor,
        title: Text('Search'.tr),
        actions: [
        //    IconButton(onPressed: (){
        //   navPushReplacement(context, MyBottomNavigationBar());


        // }, icon: const Icon(CupertinoIcons.home)),
          GestureDetector(
            onTap: () {
              selectedCity = null;
              selectedState = null;
              cityIndex = 0;
              stateIndex = 0;
              //   countries ="";
              _dropdownKey.currentState?.reset(); // Reset the selected value
              _dropdownKey2.currentState?.reset();
              countriesCities = [];
              search.text = "";
              //  countries = null;
              setState(() {});

              print("$selectedCity $selectedState $cityIndex $stateIndex");
            },
            child: Chip(
              backgroundColor: primaryColor,
              label: Row(
                children: [
                  Icon(CupertinoIcons.refresh),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Reset'.tr,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        progressIndicator: spinkit,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: SizedBox(
            height: height(context),
            width: width(context),
            child: Column(
              children: [
                dropdown(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? selectedValue;
  TextEditingController search = TextEditingController();

  Widget dropdown(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      elevation: 3.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 80),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(
                'Search Department'.tr,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 100,
                width: 100,
                child: Image.asset(
                  'assets/snapsend_logo.png',
                  // fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                'Search By Name'.tr,
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: textformfield1(
                  controller: search,
                  icon: CupertinoIcons.search,
                  hinttext: 'Search By Name'),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                'Country'.tr,
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
            dropdownbutton('Country', countriesList, selectedCountry),
            Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                'State, Province, Territory'.tr,
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
            dropdownbutton('State', countriesStates, selectedState,
                key: _dropdownKey),
            Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                'City, Town, Village'.tr,
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
            dropdownbutton('City', countriesCities, selectedCity,
                key: _dropdownKey2),
            const SizedBox(height: 30),
            longbuttons('Submit', () {
              try {
                fetchdepartments(
                    countries[countryIndex]['id'],
                    selectedState == "None" || selectedState == null
                        ? ""
                        : states[stateIndex]['id'],
                    selectedCity == "None" || selectedCity == null
                        ? ""
                        : cities[cityIndex]['id']);
              } catch (e) {
                fetchdepartments(
                    countries[countryIndex]['id'],
                    selectedState == "None" || selectedState == null
                        ? ""
                        : states[stateIndex]['id'],
                    selectedCity == "None" || selectedCity == null
                        ? ""
                        : cities[cityIndex]['id']);
              }
            }, buttonColor, width: 190),
            const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  final GlobalKey<FormFieldState<String>> _dropdownKey =
      GlobalKey<FormFieldState<String>>();
  final GlobalKey<FormFieldState<String>> _dropdownKey2 =
      GlobalKey<FormFieldState<String>>();

  var countryIndex = 0;
  var stateIndex = 0;
  var cityIndex = 0;
  dropdownbutton(txt, List<String> list, value, {Key? key}) {
    return DropdownButtonFormField2<String>(
      key: key,
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),

        // Add more decoration..
      ),
      hint: Text(
        value == null ? '$txt'.tr : '$value',
        style: const TextStyle(fontSize: 14, color: Colors.black),
      ),
      items: list
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(fontSize: 14, color: Colors.black),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select state.';
        }
        return null;
      },
      onChanged: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        if (txt == "Country") {
          selectedCountry = selectedValue;
          countryIndex = countriesList.indexOf(value.toString());
          log(countryIndex.toString());

          log(countries[countryIndex].toString());

          getStates(countries[countryIndex]['id']);
        } else if (txt == "State") {
          selectedState = selectedValue;
          stateIndex = countriesStates.indexOf(value.toString());

          log(countriesStates[stateIndex].toString());

          getCities(states[stateIndex]['id']);
        } else if (txt == "City") {
          // if (selectedCity == "None") {
          //   selectedCity = selectedValue;
          // } else {
          selectedCity = selectedValue;
          cityIndex = countriesCities.indexOf(selectedCity.toString());
          //   }
        }
        setState(() {});
        //Do something when selected item is changed.
      },
      onSaved: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        // var index = countriesList.indexOf(value.toString());
        // print(index.toString());
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
      dropdownSearchData: DropdownSearchData(
        searchController: textEditingController,
        searchInnerWidgetHeight: 50,
        searchInnerWidget: Container(
          height: 50,
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            expands: true,
            maxLines: null,
            controller: textEditingController,
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 8,
              ),
              hintText: 'Search'.tr,
              hintStyle: const TextStyle(fontSize: 12),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ),
        searchMatchFn: (item, searchValue) {
          return item.value
              .toString()
              .toLowerCase()
              .contains(searchValue.toLowerCase());
        },
      ),
      //This to clear the search value when you close the menu
      onMenuStateChange: (isOpen) {
        if (!isOpen) {
          textEditingController.clear();
        }
      },
    );
  }

  final TextEditingController textEditingController = TextEditingController();
}
