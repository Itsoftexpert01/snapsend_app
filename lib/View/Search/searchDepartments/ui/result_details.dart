import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Controller/functions.dart';
import 'package:snap_send_app/View/Search/controller/search_controller.dart';
import 'package:snap_send_app/View/Search/searchDepartments/ui/department_alerts.dart';
import 'package:snap_send_app/View/Search/searchDepartments/ui/department_notices.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../../../../model/widgets.dart';

// ignore: must_be_immutable
class ResultDetails extends StatefulWidget {
  dynamic details;
  ResultDetails({Key? key, this.details}) : super(key: key);

  @override
  State<ResultDetails> createState() => _ResultDetailsState();
}

class _ResultDetailsState extends State<ResultDetails> {
  var data;
  bool isloading = false;
  getUserDetails() async {
    isloading = true;
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState

    getUserDetails();
    super.initState();
  }

  String imgurl = '';
  @override
  Widget build(BuildContext context) {
    var data = widget.details;
    print("the details are ${widget.details}");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: buttonColor,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            pops(context);
          },
        ),
        centerTitle: true,
        title: Text(
          'Search details'.tr,
          style: TextStyle(fontSize: 24),
        ),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 0, right: 0, left: 0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  // color: Colors.red,
                  height: 250,
                  child: Stack(
                    children: [
                      ClipPath(
                          clipper: CurvedBottomClipper(),
                          child: Container(
                            width: width(context),
                            height: 200.0,
                            decoration: BoxDecoration(
                                color: buttonColor,
                                image: DecorationImage(
                                    scale: 5.3,
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                      "${widget.details['banner_picture_url'] ?? "https://snapsendreport.com//web/images/logo.png"}",
                                    )),
                                //  color: Colors.white,
                                borderRadius: BorderRadius.circular(5)),
                          )),
                      Positioned(
                        top: 110,
                        left: (width(context) - 105) / 2,
                        child: Center(
                          child: Card(
                            shape: RoundedRectangleBorder(
                                side: const BorderSide(
                                    color: Colors.black, width: 4.0),
                                borderRadius: BorderRadius.circular(20.0)),
                            elevation: 3.0,
                            child: Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      scale: 5.3,
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                        "${widget.details['officer_profile_picture_url'] ?? "https://snapsendreport.com//web/images/logo.png"}",
                                      )),
                                  // color: Colors.red,
                                  borderRadius: BorderRadius.circular(20)),
                              // child: Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: Row(
                              //     mainAxisAlignment:
                              //         MainAxisAlignment.spaceBetween,
                              //     crossAxisAlignment: CrossAxisAlignment.end,
                              //     children: [
                              //       Text(''),
                              //       GestureDetector(
                              //           onTap: () async {
                              //           //  handleImageUpload('profile');
                              //           },
                              //           child:
                              //               const Icon(CupertinoIcons.camera)),
                              //     ],
                              //   ),
                              // ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // ClipPath(
                //     clipper: CurvedBottomClipper(),
                //     child: Container(
                //       width: width(context),
                //       height: 200.0,
                //       decoration: BoxDecoration(
                //           image: DecorationImage(
                //             scale: 5.3,
                //             //fit: BoxFit.fill,
                //             image: NetworkImage(
                //               "${widget.details['banner_picture_url'] ?? "https://snapsendreport.com//web/images/logo.png"}",
                //             ),
                //             fit: BoxFit.fill,
                //           ),
                //           color: Colors.white,
                //           borderRadius: BorderRadius.circular(10)),
                //       child: Center(
                //         child: Card(
                //           elevation: 3.0,
                //           child: Container(
                //             height: 140,
                //             width: 145,
                //             decoration: imgurl.isEmpty
                //                 ? BoxDecoration(
                //                     image: DecorationImage(
                //                       scale: 5.3,
                //                       //fit: BoxFit.fill,
                //                       image: NetworkImage(
                //                         "${widget.details['officer_profile_picture_url'] ?? "https://snapsendreport.com//web/images/logo.png"}",
                //                       ),
                //                       fit: BoxFit.fill,
                //                     ),
                //                     color: Colors.white,
                //                     borderRadius: BorderRadius.circular(10))
                //                 : BoxDecoration(
                //                     image: DecorationImage(
                //                         scale: 5.3,
                //                         fit: BoxFit.fill,
                //                         image: NetworkImage(imgurl)),
                //                     color: Colors.white,
                //                     borderRadius: BorderRadius.circular(10)),
                //             child: const Padding(
                //               padding: EdgeInsets.all(8.0),
                //               child: Row(
                //                 mainAxisAlignment:
                //                     MainAxisAlignment.spaceBetween,
                //                 crossAxisAlignment: CrossAxisAlignment.end,
                //                 children: [],
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     )),
                //SizedBox(height: 150,),
                // SizedBox(height: MediaQuery.of(context).size.height / 15),

                // const SizedBox(
                //   height: 20,
                // ),
                text2(
                    widget.details == null
                        ? 'Lowes Pro'
                        : widget.details['name'].toString(),
                    25.5,
                    Colors.black),
                const SizedBox(
                  height: 5,
                ),
                Center(
                  child: SizedBox(
                    width: 300,
                    child: text1('${data['meta_data']['department_motto']}', 12,
                        Colors.black54),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),

                Padding(
                  padding: const EdgeInsets.only(left: 18.0),
                  child: Column(
                    children: [
                      gadgets(CupertinoIcons.phone, Colors.green,
                          data['mobile'].toString(), () {
                        Functions().launchUrls("tel:${data['mobile']}");
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      gadgets(CupertinoIcons.location, Colors.purple,
                          "${data['email']}", () {
                        Functions().launchUrls(
                            "mailto:${data['email']}?subject=Info&body=Hi Sir");
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      gadgets(
                          CupertinoIcons.bell, Colors.red, "Emergency Call 911",
                          () {
                        Functions().launchUrls("tel:911");
                      }),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                button('View Website', Colors.green, CupertinoIcons.globe,
                    () async {
                  if (data['website'] == null || data['website'] == 'null') {
                  } else {
                    nav(context, InAppWeb(data['website']));
                  }
                  //goToWeb();
//                   log("the website is ${data['website']}");
//                   var website = data['website'].toString();
//       if (!website.contains("https")) {
//     // ignore: prefer_interpolation_to_compose_strings
//     //website = "https://" + data['website'];
// }

//                   await Functions()
//                       .launchUrls(website);
                }),
                const SizedBox(
                  height: 10,
                ),
                button('Share', Colors.blue, CupertinoIcons.share, () {
                  SearchControllers()
                      .share(data['department_profile_url'].toString());
                }),
                const SizedBox(
                  height: 10,
                ),
                button('View Public Notice', Colors.purple,
                    CupertinoIcons.bell_circle, () {
                  nav(context,
                      DepartmentNotices(id: data['meta_data']['public_id']));
                }),
                const SizedBox(
                  height: 10,
                ),
                button('View Amber Alert', Colors.orangeAccent,
                    CupertinoIcons.bell_circle, () {
                  nav(
                      context,
                      DepartmentAmberAlerts(
                          id: data['meta_data']['public_id']));
                }),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  goToWeb() {
    WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse('https://flutter.dev'));
  }

  Widget button(txt, color, icon, void Function()? onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 50,
        width: 350,
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(5)),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                icon,
                color: Colors.white,
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                '$txt',
                style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget gadgets(icon, color, txt, void Function()? onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: color, borderRadius: BorderRadius.circular(50)),
            child: Icon(
              icon,
              size: 22,
              color: Colors.white,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          text1('$txt', 18, Colors.black54),
        ],
      ),
    );
  }
}

class CurvedBottomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..lineTo(0, size.height - 30) // Move to the top-left corner
      ..quadraticBezierTo(
        size.width / 2, size.height, // Control point and end point
        size.width, size.height - 30, // Move to the top-right corner
      )
      ..lineTo(size.width, 0) // Move to the bottom-right corner
      ..close(); // Close the path to form a shape

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
