import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/View/Notification/utils/widgets.dart';
import 'package:snap_send_app/View/Search/searchDepartments/ui/result_details.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

class SearchResult extends StatefulWidget {
  final dynamic results;
  final String? selectedCountry,
      selectedState,
      selectedCity,
      countryId,
      stateId,
      cityId,
      keyword;

  SearchResult(
      {required this.results,
      required this.selectedCountry,
      required this.selectedState,
      required this.selectedCity,
      required this.countryId,
      required this.stateId,
      required this.cityId,
      required this.keyword});

  @override
  _SearchResultState createState() => _SearchResultState();
}

class _SearchResultState extends State<SearchResult> {
  ScrollController _scrollController = ScrollController();
  List<dynamic> departmentList = [];
  bool isloading = false;
  int currentPage = 1;
  bool reachedEnd = false;

  @override
  void initState() {
    super.initState();
    _loadData();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      // User has reached the end of the list, load more data
      if (!reachedEnd) {
        _loadData();
      }
    }
  }

  int batchSize = 10;
  int currentIndex = 0;
  // var data = {
  //       "country": widget.selectedCountry,
  //       "state": widget.selectedState,
  //       "City": widget.selectedCity,
  //       // You may need to adjust this based on your API requirements
  //       "per_page": 10,
  //       "page": currentPage,
  //     };
  var resp;
  dynamic length;
  Future<void> _loadData() async {
    if (!isloading && !reachedEnd) {
      setState(() {
        if (departmentList.isEmpty) isloading = true;
      });

      var data = {
        "country": widget.countryId,
        "state": widget.stateId,
        "City": widget.cityId,
        'per_page': '1',
        'nameSearch':widget.keyword,
        // You may need to adjust this based on your API requirements
        "page": currentPage,
      };

      resp = await Controller().getDepartmentsByCountryState(data);
      length = resp['data']['list'].length;
      setState(() {
        isloading = false;
        if (currentIndex < resp['data']['list'].length) {
          int endIndex = currentIndex + batchSize;
          if (endIndex > resp['data']['list'].length) {
            endIndex = resp['data']['list'].length;
          }
          departmentList
              .addAll(resp['data']['list'].sublist(currentIndex, endIndex));
          currentIndex = endIndex;
        } else {
          reachedEnd = true; // No more data to load
        }
      });
    }
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));

    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _loadData();
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 100));
    _loadData();
    return true;
  }

  //Popular Widget
  Widget popularWidget(title, subtitle, time, like, image) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 3.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 4, bottom: 4),
                height: 100,
                width: 90,
                decoration: BoxDecoration(
                  borderRadius:
                      const BorderRadius.all(Radius.elliptical(12, 12)),
                  image: DecorationImage(
                    image: NetworkImage(image),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: 180,
                        child: Text(
                          title,
                          style: TextStyle(
                            overflow: TextOverflow.ellipsis,
                            fontSize: 16,
                            color: buttonColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 11,
                      ),
                      SizedBox(
                        width: 180,
                        child: Text(
                          subtitle,
                          style: const TextStyle(
                            color: Colors.black,
                            overflow: TextOverflow.ellipsis,
                            fontWeight: FontWeight.w800,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 11,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            time,
                            style: const TextStyle(fontSize: 14),
                          ),
                          const Padding(padding: EdgeInsets.only(left: 12)),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: buttonColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            pops(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios_new,
          ),
          color: Colors.white,
        ),
        title: text2('Search', 18, Colors.white),
        actions:  [IconButton(onPressed: (){
          navPushReplacement(context, MyBottomNavigationBar());


        }, icon: const Icon(CupertinoIcons.home))],
      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              header: const WaterDropHeader(),
              footer: CustomFooter(
                builder: (context, mode) {
                  Widget body;
                  if (mode == LoadStatus.idle && !reachedEnd) {
                    body = Text("Please wait departments loading".tr);
                  } else if (mode == LoadStatus.loading) {
                    body = spinkit;
                  } else if (mode == LoadStatus.failed) {
                    body = Text("loading Failed! retry!".tr);
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("loading more...".tr);
                  } else {
                    body = Text("No more departments".tr);
                  }
                  return SizedBox(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: SafeArea(
                child: Container(
                  margin: const EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 0, left: 25, right: 25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "${('Search Result For').tr} ${widget.selectedCountry ?? "${widget.keyword}"} ${widget.selectedState ?? ""} ${widget.keyword ?? ""}",
                                style: const TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            "$length ${('Result Found').tr}",
                            style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          departmentList.isEmpty
                              ? Center(
                                  child: Text(
                                    'No Result Found'.tr,
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black,
                                    ),
                                  ),
                                )
                              : ListView.builder(
                                  controller:
                                      _scrollController, // Attach the ScrollController here
                                  physics: const ScrollPhysics(),
                                  itemCount: departmentList.length,
                                  shrinkWrap: true,
                                  itemBuilder: ((context, i) {
                                    return InkWell(
                                      onTap: () {
                                        nav(
                                          context,
                                          ResultDetails(
                                              details: departmentList[i]),
                                        );
                                      },
                                      child: popularWidget(
                                        "${departmentList[i]['name']}",
                                        "${departmentList[i]['meta_data'] == null ? "SnapSend Department" : departmentList[i]['meta_data']['department_motto']}",
                                        "${departmentList[i]['more_data'] == null ? "${DateTime.now().toString().split(" ")[0]}" : departmentList[i]['more_data']}",
                                        "${departmentList[i]['more_data']}",
                                        "${departmentList[i]['officer_profile_picture_url'] == null ? "https://snapsendreport.com//web/images/logo.png" : departmentList[i]['officer_profile_picture_url']}",
                                      ),
                                    );
                                  }),
                                ),
                          const SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Future<void> _refresh() async {
    await Future.delayed(const Duration(seconds: 0, milliseconds: 100));
  }
}












// import 'dart:developer';

// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:snap_send_app/Controller/controller.dart';
// import 'package:snap_send_app/Controller/shared_pref.dart';
// import 'package:snap_send_app/Globals/config.dart';
// import 'package:snap_send_app/Globals/loader.dart';
// import 'package:snap_send_app/View/Search/searchDepartments/ui/result_details.dart';
// import 'package:snap_send_app/model/color.dart';
// import 'package:snap_send_app/model/nav.dart';
// import 'package:snap_send_app/model/widgets.dart';

// // ignore: use_key_in_widget_constructors
// class SearchResult extends StatefulWidget {
//   dynamic results, selectedCountry, selectedState;
//   SearchResult({this.results, this.selectedCountry, this.selectedState});
//   @override
//   // ignore: library_private_types_in_public_api
//   _SearchResultState createState() => _SearchResultState();
// }

// class _SearchResultState extends State<SearchResult> {
//   dynamic endorsements;
//   List departmentList = [];

//   bool isloading = false;

//   var user;
//   // ignore: prefer_typing_uninitialized_variables
//   var token;
//   var formattedDate;
//   getuserDetails() async {
//     isloading = true;
//     setState(() {});
//     user = await Storage.getLogin();
//     log(user.toString());
//    // await getNotices();
//     final DateTime currentDate = DateTime.now();
//     final DateFormat formatter = DateFormat('EEEE, d MMMM y', 'en_US');

//     formattedDate = formatter.format(currentDate);
//     isloading = false;
//     setState(() {});
//   }

//   @override
//   void initState() {
//     super.initState();
//     getuserDetails();
//   }

//   getNotices() async {
//     isloading = true;
//     setState(() {});

//     var response = await Controller().webEntity({"type": "endorsement"});

//     if (response['succ'] == false) {
//       showToast(response['errors']);
//     } else {
//       endorsements = response['data']['app_entity'];
//     }

//     isloading = false;
//     setState(() {});
//   }

//   //Popular Widget
//   Widget popularWidget(title, subtitle, time, like, image) => Container(
//         child: Card(
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(10),
//             //set border radius more than 50% of height and width to make circle
//           ),
//           elevation: 3.0,
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Row(
//               children: <Widget>[
//                 Container(
//                     margin: const EdgeInsets.only(top: 4, bottom: 4),
//                     height: 100,
//                     width: 90,
//                     decoration: BoxDecoration(
//                         borderRadius:
//                             const BorderRadius.all(Radius.elliptical(12, 12)),
//                         image: DecorationImage(
//                             image: NetworkImage(image), fit: BoxFit.fill))),
//                 Padding(
//                   padding: const EdgeInsets.only(left: 25.0),
//                   child: SingleChildScrollView(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: <Widget>[
//                         SizedBox(
//                           width: 180,
//                           child: Text(
//                             title,
//                             style: TextStyle(
//                               overflow: TextOverflow.ellipsis,
//                               fontSize: 16,
//                               color: buttonColor,
//                               fontWeight: FontWeight.w800,
//                             ),
//                           ),
//                         ),
//                         const SizedBox(
//                           height: 11,
//                         ),
//                         SizedBox(
//                           width: 180,
//                           child: Text(
//                             subtitle,
//                             style: const TextStyle(
//                                 color: Colors.black,
//                                 overflow: TextOverflow.ellipsis,
//                                 fontWeight: FontWeight.w800,
//                                 fontSize: 14),
//                           ),
//                         ),
//                         const SizedBox(
//                           height: 11,
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             Text(
//                               time,
//                               style: const TextStyle(fontSize: 14),
//                             ),
//                             const Padding(padding: EdgeInsets.only(left: 12)),
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//                 )
//               ],
//             ),
//           ),
//         ),
//       );

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: white,
//       appBar: AppBar(
//         elevation: 0.0,
//         backgroundColor: buttonColor,
//         centerTitle: true,
//         leading: IconButton(
//           onPressed: () {
//             pops(context);
//           },
//           icon: const Icon(
//             Icons.arrow_back_ios_new,
//           ),
//           color: Colors.white,
//         ),
//         title: text2('Search', 18, Colors.white),
//         actions: const [],
//       ),
//       body: isloading
//           ? Center(
//               child: spinkit,
//             )
//           : SafeArea(
//               child: Container(
//                 margin: const EdgeInsets.all(10),
//                 child: SingleChildScrollView(
//                   child: Padding(
//                     padding: const EdgeInsets.only(top: 0, left: 25, right: 25),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.stretch,
//                       children: <Widget>[
//                         //  getImage(),
//                         const SizedBox(
//                           height: 0,
//                         ),

//                         const SizedBox(
//                           height: 20,
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             Text(
//                               "Search Result For ${widget.selectedCountry} - ${widget.selectedState}",
//                               style: const TextStyle(
//                                   fontSize: 16, fontWeight: FontWeight.w700),
//                             ),
//                           ],
//                         ),
//                         const SizedBox(
//                           height: 5,
//                         ),
//                         Text(
//                           "${widget.results.length} Result Found",
//                           style: const TextStyle(
//                               fontSize: 16,
//                               fontWeight: FontWeight.w700,
//                               color: Colors.grey),
//                         ),
//                         const SizedBox(
//                           height: 20,
//                         ),

//                         widget.results.length == 0
//                             ? const Center(
//                                 child: Text(
//                                 'No Result Found',
//                                 style: TextStyle(
//                                     fontSize: 16,
//                                     fontWeight: FontWeight.w700,
//                                     color: Colors.black),
//                               ))
//                             : ListView.builder(
//                                 physics: const ScrollPhysics(),
//                                 itemCount: widget.results.length,
//                                 shrinkWrap: true,
//                                 itemBuilder: ((context, i) {
//                                   return InkWell(
//                                       onTap: () {
//                                         nav(
//                                             context,
//                                             ResultDetails(
//                                                 details: widget.results[i]));
//                                       },
//                                       child: popularWidget(
//                                         "${widget.results[i]['name']}",
//                                         "${widget.results[i]['meta_data'] == null ? "SnapSend Department" : widget.results[i]['meta_data']['department_motto']}",
//                                         "${widget.results[i]['more_data'] == null ? "${DateTime.now().toString().split(" ")[0]}" : widget.results[i]['more_data']}",
//                                         "${widget.results[i]['more_data']}",
//                                         "${widget.results[i]['officer_profile_picture_url'] == null ? "https://snapsendreport.com//web/images/logo.png" : widget.results[i]['officer_profile_picture_url']}",
//                                       ));
//                                 })),

//                         const SizedBox(
//                           height: 30,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//     );
//   }
// }
