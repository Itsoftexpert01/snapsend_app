import 'dart:developer';

import 'package:share_plus/share_plus.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/loader.dart';

class SearchControllers {
  getDepartments(country, state, city) async {
    var data = {
      "country": "$country",
      "state": "$state",
      "City": "$city" // not mandatory
    };

    var response = await Controller().getDepartment(data);
    log("the previous reports are ${response.toString()}");

    if (response['succ'] == false) {
      showToast(response['errors']);

      return null;
    } else {
      return response['data'];
    }
  }

  getCustomerReport(data) async {
    var response = await Controller().getCustomerReport(data);
    // log("the previous reports are ${response.toString()}");

    if (response['succ'] == 0) {
      showToast("No Reports Found");

      return null;
    } else {
      return response['data']['customer_report'];
    }
  }

  getReportDetails(id) async {
    var response = await Controller().getListDetailsInApp(id);
    // log("the previous reports are ${response.toString()}");

    if (response['succ'] == 0) {
      showToast("No Reports Found");

      return null;
    } else {
      return response['data'];
    }
  }

  getCustomerReport2(data) async {
    var response = await Controller().getCustomerReport(data);
    // log("the previous reports are ${response.toString()}");

    if (response['succ'] == 0) {
      showToast("No Reports Found");

      return null;
    } else {
      return response['data'];
    }
  }

  share(
    name,
  ) async {
    Share.share('Check this Department Profile $name',
        subject: 'Share Police Department');
  }
}
