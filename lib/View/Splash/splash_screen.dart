import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/View/Auth/login_screen.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isloading = false;
  // ignore: prefer_typing_uninitialized_variables
  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  int seconds = 4;
  getuserDetails() async {
    user = await Storage.getLogin();
    //  token = await Storage.getToken();
    var isremember = await Storage.get('isremember');
    await Future.delayed(Duration(seconds: seconds), () {
      if (user == false) {
        nav(context, LoginScreen());
      } else {
        if (isremember == 'true') {
          navPushReplacement(context, MyBottomNavigationBar());
        } else {
          nav(context, LoginScreen());
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getuserDetails();
  }

  @override
  Widget build(BuildContext context) {
    // print("my height is ${height(context)} and with is ${width(context)}");
    return Scaffold(
      backgroundColor: primaryColor,
      body: ModalProgressHUD(
        inAsyncCall: isloading,
        child: Container(
          height: height(context),
          width: width(context),
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                    'assets/splash.gif',
                  ),
                  fit: BoxFit.cover)),
          child: Align(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: () {
                seconds = 0;
                setState(() {});
                getuserDetails();
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 38.0, right: 15),
                child: Text(
                  'Skip'.tr,
                  style: const TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
