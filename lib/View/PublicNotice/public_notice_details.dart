import 'package:flutter/material.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

// ignore: must_be_immutable
class PublicNoticeDetails extends StatefulWidget {
  dynamic img, details;
  PublicNoticeDetails({Key? key, this.img, this.details}) : super(key: key);

  @override
  State<PublicNoticeDetails> createState() => _PublicNoticeDetailsState();
}

class _PublicNoticeDetailsState extends State<PublicNoticeDetails> {
  TextEditingController? email = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: buttonColor,
          elevation: 0.0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              pops(context);
            },
          ),
          centerTitle: true,
          title: const Text(
            'Details',
            style: TextStyle(fontSize: 24),
          ),
        ),
        body: Center(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // const Padding(
              //   padding: EdgeInsets.only(top: 40.0),
              //   child: Text(
              //     'Login Account',
              //     textAlign: TextAlign.center,
              //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              //   ),
              // ),
              widget.img == null
                  ? Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: SizedBox(
                        height: 120,
                        width: 120,
                        child: Image.network(
                          'assets/snapsend_logo.png',
                          // fit: BoxFit.fill,
                        ),
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: SizedBox(
                        height: 300,
                        width: 300,
                        child: Image.network(
                          '${widget.img}',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
              // const Padding(
              //   padding: EdgeInsets.only(top: 20.0),
              //   child: Text(
              //     'SnapSend About us',
              //     textAlign: TextAlign.center,
              //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              //   ),
              // ),
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 20, right: 20),
                child: Text(
                  '${widget.details}',
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 14, letterSpacing: 0.5),
                ),
              ),

              // Padding(
              //   padding: const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
              //   child: textformfield1(
              //       controller: email,
              //       icon: Icons.email,
              //       hinttext: 'Enter email address'),
              // ),

              // Padding(
              //     padding: const EdgeInsets.only(top: 20.0, left: 60, right: 60.0),
              //     child: longbuttons('Send instructions', () {}, buttonColor,
              //         width: 280)),

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Padding(
              //       padding: const EdgeInsets.only(top: 18.0),
              //       child: text3("Remember the password? ", 12.5, Colors.grey),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(top: 18.0),
              //       child: text3("Log in here", 12.5, buttonColor),
              //     ),
              //   ],
              // ),

              // longbuttons('Login', () {}, primaryColor),
              // longbuttons('Register', () {}, Colors.black),
            ],
          ),
        ));
  }
}
