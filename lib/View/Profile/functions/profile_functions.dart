import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';

import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/loader.dart';

class ProfileFunctions {
  uploadProfilePic(base64, type) async {
    final extension = base64.path.split('.').last; // Get the file extension
    log("Image extension: $extension");

    //  final Uint8List imageBytes = await pickedFile.readAsBytes();
    var imageBytes = await File(base64.path).readAsBytes();
    log("them pickedFile is $imageBytes");

    // Convert bytes to base64
    String base64Image = base64Encode(imageBytes);
    var data;
    print("my type is $type");
    // String base64Images = "data:image/png;base64," + base64Encode(imageBytes);
    if (type == "profile") {
      data = {
        "fileData": "data:image/jpeg;base64,$base64Image",
        "type": "profile_pic"
      };
    } else {
      data = {
        "fileData": "data:image/jpeg;base64,$base64Image",
        "type": "banner"
      };
    }

    var response = await Controller().uploadProfilePic(data);

    if (response['succ'] == false) {
      showToast(response['errors']);

      return null;
    } else {
      return response['data'];
    }
  }

  Future<File?> cropImage(File imageFile) async {
    CroppedFile? croppedFile = await ImageCropper.platform.cropImage(
      sourcePath: imageFile.path,
      aspectRatio: const CropAspectRatio(
          ratioX: 1, ratioY: 1), // You can adjust the aspect ratio as needed
      compressQuality: 100, // You can adjust the image quality
      maxHeight: 800, // You can adjust the maximum height
      maxWidth: 800, // You can adjust the maximum width
    );

    return File(croppedFile!.path);
  }
}
