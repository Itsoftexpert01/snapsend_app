import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/widgets.dart';

Widget card(
    {dynamic text,
    IconData? icon,
    dynamic onTap,
    double h = 130.0,
    double w = 130.0}) {
  return SizedBox(
    height: h,
    width: w,
    child: BouncingWidget(
      duration: Duration(milliseconds: 100),
      scaleFactor: 1.5,
      onPressed: onTap,
      child: Card(
        elevation: 6,
        shape: OutlineInputBorder(
            borderRadius: BorderRadius.circular(
              10,
            ),
            borderSide: const BorderSide(color: Colors.transparent)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 35,
              color: buttonColor,
            ),
            const SizedBox(
              height: 9,
            ),
            text1(
              text,
              15,
              Colors.black,
            ),
          ],
        ),
      ),
    ),
  );
}
