import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

// ignore: must_be_immutable
class SetUpScreen extends StatefulWidget {
  dynamic isfrom;
  SetUpScreen({super.key, this.isfrom});
  @override
  State<SetUpScreen> createState() => _SetUpScreenState();
}

class _SetUpScreenState extends State<SetUpScreen> {
  @override
  void initState() {
    super.initState();
    getCountries();
    getDepartments(context);
  }

  final GlobalKey<FormFieldState<String>> _dropdownKey =
      GlobalKey<FormFieldState<String>>();
  final GlobalKey<FormFieldState<String>> _dropdownKey2 =
      GlobalKey<FormFieldState<String>>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: buttonColor,
        leading: IconButton(
            onPressed: () {
              pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios)),
        title: Text('Your Top Picks'.tr),
        actions: [
          GestureDetector(
            onTap: () {
              selectedCity = null;
              selectedState = null;
              cityIndex = 0;
              stateIndex = 0;
              _dropdownKey.currentState?.reset(); // Reset the selected value
              _dropdownKey2.currentState?.reset();
              countriesCities = [];
              setState(() {});
            },
            child: Chip(
              backgroundColor: primaryColor,
              label: Row(
                children: [
                  const Icon(CupertinoIcons.refresh),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Reset'.tr,
                    style: const TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : ModalProgressHUD(
              inAsyncCall: isloading,
              progressIndicator: spinkit,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: SizedBox(
                  height: height(context),
                  width: width(context),
                  child: ListView(
                    children: [
                      dropdown(context),
                    ],
                  ),
                ),
              ),
            ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.only(top: 0.0, bottom: 28.0),
          child: Center(
              child: longbuttons('Save Changes', () async {
            updateDepartment(context);
          }, buttonColor, width: 250)),
        ),
      ],
    );
  }

  final List<String> genderItems = [
    'Male',
    'Female',
  ];

  String? selectedValue;

  final _formKey = GlobalKey<FormState>();

  TextEditingController setUpdetails = TextEditingController();

  Widget dropdown(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      elevation: 3.0,
      child: Form(
        key: _formKey,
        child: ListView(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 100,
                width: 100,
                child: Image.asset(
                  'assets/snapsend_logo.png',
                  // fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Country'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('Country', countriesList, selectedCountry),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'State, Province, Territory'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('State', countriesStates, selectedState),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'City, Town, Village'.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  dropdownbutton('City', countriesCities, selectedCity),
                  const SizedBox(height: 10),
                  ListView.builder(
                      physics: const ScrollPhysics(),
                      itemCount: fetchedDepartments.length,
                      shrinkWrap: true,
                      itemBuilder: (context, i) =>
                          department(fetchedDepartments[i]['name'], () {
                            if (departmentList
                                .contains(fetchedDepartments[i]['publicId'])) {
                              departmentList
                                  .remove(fetchedDepartments[i]['publicId']);
                            } else {
                              departmentList
                                  .add(fetchedDepartments[i]['publicId']);
                            }
                            setState(() {});
                          }, 'fetch', fetchedDepartments[i]['publicId'])),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      'Selected Departments'.tr,
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  //fetchedDepartments

                  departments == null
                      ? const SizedBox()
                      : ListView.builder(
                          physics: const ScrollPhysics(),
                          itemCount:
                              departments['user_department_in_array'].length,
                          shrinkWrap: true,
                          itemBuilder: (context, i) => department(
                                  departments['user_department_in_array'][i],
                                  () {
                                if (departmentList.contains(
                                    departments['user_department_id'][i])) {
                                  departmentList.remove(
                                      departments['user_department_id'][i]);
                                } else {
                                  departmentList.add(
                                      departments['user_department_id'][i]);
                                }

                                setState(() {});
                              }, 'save', departments['user_department_id'][i])),

                  const SizedBox(height: 30),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isloading = false;

  dynamic departments;
  List departmentList = [];
  getDepartments(context) async {
    isloading = true;
    setState(() {});
    var data = {"department_type": 1};

    var response = await Controller().getDepartment(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      departments = response['data'];

      if (departments != null && departments['user_department_id'].isNotEmpty) {
        for (int i = 0; i < departments['user_department_id'].length; i++) {
          departmentList.add(departments['user_department_id'][i]);
        }
      }
    }
    await getStates(231);
    selectedCountry = "United States";
    countryIndex = 230;

    isloading = false;
    setState(() {});
  }

  updateDepartment(context) async {
    isloading = true;
    setState(() {});
    var data = {
      "department_type": 1,
      "department": departmentList
      // departments['user_department_id']
    };

    var response = await Controller().updateDepartment(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      showToast(response['msg']);
      fetchedDepartments = [];
      if (widget.isfrom == "TIP") {
        pop(context);
        pop(context);
      } else {
        pop(context);
      }
    }

    isloading = false;
    setState(() {});
  }

  department(txt, void Function()? onPressed, type, id) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: Colors.grey)),
        height: 40,
        width: width(context),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          SizedBox(
            width: width(context) - 200,
            child: Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: Text(
                '$txt',
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          const Spacer(),
          type == "fetch"
              ? IconButton(
                  onPressed: onPressed,
                  icon: Icon(departmentList.contains(id)
                      ? Icons.check_box
                      : Icons.check_box_outline_blank))
              : IconButton(
                  onPressed: onPressed,
                  icon: Icon(departmentList.contains(id)
                      ? Icons.check_box
                      : Icons.check_box_outline_blank))
        ]),
      ),
    );
  }

  var countryIndex = 0;
  var stateIndex = 0;
  var cityIndex = 0;
  String? selectedCountry;
  String? selectedState;
  String? selectedCity;
  String? initialCode;
  dropdownbutton(txt, List<String> list, value, {Key? key}) {
    print("value is $value");
    return DropdownButtonFormField2<String>(
      key: key,
      isExpanded: true,
      decoration: InputDecoration(
        // Add Horizontal padding using menuItemStyleData.padding so it matches
        // the menu padding when button's width is not specified.
        contentPadding: const EdgeInsets.symmetric(vertical: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),

        // Add more decoration..
      ),
      hint: Text(
        value == null ? '$txt'.tr : '$value',
        style: const TextStyle(fontSize: 14, color: Colors.black),
      ),
      items: list
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(fontSize: 14, color: Colors.black),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Please select gender.';
        }
        return null;
      },
      onChanged: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        if (txt == "Country") {
          selectedCountry = selectedValue;
          countryIndex = countriesList.indexOf(value.toString());

          log(countries[countryIndex].toString());
          getStates(countries[countryIndex]['id']);
        } else if (txt == "State") {
          selectedState = selectedValue;
          stateIndex = countriesStates.indexOf(value.toString());

          log(countriesStates[stateIndex].toString());
          fetchdepartments(
              countries[countryIndex]['id'], states[stateIndex]['id'], '');
          getCities(states[stateIndex]['id']);
        } else if (txt == "City") {
          selectedCity = selectedValue;
          cityIndex = countriesCities.indexOf(selectedCity.toString());
          fetchdepartments(countries[countryIndex]['id'],
              states[stateIndex]['id'], cities[cityIndex]['id']);
        }

        //Do something when selected item is changed.
      },
      onSaved: (value) {
        selectedValue = value.toString();
        log(selectedValue.toString());

        // var index = countriesList.indexOf(value.toString());
        // print(index.toString());
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        iconSize: 24,
      ),
      dropdownStyleData: DropdownStyleData(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
      dropdownSearchData: DropdownSearchData(
        searchController: textEditingController,
        searchInnerWidgetHeight: 50,
        searchInnerWidget: Container(
          height: 50,
          padding: const EdgeInsets.only(
            top: 8,
            bottom: 4,
            right: 8,
            left: 8,
          ),
          child: TextFormField(
            expands: true,
            maxLines: null,
            controller: textEditingController,
            decoration: InputDecoration(
              isDense: true,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 8,
              ),
              hintText: 'Search...',
              hintStyle: const TextStyle(fontSize: 12),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ),
        searchMatchFn: (item, searchValue) {
          return item.value
              .toString()
              .toLowerCase()
              .contains(searchValue.toLowerCase());
        },
      ),
      //This to clear the search value when you close the menu
      onMenuStateChange: (isOpen) {
        if (!isOpen) {
          textEditingController.clear();
        }
      },
    );
  }

  List<String> removeDuplicates(List<String> items) {
    Set<String> seen = Set<String>();
    List<String> uniqueItems = [];

    for (String item in items) {
      if (!seen.contains(item)) {
        uniqueItems.add(item);
        seen.add(item);
      }
    }

    return uniqueItems;
  }

  String toTitleCase(String text) {
    if (text == null || text.isEmpty) {
      return text;
    }

    return text[0].toUpperCase() + text.substring(1);
  }

  final TextEditingController textEditingController = TextEditingController();
  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  List fetchedDepartments = [];
  fetchdepartments(country, state, city) async {
    fetchedDepartments = [];
    var data = {
      "country": country,
      "state": state,
      "City": city, // not mandatory
      "per_page": "",
      "page": "page"
    };
    isloading = true;
    setState(() {});
    var resp = await Controller().getDepartmentsByCountryState(data);

    //var resp = await Controller().getDepartmentsByCountryState(data);
    log(resp.toString());
    fetchedDepartments = resp['data']['list'];
    isloading = false;
    setState(() {});
  }

  var countries;

  var states;
  var cities;
  List<String> countriesList = [];
  List<String> countriesStates = [];

  List<String> countriesCities = [];
//getCountriesList
  getCountries() async {
    var data = await Controller().getAllCountries();
    countries = data['data']['app_countries'];
    for (int i = 0; i < countries.length; i++) {
      if (countriesList.contains(countries[i]['name'])) {
      } else {
        countriesList.add(countries[i]['name']);
      }
    }
    await getStates(231);
    selectedCountry = "United States";
    countryIndex = 230;
  }

//getStatesList
  getStates(id) async {
    countriesStates = [];
    isloading = true;
    setState(() {});
    var data = {"country": id};
    var resp = await Controller().getAllStates(data);
    states = resp['data']['app_states'];
    for (int i = 0; i < states.length; i++) {
      if (countriesStates.contains(states[i]['name'])) {
      } else {
        countriesStates.add(states[i]['name']);
      }
    }
    isloading = false;
    setState(() {});
  }

//getCitiesList
  getCities(id) async {
    countriesCities = [];
    isloading = true;
    setState(() {});
    var data = {"state": id};
    var resp = await Controller().getAllCities(data);
    cities = resp['data']['app_cities'];
    for (int i = 0; i < cities.length; i++) {
      if (countriesCities.contains(cities[i]['name'])) {
      } else {
        countriesCities.add(cities[i]['name']);
      }
    }
    isloading = false;
    setState(() {});
  }
}
