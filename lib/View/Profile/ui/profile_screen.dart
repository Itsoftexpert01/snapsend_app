// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Auth/login_screen.dart';
import 'package:snap_send_app/View/Profile/functions/profile_functions.dart';
import 'package:snap_send_app/View/Profile/ui/edit_profile.dart';
import 'package:snap_send_app/View/Profile/ui/set_up.dart';
import 'package:snap_send_app/View/Profile/ui/travel_destinations.dart';

import 'package:snap_send_app/View/Profile/utils/profile_utils.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/search_tip.dart';
import 'package:snap_send_app/View/Settings/ui/settings_screen.dart';
import 'package:snap_send_app/View/SexOffenderSearch/ui/sex_offender_search.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

import '../../../Globals/config.dart';
import '../../../model/widgets.dart';

// ignore: must_be_immutable
class MyProfile extends StatefulWidget {
  dynamic from;
  MyProfile({this.from, Key? key}) : super(key: key);

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  var data;
  bool isloading = false;
  var user;
  var token;
  getUserDetails() async {
    isloading = true;
    setState(() {});
    await getCustomer();
    user = await Storage.getLogin();
    token = await Storage.getToken();
    log(user.toString());
    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState

    getUserDetails();
    super.initState();
  }

  String imgurl = '';
  String bannerurl = '';
  @override
  Widget build(BuildContext context) {
    // print("http://54.241.188.7/web/profilepic/$imgurl");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: buttonColor,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: widget.from != "Setup" ? buttonColor : Colors.white,
          ),
          onPressed: () {
            widget.from != "Setup" ? print("Lol") : pops(context);
          },
        ),
        centerTitle: true,
        title: Text(
          'My Profile'.tr,
          style: TextStyle(fontSize: 24),
        ),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home))
        ],
      ),
      body: isloading
          ? Center(
              child: spinkit,
            )
          : SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 0, right: 0, left: 0),
                child: SingleChildScrollView(
                  physics: const ScrollPhysics(),
                  child: Column(
                    children: [
                      SizedBox(
                        // color: Colors.red,
                        height: 250,
                        child: Stack(
                          children: [
                            ClipPath(
                                clipper: CurvedBottomClipper(),
                                child: Container(
                                  width: width(context),
                                  height: 200.0,
                                  decoration: BoxDecoration(
                                      color: buttonColor,
                                      image: DecorationImage(
                                          scale: 5.3,
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              // "${Config.imageURL}/web/user/banner/$
                                              "$bannerurl")),
                                      //  color: Colors.white,
                                      borderRadius: BorderRadius.circular(0)),
                                )),
                            Positioned(
                                top: 0,
                                right: 0,
                                child: GestureDetector(
                                  onTap: () {
                                    handleImageUpload('cover');
                                    // Add your edit functionality here
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: const Color.fromARGB(
                                              255, 223, 111, 111)
                                          .withOpacity(0.5),
                                    ),
                                    padding: const EdgeInsets.all(4),
                                    child: const Icon(
                                      Icons.edit,
                                      color: Colors.white,
                                      size: 30,
                                    ),
                                  ),
                                )),
                            Positioned(
                              top: 110,
                              left: (width(context) - 120) / 2,
                              child: Center(
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      side: const BorderSide(
                                          color: Colors.black, width: 4.0),
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                  elevation: 3.0,
                                  child: Container(
                                    height: 110,
                                    width: 120,
                                    decoration: imgurl.isEmpty
                                        ? BoxDecoration(
                                            image: const DecorationImage(
                                                scale: 5.3,
                                                //fit: BoxFit.fill,
                                                image: AssetImage(
                                                    "assets/snapsend_logo.png")),
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(20))
                                        : BoxDecoration(
                                            image: DecorationImage(
                                              scale: 5.3,
                                              fit: BoxFit.cover,
                                              image: NetworkImage(imgurl
                                                  // "${Config.imageURL}/web/profilepic/$imgurl",
                                                  ),
                                            ),
                                            // color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Text(''),
                                          GestureDetector(
                                            onTap: () async {
                                              handleImageUpload('profile');
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: const Color.fromARGB(
                                                        255, 223, 111, 111)
                                                    .withOpacity(0.5),
                                              ),
                                              padding: const EdgeInsets.all(4),
                                              child: const Icon(
                                                Icons.edit,
                                                color: Colors.white,
                                                size: 30,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //SizedBox(height: 150,),
                      // SizedBox(height: MediaQuery.of(context).size.height / 15),

                      const SizedBox(
                        height: 0,
                      ),
                      text2(
                          userdetails == null
                              ? 'Lowes Pro'
                              : userdetails['first_name'].toString(),
                          25.5,
                          Colors.black),
                      const SizedBox(
                        height: 5,
                      ),
                      Center(
                        child: SizedBox(
                          width: 180,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Icon(
                                Icons.phone,
                                size: 20,
                                color: Colors.black54,
                              ),
                              text1('${userdetails['mobile']}', 12,
                                  Colors.black54),
                              GestureDetector(
                                onTap: () async {
                                  // await Storage.setToken('null');
                                  // await Storage.setLogin('null');
                                  // await Storage.logout();
                                  await Storage.set('isremember', 'false');

                                  navandClear(context, LoginScreen());
                                },
                                child: Container(
                                  // width: 55,
                                  height: 17,
                                  decoration: BoxDecoration(
                                      color: primaryColor,
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: Center(
                                        child: Text(
                                      'Logout'.tr,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 10,
                                      ),
                                    )),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        //  color: Colors.red,
                        width: 290,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              card(
                                  //  h: height(context) / 6.2,
                                  text: 'Home\nLocation',
                                  icon: CupertinoIcons.hand_draw,
                                  // ignore: duplicate_ignore
                                  onTap: () async {
                                    await Future.delayed(
                                        const Duration(milliseconds: 200));
                                    // ignore: use_build_context_synchronously
                                    nav(context, SetUpScreen());
                                  }),
                              card(
                                  // h: height(context) / 6.2,
                                  text: 'Travel\nDestinations',
                                  icon: CupertinoIcons.location,
                                  onTap: () async {
                                    await Future.delayed(
                                        const Duration(milliseconds: 200));
                                    // ignore: use_build_context_synchronously
                                    nav(context, TravelDestinationScreen());
                                  }),
                            ],
                          ),
                        ),
                      ),

                      SizedBox(
                        //  color: Colors.red,
                        width: 290,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              card(
                                  //  h: height(context) / 6.2,
                                  text: 'Edit Profile',
                                  icon: CupertinoIcons.profile_circled,
                                  onTap: () async {
                                    await Future.delayed(
                                        const Duration(milliseconds: 200));
                                    nav(context, EditProfileScreen());
                                  }),
                              card(
                                  // h: height(context) / 6.2,
                                  text: 'Search Tips',
                                  icon: CupertinoIcons.search,
                                  onTap: () async {
                                    await Future.delayed(
                                        const Duration(milliseconds: 200));

                                    nav(context, SearchTip());
                                  }),
                            ],
                          ),
                        ),
                      ),

                      Container(
                        width: 290,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              card(
                                  text: 'Search Sex Offender',
                                  icon: CupertinoIcons.search_circle,
                                  onTap: () async {
                                    await Future.delayed(
                                        const Duration(milliseconds: 200));
                                    nav(context, SexOffenderScreen());
                                    // Navigator.push(
                                    //     context,
                                    //     MaterialPageRoute(
                                    //         builder: (context) =>
                                    //             const ChangePassword()));
                                  }),
                              card(
                                  text: 'Settings',
                                  icon: CupertinoIcons.settings,
                                  onTap: () async {
                                    await Future.delayed(
                                        const Duration(milliseconds: 200));
                                    // ignore: use_build_context_synchronously

                                    // Navigator.push(
                                    //         context,
                                    //         MaterialPageRoute(
                                    //             builder: (context)=>Settings(
                                    //       userdetails: userdetails,
                                    //     )))
                                    //     .then((value) async {});

                                    final result = await Navigator.of(context,
                                            rootNavigator: true)
                                        .push(
                                      MaterialPageRoute(
                                        builder: (context) => Settings(
                                          userdetails: userdetails,
                                        ),
                                      ),
                                    );

                                    // Check if a result was returned from the second screen.
                                    if (result != null) {
                                      // Do something with the result.
                                      print(
                                          'Received result from Second Screen: $result');
                                      getUserDetails();
                                    }
                                    // var result = navandThen(
                                    //     context,
                                    //     Settings(
                                    //       userdetails: userdetails,
                                    //     ),
                                    //     getUserDetails());
                                    //     print("the result is $result");

                                    //   Navigator.push(context, MaterialPageRoute(builder: (context)=>const OrderDetail()));
                                  }),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  void handleImageUpload(type) async {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.photo_library),
                title: Text('Gallery'),
                onTap: () async {
                  Navigator.of(context).pop();
                  final File image = await getImageFromGallery();
                  if (image != null) {
                    final croppedImage =
                        await ProfileFunctions().cropImage(image);
                    uploadSelectedImage(croppedImage!, type);
                  }
                },
              ),
              ListTile(
                leading: Icon(Icons.photo_camera),
                title: Text('Camera'),
                onTap: () async {
                  Navigator.of(context).pop();
                  final File image = await getImageFromCamera();
                  if (image != null) {
                    final croppedImage =
                        await ProfileFunctions().cropImage(image);
                    uploadSelectedImage(croppedImage!, type);
                  }
                  // uploadSelectedImage(image, type);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Future<dynamic> getImageFromGallery() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      return File(image.path);
    }
    return null;
  }

  Future<dynamic> getImageFromCamera() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.camera);
    if (image != null) {
      return File(image.path);
    }
    return null;
  }

  String? imageUrl;
  void uploadSelectedImage(File imageFile, String type) async {
    isloading = true;
    setState(() {});
    // ignore: unnecessary_null_comparison
    if (imageFile != null) {
      String fileName =
          'unique_filename.jpg'; // You can generate a unique filename here
      String downloadUrl = '';
      await ProfileFunctions().uploadProfilePic(imageFile, type);

      await getCustomer();

      // ignore: unnecessary_null_comparison
      if (downloadUrl != null) {
        // Use the download URL to display the image
        setState(() {
          imageUrl = downloadUrl;
        });
      } else {
        // Handle the error
        print('Image upload failed.');
      }
    } else {
      // No image selected
      print('No image selected.');
    }
    isloading = false;
    setState(() {});
  }

  dynamic userdetails;
  List departmentList = [];

  getCustomer() async {
    isloading = true;
    setState(() {});
    var data = {};

    var response = await Controller().getCustomer(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      userdetails = response['data']['user_details'];
      print("userdetails are $userdetails");
      await Storage.setLogin(userdetails);

      if (userdetails['profile_picture_url'] == null) {
      } else {
        imgurl = userdetails['profile_picture_url'];
        // showToast(imgurl);
      }
      if (userdetails['meta_data'] == null ||
          userdetails['meta_data'].isEmpty) {
      } else {
        if (userdetails['banner_picture_url'] == null) {
        } else {
          try {
            bannerurl = userdetails['banner_picture_url'];
            // .toString()
            // .split("https://snapsendreport.com//web/user/banner/")[1];
          } catch (e) {
            bannerurl = userdetails['meta_data']['banner'];
          }
        }

        // bannerurl = userdetails['meta_data']['banner'];
      }
    }

    isloading = false;
    setState(() {});
  }
}

class CurvedBottomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..lineTo(0, size.height - 30) // Move to the top-left corner
      ..quadraticBezierTo(
        size.width / 2, size.height, // Control point and end point
        size.width, size.height - 30, // Move to the top-right corner
      )
      ..lineTo(size.width, 0) // Move to the bottom-right corner
      ..close(); // Close the path to form a shape

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
