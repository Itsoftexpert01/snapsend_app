import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/model/buttons.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';
import 'package:snap_send_app/model/widgets.dart';

class ChangePassword extends StatefulWidget {
  ChangePassword({Key? key}) : super(key: key);

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController? currentPassword = TextEditingController();

  TextEditingController? password = TextEditingController();
  TextEditingController? cpassword = TextEditingController();

  bool obsecureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title:  Text(
            'Change Password'.tr,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              pops(context);
            },
          ),
        ),
        // backgroundColor: primaryColor,
        body: ModalProgressHUD(
          inAsyncCall: isloading,
          progressIndicator: spinkit,
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // const Padding(
                  //   padding: EdgeInsets.only(top: 40.0),
                  //   child: Text(
                  //     'Login Account',
                  //     textAlign: TextAlign.center,
                  //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Image.asset(
                        'assets/snapsend_logo.png',
                        // fit: BoxFit.fill,
                      ),
                    ),
                  ),
                   Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Current Password'.tr,
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 5.0, left: 60, right: 60.0),
                    child: textformfield1(
                        obscureText: obsecureText,
                        onPressed: () {
                          obsecureText = !obsecureText;
                          setState(() {});
                        },
                        controller: currentPassword,
                        icon: !obsecureText
                            ? Icons.visibility_off
                            : Icons.visibility,
                        hinttext: 'Current Password'),
                  ),
                   Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Text(
                      'New Password'.tr,
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 5.0, left: 60, right: 60.0),
                    child: textformfield1(
                        obscureText: obsecureText,
                        onPressed: () {
                          obsecureText = !obsecureText;
                          setState(() {});
                        },
                        controller: password,
                        icon: !obsecureText
                            ? Icons.visibility_off
                            : Icons.visibility,
                        hinttext: 'New Password'),
                  ),
                   Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Confirm Password'.tr,
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 5.0, left: 60, right: 60.0),
                    child: textformfield1(
                        obscureText: obsecureText,
                        onPressed: () {
                          obsecureText = !obsecureText;
                          setState(() {});
                        },
                        controller: cpassword,
                        icon: !obsecureText
                            ? Icons.visibility_off
                            : Icons.visibility,
                        hinttext: 'Confirm Password'),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 60, right: 60.0, bottom: 60),
                      child: longbuttons('Submit', () async {
                        if (currentPassword!.text.isEmpty) {
                          showToast("Please write your password first");
                        } else {
                          var data = {
                            "old_password": currentPassword!.text.trim(),
                            "password": password!.text.trim(),
                            "confirm_password": cpassword!.text.trim(),
                          };
                          forgorPassword(data, context);
                        }
                      }, buttonColor, width: 280)),

                  // longbuttons('Login', () {}, primaryColor),
                  // longbuttons('Register', () {}, Colors.black),
                ],
              ),
            ),
          ),
        ));
  }

  bool isloading = false;

  forgorPassword(data, context) async {
    isloading = true;
    setState(() {});
    var resp = await Controller().updatePassword(data);
    log(resp.toString());
    if (resp['succ'] == 0) {
      showToast(resp['errors']);
    } else {}
    isloading = false;
    setState(() {});
  }
}
