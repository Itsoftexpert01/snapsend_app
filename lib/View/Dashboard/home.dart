import 'dart:convert';
import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:snap_send_app/Controller/functions.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/Globals/config.dart';
import 'package:snap_send_app/View/AmberAlert/amber_alert.dart';
import 'package:snap_send_app/View/Drawer/drawer.dart';
import 'package:snap_send_app/View/Profile/ui/profile_screen.dart';
import 'package:snap_send_app/View/PublicNotice/public_notice.dart';
import 'package:snap_send_app/View/Search/searchDepartments/ui/search_screen.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/search_tip.dart';
import 'package:snap_send_app/View/Settings/utils/language_dialog.dart';
import 'package:snap_send_app/View/Tip/tip_screen.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';



class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> key = GlobalKey();

  bool isloading = false;
  // ignore: prefer_typing_uninitialized_variables
  var user;
  // ignore: prefer_typing_uninitialized_variables
  var token;
  getuserDetails() async {
    isloading = true;
    setState(() {});
    user = await Storage.getLogin();
    token = await Storage.getToken();
    log(user.toString());
    isloading = false;
    setState(() {});
  }

  openDialog() async {
    var islanguageSelected = await Storage.get('language');
    print("islanguageSelected is $islanguageSelected");
    if (islanguageSelected.runtimeType == bool && !islanguageSelected) {
      return Get.dialog(LanguageDialog());
    }
  }

  @override
  void initState() {
    openDialog();
    super.initState();
    getuserDetails();
  }


  

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async {
        if (key.currentState!.isDrawerOpen) {
          Navigator.of(context).pop();
          return false;
        }
        return true;
      },
      child: Scaffold(
        //    drawerEnableOpenDragGesture: false,
        key: key,
        appBar: AppBar(
          leading: InkWell(
              onTap: () {
                key.currentState!.openDrawer();
              },
              child: const Icon(Icons.menu)),
          backgroundColor: buttonColor,
          title: Text(
            'Home'.tr,
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
        ),
        drawer: DrawerScreen(),
        body: isloading
            ? Center(
                child: spinkit,
              )
            : ModalProgressHUD(
                inAsyncCall: isloading,
                progressIndicator: spinkit,
                child: ListView(
                  children: [
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 5.0, bottom: 16.0, left: 10, right: 10),
                          //    padding: const EdgeInsets.all(16.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: SizedBox(
                              height: 180,
                              width: MediaQuery.of(context).size.width,
                              //  width: MediaQuery.of(context).size.width-2,
                              child: Image.asset(
                                'assets/intro.jpg',
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 5.0, bottom: 16.0, left: 10, right: 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Container(
                              height: 180,
                              width: MediaQuery.of(context).size.width,
                              color: const Color(0xff4064c4).withOpacity(0.5),
                            ),
                          ),
                        ),
                        Positioned(
                          right: 20,
                          //30,
                          top: 40,
                          child: Image.asset(
                            'assets/snapsend_logo.png',
                            fit: BoxFit.cover,
                            height: 120,
                            width: 120,
                          ),
                        ),
                        Positioned(
                            left: 20,
                            //30,
                            top: 35,
                            child: Text(
                              'Hello!'.tr,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.5),
                            )),
                        Positioned(
                            left: 20,
                            // left: 30,
                            top: 50,
                            child: Text(
                              '${user == false ? "" : user['first_name'].toString()}',
                              style: const TextStyle(
                                  color: Colors.white,
                                  //fontWeight: FontWeight.bold,
                                  fontSize: 26),
                            )),
                        Positioned(
                            left: 20,
                            //   left: 30,
                            top: 82,
                            child: Text(
                              'REPORT TIPS FOR\nSAFER COMMUNITIES'.tr,
                              style: TextStyle(
                                  color: Colors.white,
                                  // fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            )),
                        // 18 November 5:18pm
                        Positioned(
                            left: 20,
                            // left: 30,
                            top: 120,
                            child: Text(
                              'Role : User'.tr,
                              style: TextStyle(
                                  color: Colors.white,
                                  //  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            )),
                        Positioned(
                          left: 20,
                          // left: 30,
                          top: 140,
                          child: GestureDetector(
                            onTap: () {
                              Functions().launchUrls("tel:911");
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: 30,
                              // width: 100,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 8.0, right: 8.0),
                                child: Center(
                                    child: Row(
                                  children: [
                                    const SizedBox(
                                      width: 2,
                                    ),
                                    const Icon(
                                      Icons.call,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      'Call SOS'.tr,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                  ],
                                )),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    // GestureDetector(
                    //   onTap: () {
                    //     nav(
                    //         context,
                    //         SearchScreen(
                    //           from: "home",
                    //         ));
                    //   },
                    //   child: Chip(
                    //       backgroundColor: Colors.white,
                    //       elevation: 2.0,
                    //       label: SizedBox(
                    //         width: 300,
                    //         child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.center,
                    //           children: [
                    //             Icon(
                    //               CupertinoIcons.search,
                    //               color: buttonColor,
                    //             ),
                    //             const SizedBox(
                    //               width: 10,
                    //             ),
                    //             Text("Search Department".tr),
                    //           ],
                    //         ),
                    //       )),
                    // ),
                    SingleChildScrollView(
                      child: SizedBox(
                        height: height(context) - 340,
                        width: width(context),
                        child: ListView(
                          //   primary: false,
                          //  // shrinkWrap: true,
                          //   physics: const ScrollPhysics(),
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                //user_dummy.png

                                design1('assets/tipIcon.png', 'Submit a Tip',
                                    () async {
                                  await Future.delayed(
                                      const Duration(milliseconds: 200));
                                  // ignore: use_build_context_synchronously
                                  nav(context, TipScreen(from: "home"));
                                }),
                                const SizedBox(
                                  width: 2,
                                ),
                                design2(CupertinoIcons.search, 'Search Tip',
                                    () async {
                                  await Future.delayed(
                                      const Duration(milliseconds: 200));
                                  // ignore: use_build_context_synchronously
                                  nav(context, SearchTip(isfrom: 'dashboard',));
                                }),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                //user_dummy.png

                                design2(
                                    CupertinoIcons.search, 'Search Department',
                                    () async {
                                  await Future.delayed(
                                      const Duration(milliseconds: 200));
                                  // ignore: use_build_context_synchronously
                                  nav(
                                      context,
                                      SearchScreen(
                                        from: "home",
                                      ));
                                  //    nav(context, SetUpScreen());
                                }),

                                const SizedBox(
                                  width: 2,
                                ),
                                design1(
                                    'assets/noticeIcon.png', 'Public Notice',
                                    () async {
                                  await Future.delayed(
                                      const Duration(milliseconds: 200));
                                  // ignore: use_build_context_synchronously
                                  nav(context, const PublicNotice());
                                }),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                design1('assets/alertIcon.png', 'Amber Alert',
                                    () async {
                                  await Future.delayed(
                                      const Duration(milliseconds: 200));
                                  // ignore: use_build_context_synchronously
                                  nav(context, AmberAlert());
                                }),
                                const SizedBox(
                                  width: 2,
                                ),
                                design1('assets/user_dummy.png', 'Set-Up',
                                    () async {
                                  await Future.delayed(
                                      const Duration(milliseconds: 200));
                                  // ignore: use_build_context_synchronously
                                  nav(
                                      context,
                                      MyProfile(
                                        from: "Setup",
                                      ));
                                  //    nav(context, SetUpScreen());
                                }),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                    // const Center(
                    //   child: Text('Home Screen'),
                    // ),
                  ],
                ),
              ),
      ),
    );
  }

//assets/tipIcon.png
  Widget design1(img, txt, void Function() onPressed) {
    return GestureDetector(
      // duration: const Duration(milliseconds: 100),
      // scaleFactor: 1.5,
      onTap: onPressed,
      child: Card(
        elevation: 2.0,
        color: Colors.white,
        child: SizedBox(
          height: 100,
          width: 160,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // const SizedBox(
                  //   height: 5,
                  // ),
                  Image.asset(
                    '$img',
                    width: 40,
                    color: buttonColor,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    '$txt'.tr,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ]),
          ),
        ),
      ),
    );
  }

  Widget design2(img, txt, void Function() onPressed) {
    return GestureDetector(
      // duration: const Duration(milliseconds: 100),
      // scaleFactor: 1.5,
      onTap: onPressed,
      child: Card(
        elevation: 2.0,
        color: Colors.white,
        child: SizedBox(
          height: 100,
          width: 160,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // const SizedBox(
                  //   height: 5,
                  // ),
                  Icon(
                    img,
                    size: 40,
                    color: buttonColor,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    '$txt'.tr,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
