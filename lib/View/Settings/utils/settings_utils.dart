import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:get/get.dart';

class SettingsUtils extends StatelessWidget {
  String? text;
  IconData? icon1;
  IconData? icon2;
  dynamic ontap;
  SettingsUtils({super.key, this.text, this.icon1, this.icon2, this.ontap});

  @override
  Widget build(BuildContext context) {
    return BouncingWidget(
      duration: const Duration(milliseconds: 100),
      scaleFactor: 1.5,
      onPressed: ontap,
      child: ListTile(
        leading: Icon(
          icon1,
          size: 25,
          color: primaryColor,
        ),
        title: Text(
          "${text}".tr,
          style: const TextStyle(fontSize: 18),
        ),
        trailing: const Icon(
          Icons.arrow_forward_ios_outlined,
          size: 20,
          color: Colors.grey,
        ),
      ),
    );
  }
}

class SettingsUtils2 extends StatefulWidget {
  String? text;
  IconData? icon1;
  IconData? icon2;
  dynamic ontap;
  dynamic state;
  void Function(bool)? onChanged;
  SettingsUtils2(
      {super.key,
      this.text,
      this.icon1,
      this.icon2,
      this.ontap,
      this.state,
      this.onChanged});

  @override
  State<SettingsUtils2> createState() => _SettingsUtils2State();
}

class _SettingsUtils2State extends State<SettingsUtils2> {
  bool value = false;

  Future<bool> _getFuture() async {
    await Future.delayed(const Duration(seconds: 2));
    return !value;
  }

  @override
  Widget build(BuildContext context) {
    return
        // BouncingWidget(
        //   duration: const Duration(milliseconds: 100),
        //   scaleFactor: 1.5,
        //   onPressed: widget.ontap,
        //   child:
        ListTile(
      leading: Icon(
        widget.icon1,
        size: 25,
        color: primaryColor,
      ),
      title: Text(
        widget.text!.tr,
        style: const TextStyle(fontSize: 18),
      ),
      trailing:
          CupertinoSwitch(value: widget.state, onChanged: widget.onChanged),

      // LoadSwitch(
      //   thumbSizeRatio: 0.8,
      //   width: 50,
      //   height: 30,
      //   value: value,
      //   future: _getFuture,
      //   onChange: (v) {
      //     value = v;
      //     widget.state = v;
      //     print('Value changed to $v');
      //     setState(() {});
      //   },
      //   onTap: (v) {
      //     print('Tapping while value is $v');
      //   },
      // )
      //  ),
    );
  }
}
