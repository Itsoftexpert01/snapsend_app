import 'package:flutter/material.dart';
import 'package:snap_send_app/model/color.dart';

class TextEntryField extends StatelessWidget {
  final TextEditingController? controller;
  final String? label;
  final String? image;
  final String? initialValue;
  final bool? readOnly;
  final TextInputType? keyboardType;
  final int? maxLength;
  final int? maxLines;
  final String? hint;
  final InputBorder? border;
  final Widget? prefix;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function? onTap;
  final TextCapitalization? textCapitalization;

  TextEntryField({
    this.controller,
    this.label,
    this.image,
    this.initialValue,
    this.readOnly,
    this.keyboardType,
    this.maxLength,
    this.hint,
    this.border,
    this.prefix,
    this.maxLines,
    this.suffixIcon,
    this.onTap,
    this.textCapitalization,
    this.prefixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        label != null
            ? Text(
                label!,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2!
                    .copyWith(fontSize: 18.5),
              )
            : SizedBox.shrink(),
        TextFormField(
          textCapitalization:
              textCapitalization ?? TextCapitalization.sentences,
          cursorColor: primaryColor,
          onTap: onTap as void Function()?,
          autofocus: false,
          controller: controller,
          initialValue: initialValue ?? null,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 18.5),
          readOnly: readOnly ?? false,
          keyboardType: keyboardType,
          minLines: 1,
          maxLength: maxLength,
          maxLines: maxLines,
          decoration: InputDecoration(
            prefixIconConstraints: BoxConstraints(minHeight: 12, minWidth: 12),
            // contentPadding: EdgeInsets.only(top: 15),
            prefix: prefix ?? null,
            prefixIcon: prefixIcon ?? null,
            suffixIcon: suffixIcon,
            hintText: hint,
            hintStyle:
                Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 16.5),
            border: border ??
                UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[200]!),
                ),
            enabledBorder: border ??
                UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[200]!),
                ),
            focusedBorder: border ??
                UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[200]!),
                ),
            counter: Offstage(),
            icon: (image != null)
                ? ImageIcon(
                    AssetImage(image!),
                    color: primaryColor,
                    size: 20.0,
                  )
                : null,
          ),
        ),
      ],
    );
  }
}

class TextEntryFieldR extends StatelessWidget {
  final TextEditingController? controller;
  final String? label;
  final String? image;
  final String? initialValue;
  final bool? readOnly;
  final TextInputType? keyboardType;
  final int? maxLength;
  final int? maxLines;
  final String? hint;
  final InputBorder? border;
  final Widget? prefix;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function? onTap;
  final TextCapitalization? textCapitalization;

  TextEntryFieldR({
    this.controller,
    this.label,
    this.image,
    this.initialValue,
    this.readOnly,
    this.keyboardType,
    this.maxLength,
    this.hint,
    this.border,
    this.prefix,
    this.maxLines,
    this.suffixIcon,
    this.onTap,
    this.textCapitalization,
    this.prefixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
      child: Column(
        children: [
          TextFormField(
            textCapitalization:
                textCapitalization ?? TextCapitalization.sentences,
            cursorColor: primaryColor,
            onTap: onTap as void Function()?,
            autofocus: false,
            controller: controller,
            initialValue: initialValue ?? null,
            style: Theme.of(context).textTheme.caption,
            readOnly: readOnly ?? false,
            keyboardType: keyboardType,
            minLines: 1,
            maxLength: maxLength,
            maxLines: maxLines,
            decoration: InputDecoration(
              alignLabelWithHint: true,
              prefix: prefix ?? null,
              prefixIcon: prefixIcon ?? null,
              suffixIcon: suffixIcon,
              labelText: label ?? null,
              hintText: hint,
              hintStyle: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(color: Colors.grey, fontSize: 18),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[200]!),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[200]!),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[200]!),
              ),
              counter: Offstage(),
              icon: (image != null)
                  ? ImageIcon(
                      AssetImage(image!),
                      color: primaryColor,
                      size: 20.0,
                    )
                  : null,
            ),
          ),
        ],
      ),
    );
  }
}

class EntryField extends StatelessWidget {
  final String? labelText;
  final String? hintText;
  final bool showSuffixIcon;
  final TextEditingController tc;
  final bool isobsecure;
  EntryField(this.labelText, this.hintText, this.showSuffixIcon, this.tc,
      {this.isobsecure = false});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          labelText!,
          textAlign: TextAlign.start,
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: Colors.black, fontSize: 18.5),
        ),
        SizedBox(
          height: 5,
        ),
        TextFormField(
          controller: tc,
          obscureText: isobsecure,
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(color: Colors.grey),
          decoration: InputDecoration(
            isDense: true,
            suffixIcon: showSuffixIcon
                ? Icon(Icons.keyboard_arrow_down, color: Colors.grey)
                : null,
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[300]!)),
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[300]!)),
            hintText: hintText,
            hintStyle:
                Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 18.5),
          ),
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }
}

class EntryFieldR extends StatelessWidget {
  final String? labelText;
  final String? hintText;
  final TextEditingController tc;
  EntryFieldR(this.labelText, this.hintText, this.tc);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          labelText!,
          textAlign: TextAlign.start,
          style:
              Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 13.5),
        ),
        SizedBox(
          height: 5,
        ),
        TextFormField(
          controller: tc,
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: Colors.grey, fontSize: 18.5),
          decoration: InputDecoration(
            isDense: true,
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[200]!)),
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[200]!)),
            hintText: hintText,
            hintStyle:
                Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 18.5),
          ),
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }
}

class EntryFieldB extends StatelessWidget {
  final String? labelText;
  final String hintText;
  final bool showSuffixIcon;
  final TextEditingController tc;
  EntryFieldB(this.labelText, this.hintText, this.showSuffixIcon, this.tc);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          labelText!,
          textAlign: TextAlign.start,
          style:
              Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 18.5),
        ),
        SizedBox(
          height: 5,
        ),
        TextFormField(
          readOnly: true,
          controller: tc,
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(color: Colors.grey,fontSize: 16),
          decoration: InputDecoration(
            isDense: true,
            suffixIcon: showSuffixIcon
                ? Icon(Icons.keyboard_arrow_down, color: Colors.grey)
                : null,
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[300]!)),
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[300]!)),
            hintText: hintText,
            hintStyle:
                Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 18.5),
          ),
        ),
      ],
    );
  }
}

class ColorButton extends StatelessWidget {
  final String? title;
  ColorButton(this.title);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: Theme.of(context).primaryColor),
      child: Center(
          child: Text(
        title!.toUpperCase(),
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
            fontSize: 15,
            letterSpacing: 2,
            color: Colors.white,
            fontWeight: FontWeight.bold),
      )),
    );
  }
}

class ColorButtonFull extends StatelessWidget {
  final String? title;
  ColorButtonFull(this.title);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
      decoration: BoxDecoration(color: Theme.of(context).primaryColor),
      child: Center(
          child: Text(
        title!.toUpperCase(),
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
            fontSize: 13.5,
            letterSpacing: 2,
            color: Colors.white,
            fontWeight: FontWeight.bold),
      )),
    );
  }
}
