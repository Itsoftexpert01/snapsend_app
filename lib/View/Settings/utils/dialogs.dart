import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Globals/loader.dart';
import 'package:snap_send_app/View/Search/searchTip/ui/search_tip.dart';
import 'package:snap_send_app/model/nav.dart';

class TipDialog extends StatelessWidget {
  String txt;
  String? isfrom;
  TipDialog(this.txt, {this.isfrom});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text(removeHtmlTags(txt.tr))),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image.asset(
            "assets/success.png",
            width: 100,
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {
              pop(context);
              print("is from $isfrom");
              if (isfrom == null) {

                nav(context, SearchTip(isfrom:isfrom));
              } else {
                pop(context);
              }

              //
            },
            child: Text('View Tip'.tr),
          ),
          // ElevatedButton(
          //   onPressed: () {
          //     languageController.changeLanguage(const Locale('es', 'ES'));
          //     Storage.set('language', 'es');
          //     Get.back();
          //   },
          //   child: const Text('Español'),
          // ),
        ],
      ),
    );
  }
}
