import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';

class LanguageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LanguageController languageController = Get.put(LanguageController());
    return AlertDialog(
      title: Text('Change Language'.tr),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ElevatedButton(
            onPressed: () {
              languageController.changeLanguage(const Locale('en', 'US'));
              Storage.set('language', 'en');
              Get.back();
            },
            child: const Text('English'),
          ),
          ElevatedButton(
            onPressed: () {
              languageController.changeLanguage(const Locale('es', 'ES'));
              Storage.set('language', 'es');
              Get.back();
            },
            child: const Text('Español'),
          ),
        ],
      ),
    );
  }
}

class LanguageController extends GetxController {
  var locale = Get.locale;

  void changeLanguage(Locale newLocale) {
    Get.updateLocale(newLocale);
    locale = newLocale;
  }
}
