import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/View/Notification/notification_screen.dart';
import 'package:snap_send_app/View/Profile/ui/change_password.dart';
import 'package:snap_send_app/View/Settings/functions/setting_functions.dart';
import 'package:snap_send_app/View/Settings/utils/language_dialog.dart';
import 'package:snap_send_app/View/Settings/utils/settings_utils.dart';
import 'package:snap_send_app/model/bottom_nav.dart';
import 'package:snap_send_app/model/color.dart';
import 'package:snap_send_app/model/nav.dart';

class Settings extends StatefulWidget {
  dynamic userdetails;
  Settings({this.userdetails, super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  var meta_data;
  bool savereports = false;
  bool emailreport = false;

  bool textreport = false;

  bool emailupdates = false;

  bool allowapptoaccess = false;
  bool rememberLogin = false;
  getSettings() async {
    meta_data = widget.userdetails['meta_data'];
    if (meta_data == null ||
        meta_data.isEmpty ||
        meta_data['notification'].isEmpty) {
    } else {
      if (meta_data['notification'].contains(0)) {
        savereports = true;
      }
      if (meta_data['notification'].contains(1)) {
        emailreport = true;
      }
      if (meta_data['notification'].contains(2)) {
        textreport = true;
      }
      if (meta_data['notification'].contains(3)) {
        emailupdates = true;
      }
      if (meta_data['notification'].contains(4)) {
        allowapptoaccess = true;
      }
    }

    var res = await Storage.get('remeberlogin');
    if (res == 'false') {
    } else {
      rememberLogin = true;
    }
    log(res.runtimeType.toString());
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getSettings();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: buttonColor,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop(widget.userdetails);
          },
        ),
        centerTitle: true,
        title: Text(
          'Settings'.tr,
          style: TextStyle(fontSize: 24),
        ),
        actions: [
          IconButton(
              onPressed: () {
                navPushReplacement(context, MyBottomNavigationBar());
              },
              icon: const Icon(CupertinoIcons.home)),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Container(
                width: width(context),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 0.5, color: Colors.grey)),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SettingsUtils(
                        icon1: CupertinoIcons.lock,
                        text: 'Change Password',
                        ontap: () {
                          nav(context, ChangePassword());
                        },
                      ),
                      SettingsUtils(
                        icon1: CupertinoIcons.bell,
                        text: 'Notification',
                        ontap: () {
                          nav(context, const NotificationScreen());
                        },
                      ),
                      SettingsUtils2(
                        icon1: Icons.lock,
                        text: 'Remember Login Details',
                        ontap: () {},
                        state: rememberLogin,
                        onChanged: (v) {
                          rememberLogin = v;
                          Storage.set('remeberlogin', '$rememberLogin');
                          setState(() {});
                        },
                      ),

                      SettingsUtils(
                        icon1: Icons.language,
                        text: 'Change Language',
                        ontap: () {
                          Get.dialog(LanguageDialog());
                        },
                      ),

                      // SettingsUtils(
                      //   icon1: Icons.error_outline,
                      //   text: 'Terms & Conditions',
                      //   ontap: () {},
                      // ),
                    ]),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(left: 18.0),
              child: Text(
                'Profile Setting'.tr,
                style: TextStyle(
                    fontSize: 20,
                    color: buttonColor,
                    fontWeight: FontWeight.w500),
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Container(
                width: width(context),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 0.5, color: Colors.grey)),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SettingsUtils2(
                        icon1: CupertinoIcons.bell,
                        text: 'Save reports in my private file after sending',
                        state: savereports,
                        onChanged: (val) {
                          setState(() {
                            savereports = val;
                          });
                          if (savereports) {
                            SettingFunctions.updateSettings(0);
                          } else {
                            SettingFunctions.deleteSettings(0);
                          }

                          print("savereports is $savereports");
                        },
                      ),
                      SettingsUtils2(
                        icon1: CupertinoIcons.bell,
                        text:
                            'E-mail my report tracking number to my email above',
                        state: emailreport,
                        onChanged: (val) {
                          setState(() {
                            emailreport = val;
                          });
                          if (emailreport) {
                            SettingFunctions.updateSettings(1);
                          } else {
                            SettingFunctions.deleteSettings(1);
                          }
                          print("emailreport is $emailreport");
                        },
                      ),
                      SettingsUtils2(
                        icon1: CupertinoIcons.bell,
                        text: 'Text my report tracking number this phone',
                        state: textreport,
                        onChanged: (val) {
                          setState(() {
                            textreport = val;
                          });
                          if (textreport) {
                            SettingFunctions.updateSettings(2);
                          } else {
                            SettingFunctions.deleteSettings(2);
                          }
                          print("textreport is $textreport");
                        },
                      ),
                      SettingsUtils2(
                        icon1: CupertinoIcons.bell,
                        text: 'E-mail my updates to my email above',
                        state: emailupdates,
                        onChanged: (val) {
                          setState(() {
                            emailupdates = val;
                          });
                          if (emailupdates) {
                            SettingFunctions.updateSettings(3);
                          } else {
                            SettingFunctions.deleteSettings(3);
                          }
                          print("emailupdates is $emailupdates");
                        },
                      ),
                      SettingsUtils2(
                        icon1: CupertinoIcons.bell,
                        text:
                            'Allow this app to access your phone to send in reports',
                        state: allowapptoaccess,
                        onChanged: (val) {
                          setState(() {
                            allowapptoaccess = val;
                          });
                          if (allowapptoaccess) {
                            SettingFunctions.updateSettings(4);
                          } else {
                            SettingFunctions.deleteSettings(4);
                          }
                          print("allowapptoaccess is $allowapptoaccess");
                        },
                      ),
                      SizedBox(
                        height: 10,
                      )
                      // SettingsUtils(
                      //   icon1: Icons.error_outline,
                      //   text: 'Terms & Conditions',
                      //   ontap: () {},
                      // ),
                    ]),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 50),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      'Version 1.0.0 (2023)',
                      style: TextStyle(fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.only(top: 18.0),
            //   child: Button(
            //     title: 'Save Changes',
            //     fontsize: 24,
            //     color: Colors.white,
            //     fw: FontWeight.bold,
            //     containerColor: primaryColor,
            //     onPressed: () {},
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
