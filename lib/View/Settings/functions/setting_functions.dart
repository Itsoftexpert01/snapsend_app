import 'package:snap_send_app/Controller/controller.dart';
import 'package:snap_send_app/Globals/loader.dart';

class SettingFunctions {
  static updateSettings(index) async {
    var data = {
      "meta_data": {
        "notification": [index]
      }
    };

    var response = await Controller().updateProfileSetting(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      return response['data'];
    }
  }

  static deleteSettings(index) async {
    var data = {
      "meta_data": {
        "notification": [index]
      }
    };

    var response = await Controller().deleteProfileSetting(data);

    if (response['succ'] == false) {
      showToast(response['errors']);
    } else {
      return response['data'];
    }
  }
}
