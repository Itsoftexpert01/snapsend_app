import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';
import 'package:snap_send_app/View/Splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:snap_send_app/model/Language/languages.dart';
import 'package:snap_send_app/model/color.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var locale = const Locale('en', 'US');
  bool isloading = false;
  getLanguage() async {
    isloading = true;
    setState(() {});
    var islanguageSelected = await Storage.get('language');
    if (islanguageSelected.runtimeType == bool && !islanguageSelected) {
    } else {
      if (islanguageSelected == "en") {
        locale = const Locale('en', 'US');
      } else {
        locale = const Locale('es_ES', 'ES');
      }
    }
    isloading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getLanguage();
  }

  @override
  Widget build(BuildContext context) {
    return isloading
        ? Container(
            color: primaryColor,
          )
        : GetMaterialApp(
            debugShowCheckedModeBanner: false,
            locale: locale,
            fallbackLocale: locale,
            translations: Languages(),
            builder: (context, child) => ResponsiveWrapper.builder(child,
                maxWidth: 1200,
                minWidth: 400,
                defaultScale: true,
                breakpoints: const [
                  ResponsiveBreakpoint.resize(400, name: MOBILE),
                  ResponsiveBreakpoint.autoScale(800, name: TABLET),
                  ResponsiveBreakpoint.resize(1000, name: DESKTOP),
                ],
                background: Container(color: const Color(0xFFF5F5F5))),
            title: 'SnapSend',
            theme: ThemeData(
              colorScheme:
                  const ColorScheme.light().copyWith(primary: buttonColor),
            ),
            home: SplashScreen()
            );
  }
}
