import 'package:snap_send_app/Controller/controller.dart';
import 'package:url_launcher/url_launcher.dart';

class Functions {
  // ignore: prefer_typing_uninitialized_variables
  var countries;
  // ignore: prefer_typing_uninitialized_variables
  var states;
  // ignore: prefer_typing_uninitialized_variables
  var cities;
  List<String> countriesList = [];
  List<String> countriesStates = [];

  List<String> countriesCities = [];

  getCountries() async {
    countriesList = [];
    countriesStates = [];
    countriesCities = [];
    var data = await Controller().getAllCountries();
    countries = data['data']['app_countries'];
    for (int i = 0; i < countries.length; i++) {
      if (countriesList.contains(countries[i]['name'])) {
      } else {
        countriesList.add(countries[i]['name']);
      }
    }
    return {"countries": countries, "countriesList": countriesList};
  }

  getStates(id) async {
    countriesStates = [];
    countriesCities = [];
    var data = {"country": id};
    var resp = await Controller().getAllStates(data);
    states = resp['data']['app_states'];
    for (int i = 0; i < states.length; i++) {
      if (countriesStates.contains(states[i]['name'])) {
      } else {
        countriesStates.add(states[i]['name']);
      }
    }
    return {"states": states, "statesList": countriesStates};
  }

  getCities(id) async {
    countriesCities = [];
    var data = {"state": id};
    var resp = await Controller().getAllCities(data);
    cities = resp['data']['app_cities'];
    for (int i = 0; i < cities.length; i++) {
      if (countriesCities.contains(cities[i]['name'])) {
      } else {
        countriesCities.add(cities[i]['name']);
      }
    }
    return {"cities": cities, "citiesList": countriesCities};
  }

  Future<void> launchUrls(url) async {
    if (!await launchUrl(Uri.parse(url))) {
      throw Exception('Could not launch $url');
    }
  }
}
