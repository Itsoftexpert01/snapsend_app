// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, prefer_interpolation_to_compose_strings

import 'package:snap_send_app/Controller/req.dart';
import 'package:snap_send_app/Globals/config.dart';

class Controller {
  var req = Req();
  // api

  /*Api Keys*/

  var GET_COUNTRY_LIST = "master/countries";
  var GET_STATE_LIST = "master/states";
  var GET_CITY_LIST = "master/cities";
  var CUSTOMER_SIGNUP = "customer/signup";
  var CUSTOMER_REQUEST_OTP = "customer/reqOTP";
  var CUSTOMER_VERIFY_OTP = "customer/verifyOTP";
  var CUSTOMER_RESEND_OTP = "customer/resendOTP";
  var MASTER_DEPARTMENT_LIST = "master/departments";
  var CUSTOMER_LOGIN = "customer/login";
  var GET_CUSTOMER_DETAILS = "customer/get";
  var UPDATE_CUSTOMER_DETAILS = "customer/update";
  var SEARCH_DEPARTMENT = "public/departments";
  var PUBLIC_NOTICE = "web/public/notice";
  var UPDATE_DEPARTMENT = "customer/updateDepartment";
  var GET_DEPARTMENT = "customer/getDepartment";
  var UPDATE_PROFILE_SETTING = "customer/updateProfileSetting";
  var DELETE_PROFILE_SETTING = "customer/deleteProfileSetting";
  var GET_FILE_A_TIP = "customer/getCustomerReport";
  var UPLOAD_PROFILE_PICTURE = "customer/uploadProfilePic";

  var GET_OFFICER_LIST = "officer/list";

  var GET_HOME_SLIDES = "web/entity";

  var FILE_A_TIP = "customer/createfileReport";
  var LED_UPDATE_PROFILE = "public/department/update";
  var TIP_LIST = "department/tip/list";

  var OFFICER_PROFILE_DETAILS = "public/department/profileshow";
  var LED_DESK_LIST = "department/desks/list";
  var CREATE_NOTICE = "department/create/notice";
  var UPDATE_PASSWORD = "customer/updatePass";
  var RESET_PASSWORD = "customer/send-reset-password-link/email";
  var GET_SEX_OFFENDERS = "master/sex_offenders";
  var CONTACT_US = "master/contact_us";
  var SEARCH_REPORT_DETAILS = "department/tip/list-detail-in-app";
  var GET_NOTIFICATION = "department/customer/notification";
  var MARK_AS_READ_NOTIFICATION =
      "department/customer/notification/mark-as-read";
  var GET_PAST_TIP = "customer/getCustomerReport";
  var PERSON_OF_INTEREST = "master/suspects/add";
  var UPDATE_NOTIFICATION_SETTING = "customer/updateProfileSetting";
  var REMOVE_NOTIFICATION_SETTING = "customer/deleteProfileSetting";
  var GET_NOTIFICATION_SETTING = "customer/get-customer-profile-settings";

  var IMAGE_BASE_URL = "https://snapsendreport.com/web/report/picture/";
  var VIDEO_BASE_URL = "https://snapsendreport.com/web/report/video/";
  var AUDIO_BASE_URL = "https://snapsendreport.com/web/report/voicetrack/";
  Future<dynamic> login(data) async {
    return await req.post(Config.baseURL + 'customer/login', data);
  }

  Future<dynamic> signUp(data) async {
    return await req.post(Config.baseURL + 'customer/signup', data);
  }

  Future<dynamic> contactUs(data) async {
    return await req.post(Config.baseURL + CONTACT_US, data);
  }

  Future<dynamic> updateCustomer(data) async {
    return await req.post(Config.baseURL + UPDATE_CUSTOMER_DETAILS, data);
  }

  Future<dynamic> resetPassword(data) async {
    return await req.post(Config.baseURL + RESET_PASSWORD, data);
  }

  Future<dynamic> updatePassword(data) async {
    return await req.post(Config.baseURL + UPDATE_PASSWORD, data);
  }

  Future<dynamic> createBooking(data) async {
    return await req.post(Config.baseURL + 'master/countries', data);
  }

  Future<dynamic> getAllBookings(data) async {
    return await req.post(Config.baseURL + 'master/countries', data);
  }

  Future<dynamic> getAllCountries() async {
    return await req.get(Config.baseURL + 'master/countries');
  }

  Future<dynamic> getAllStates(data) async {
    return await req.post(Config.baseURL + 'master/states', data);
  }

  Future<dynamic> getListDetailsInApp(id) async {
    return await req.get(Config.baseURL + SEARCH_REPORT_DETAILS+"?report_id=$id");
  }

  Future<dynamic> getDepartmentsByCountryState(data) async {
    return await req.get(Config.liveURL +
        SEARCH_DEPARTMENT +
        "?country=${data['country']}&state=${data['state']}&city=${data['City']}&per_page=${data['per_page']}&page=${data['page']}&nameSearch=${data['nameSearch'] ?? ""}");

    //  return await req.post(Config.baseURL + MASTER_DEPARTMENT_LIST, data);
  }

  Future<dynamic> getHomeDepartmentsByCountryState(data) async {
    return await req.post(Config.baseURL + MASTER_DEPARTMENT_LIST, data);
  }

  Future<dynamic> getAllCities(data) async {
    return await req.post(Config.baseURL + 'master/cities', data);
  }

  Future<dynamic> getBookingsbyId(data) async {
    return await req.post(Config.baseURL + 'post/getbookingbyId', data);
  }

  Future<dynamic> getBookings(data, action) async {
    return await req.post(Config.baseURL + 'post/$action', data);
  }

  Future<dynamic> reqOTP(data) async {
    return await req.post(Config.baseURL + CUSTOMER_REQUEST_OTP, data);
  }

  Future<dynamic> verifyOTP(data) async {
    return await req.post(Config.baseURL + CUSTOMER_VERIFY_OTP, data);
  }

  Future<dynamic> resendOTP(data) async {
    return await req.post(Config.baseURL + CUSTOMER_RESEND_OTP, data);
  }

  Future<dynamic> getDepartment(data) async {
    return await req.post(Config.baseURL + GET_DEPARTMENT, data);
  }

  Future<dynamic> updateProfileSetting(data) async {
    return await req.post(Config.baseURL + UPDATE_PROFILE_SETTING, data);
  }

  Future<dynamic> deleteProfileSetting(data) async {
    return await req.post(Config.baseURL + DELETE_PROFILE_SETTING, data);
  }

  Future<dynamic> getCustomerReport(data) async {
    return await req.post(Config.baseURL + GET_PAST_TIP, data);
  }

  Future<dynamic> uploadProfilePic(data) async {
    return await req.post(Config.baseURL + UPLOAD_PROFILE_PICTURE, data);
  }

  Future<dynamic> getNotice(data) async {
    return await req.post(Config.baseURL + PUBLIC_NOTICE, data);
  }

  Future<dynamic> getSexOffender(data) async {
    return await req.get(Config.baseURL + GET_SEX_OFFENDERS + data);
  }

  Future<dynamic> getCustomer(data) async {
    return await req.post(Config.baseURL + GET_CUSTOMER_DETAILS, data);
  }

  Future<dynamic> createfileReport(data) async {
    return await req.post(Config.liveURL + FILE_A_TIP, data);
  }

  Future<dynamic> webEntity(data) async {
    return await req.post(Config.baseURL + GET_HOME_SLIDES, data);
  }

  Future<dynamic> getNotifications(data) async {
    return await req.post(Config.baseURL + GET_NOTIFICATION, data);
  }

  Future<dynamic> updateDepartment(data) async {
    return await req.post(Config.baseURL + UPDATE_DEPARTMENT, data);
  }

  Future<dynamic> cancelBooking(id) async {
    return await req
        .get(Config.baseURL + 'post/cancelBooking/' + id.toString());
  }

  Future<dynamic> getRandomAds() async {
    return await req.get(Config.baseURL + 'post/getRandomAds');
  }

  Future<dynamic> getAllAds(data) async {
    return await req.post(Config.baseURL + 'post/getAllAds', data);
  }

  Future<dynamic> checkIApplied(data) async {
    return await req.post(Config.baseURL + 'post/checkIApplied', data);
  }

  Future<dynamic> applyingBooking(data) async {
    return await req.post(Config.baseURL + 'post/applyingBooking', data);
  }

  Future<dynamic> getAllHelpersByBooking(id) async {
    return await req
        .get(Config.baseURL + 'post/getAllHelpersByBooking/' + id.toString());
  }

  Future<dynamic> hire(id, postId, fcm, helperid) async {
    return await req.get(Config.baseURL +
        'post/hire/' +
        id.toString() +
        '/' +
        postId.toString() +
        '/' +
        fcm +
        '/' +
        helperid);
  }

  Future<dynamic> updateBooking(data, id) async {
    return await req.post(
        Config.baseURL + 'post/updateBooking/' + id.toString(), data);
  }

  Future<dynamic> markAsComplete(data) async {
    return await req.post(Config.baseURL + 'post/markAsComplete', data);
  }

  Future<dynamic> getAllHelperBookings(data) async {
    return await req.post(Config.baseURL + 'post/getAllHelperBookings', data);
  }

  Future<dynamic> getcompletedBookings(data) async {
    return await req.post(Config.baseURL + 'post/getCompletedBookings', data);
  }
}
