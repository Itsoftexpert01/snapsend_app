import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  static Future setLogin(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user', jsonEncode(data));
    return true;
  }

  static Future<dynamic> getLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('user') != null) {
      dynamic temp = prefs.getString('user');
      return jsonDecode(temp);
    } else {
      return false;
    }
  }

  static Future<dynamic> setToken(token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
    return true;
  }

  static Future<dynamic> set(name, value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('$name', value);
    return true;
  }

  static Future<dynamic> get(name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('$name') != null) {
      return prefs.getString('$name');
    } else {
      return false;
    }
  }

  static Future<dynamic> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('token') != null) {
      return prefs.getString('token');
    } else {
      return false;
    }
  }

  static Future<bool> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('user');
    await prefs.remove('token');
    await prefs.remove('accountid');
    await prefs.remove('fcm');
    return true;
  }

  static Future<dynamic> remove(key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('$key');

    return true;
  }
}
