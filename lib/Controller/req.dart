// ignore_for_file: deprecated_member_use

import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:snap_send_app/Controller/shared_pref.dart';

class DioConfig {
  var dio = Dio();
  DioConfig() {
    dio.interceptors.add(InterceptorsWrapper(
      onRequest:
          (RequestOptions options, RequestInterceptorHandler handler) async {
        options.headers['Content-Type'] = "application/json";
        options.headers["Accept"] = "application/json";
        try {
          var token = await Storage.getToken();
          // log("my token is $token");
          options.headers["Authorization"] = "Bearer $token";
        } catch (e) {
          //  log(e.toString());
        }
        return handler.next(options);
      },
      onResponse: (e, handler) async {
        return handler.next(e);
      },
      onError: (e, handler) async {
        return handler.next(e);
      },
    ));
  }
}

class Req {
  var interceptor = DioConfig();
  get(url) async {
    log("$url");
    try {
      var resp = await interceptor.dio.get(url);
      //  log(resp.data.toString());
      if (resp.statusCode == 200) {
        log("The link is $url & response is ${resp.data.toString()}");
        return json.decode(resp.data);
      } else {
        return null;
      }
    } on DioError catch (e) {
      log("Connection Error");

      return e;
    }
  }

  getWithBody(url, body) async {
    log("$url & $body");
    try {
      var resp = await interceptor.dio.get(url, queryParameters: body);
      log(resp.data.toString());
      if (resp.statusCode == 200) {
        return json.decode(resp.data);
      } else {
        return null;
      }
    } on DioError catch (e) {
      log("Connection Error");

      return e;
    }
  }

  post(url, data) async {
    log("$url $data");
    try {
      var resp = await interceptor.dio.post(url, data: data);
      log("Response of $url is : ${resp.data.toString()}");

      if (resp.statusCode == 200) {
        return json.decode(resp.data);
      } else {
        return null;
      }
    } on DioError catch (e) {
      log("Connection Error");

      return e;
    }
  }
}
