import 'package:snap_send_app/Controller/req.dart';
import 'package:snap_send_app/Globals/config.dart';

class UploaderService {
  var req = Req();
  Future<dynamic> uploader(data) async {
    return await req.post('${Config.baseURL}app/uploader', data);
  }
}
